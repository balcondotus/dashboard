const path = require('path');
const { upload } = require('bugsnag-sourcemaps');
const glob = require('glob');
const fs = require('fs');
const reportBuild = require('bugsnag-build-reporter');
const appVersion = require('./package.json').version;

require('dotenv').config({
  path: path.resolve(__dirname, '.env'),
});

/**
 * Find all of the map files in ./build
 */
function findSourceMaps(callback) {
  return glob('build/**/*/*.map', callback);
}

/**
 * Uploads the source map with accompanying sources
 * @param sourceMap - single .map file
 * @returns {Promise<string>} - upload to Bugsnag
 */
function uploadSourceMap(sourceMap) {
  // Remove .map from the file to get the js filename
  const minifiedFile = sourceMap.replace('.map', '');

  // Remove absolute path to the static assets folder
  const minifiedFileRelativePath = minifiedFile.split('build/')[1];

  return upload({
    apiKey: process.env.REACT_APP_BUGSNAG_API_KEY,
    overwrite: true,
    minifiedUrl: `${process.env.REACT_APP_BALCON_DASHBOARD_URI}/${minifiedFileRelativePath}`,
    sourceMap,
    minifiedFile,
    projectRoot: __dirname,
    uploadSources: true,
  });
}

/**
 * Delete the .map files
 * We do this to protect our source
 * @param files - array of sourcemap files
 */
function deleteFiles(files) {
  files.forEach((file) => {
    fs.unlinkSync(`${__dirname}/${file}`);
  });
}

/**
 * Notifies Bugsnag of the new release
 */
function notifyRelease() {
  reportBuild({
    apiKey: process.env.REACT_APP_BUGSNAG_API_KEY,
    appVersion,
    releaseStage: process.env.NODE_ENV,
  })
    .then(() => console.log('Bugsnag build reported'))
    .catch((err) =>
      console.log('Reporting Bugsnag build failed', err.messsage)
    );
}

/**
 * Find, upload and delete Source Maps
 */
function processSourceMaps() {
  findSourceMaps((error, files) =>
    Promise.all(files.map(uploadSourceMap))
      .then(() => {
        deleteFiles(files);
        notifyRelease();
      })
      .catch((e) => {
        console.log(e);
      })
  );
}

processSourceMaps();
