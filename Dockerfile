ARG NODE_ENV

# dev stage
FROM node:14.5.0-alpine AS dev

# Set working directory
WORKDIR /var/www/

# Install app dependencies
COPY package.json package-lock.json ./
RUN npm ci --silent --only=production

# add app
COPY . ./

# builder stage
FROM dev AS builder

ARG NODE_ENV

ENV NODE_ENV ${NODE_ENV}

RUN npm run build:app

# stage stage
FROM nginx:1.19.1-alpine AS stage

# Remove default files created by Nginx
RUN rm -rvf /usr/share/nginx/html/*
RUN rm -vf /etc/nginx/conf.d/default.conf

# Copy nginx configurations
COPY nginx/staging.dashboard.balcon.us.conf /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf

# Validate Nginx configurations have no syntax errors
RUN nginx -t

COPY --from=builder /var/www/build/ /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]

# master stage
FROM nginx:1.19.1-alpine AS master

# Remove default files created by Nginx
RUN rm -rvf /usr/share/nginx/html/*
RUN rm -vf /etc/nginx/conf.d/default.conf

# Copy nginx configurations
COPY nginx/dashboard.balcon.us.conf /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf

# Validate Nginx configurations have no syntax errors
RUN nginx -t

COPY --from=builder /var/www/build/ /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
