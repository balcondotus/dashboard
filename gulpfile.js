const { series, parallel, src, dest, watch } = require('gulp');
const browserify = require('browserify');
const { pipeline } = require('readable-stream');
const rename = require('gulp-rename');
const merge = require('merge-stream');
const del = require('del');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const rtlcss = require('gulp-rtlcss');
const fs = require('fs');
const cleanCss = require('gulp-clean-css');
const terser = require('gulp-terser');

sass.compiler = require('node-sass');

/**
 * Compiles Bootstrap vendor.
 */
function compileBootstrap() {
  const jsFiles = pipeline(
    src(['./vendors/bootstrap/dist/js/bootstrap.bundle.js']),
    rename({ basename: 'bootstrap' }),
    dest('./public/assets/js/vendors')
  );

  const cssFiles = pipeline(
    src(['./vendors/bootstrap/dist/css/bootstrap.css']),
    dest('./public/assets/css/vendors')
  );

  return merge(jsFiles, cssFiles);
}

/**
 * Compiles Font Awesome vendor.
 */
function compileFontAwesome() {
  const cssFiles = pipeline(
    src(['./vendors/fontawesome/css/all.css']),
    rename({
      basename: 'fontawesome',
    }),
    dest('./public/assets/css/vendors')
  );

  const jsFiles = pipeline(
    src(['./vendors/fontawesome/js/all.js']),
    rename({
      basename: 'fontawesome',
    }),
    dest('./public/assets/js/vendors')
  );

  return merge(jsFiles, cssFiles);
}

/**
 * Deletes font directories from the asset directory.
 */
function deleteFontDirs() {
  return del(
    ['../public/assets/fonts/**/*', '../public/assets/webfonts/**/*'],
    {
      force: true,
    }
  );
}

/**
 * Copies webfonts directory into the public directory.
 */
function copyWebFontsFromFontAwesome() {
  return pipeline(
    src(['./vendors/fontawesome/webfonts/**/*']),
    dest('./public/assets/webfonts')
  );
}

/**
 * Copies Font Iran style into vendors directory.
 */
function copyFontIranStyle() {
  return pipeline(
    src(['./vendors/fontiran/Non_English/WebFonts/css/fontiran.css']),
    dest('./public/assets/css/vendors')
  );
}

/**
 * Copies Font Iran fonts into assets directory.
 */
function copyFontIranFonts() {
  return pipeline(
    src(['./vendors/fontiran/Non_English/WebFonts/fonts/**/*']),
    dest('./public/assets/fonts')
  );
}

/**
 * Builds vendors.js for index.html
 */
function buildVendorsJs() {
  return browserify({
    entries: './browserify.js',
    debug: false,
  })
    .bundle()
    .pipe(fs.createWriteStream('./public/assets/js/vendors.js'));
}

/**
 * Builds vendors.css
 */
function buildVendorsCss() {
  return pipeline(
    // Ignored Bootstrap as we use Front
    src([
      './public/assets/css/vendors/**/*.css',
      '!./public/assets/css/vendors/**/bootstrap.css',
    ]),
    concat('vendors.css'),
    autoprefixer(),
    dest('./public/assets/css')
  );
}

/**
 * Compiles sass files in the public directory.
 */
function compilePublicSass() {
  return pipeline(
    src(['./public/assets/scss/**/*.scss']),
    sass().on('error', sass.logError),
    autoprefixer(),
    dest('./public/assets/css')
  );
}

function clearCompiledVendorsDir() {
  return del(
    [
      './public/assets/css/vendors/**/*',
      './public/assets/js/vendors/**/*',
      '!./public/assets/js/vendors/modernizr-custom.js',
    ],
    {
      force: true,
    }
  );
}

function makeCssRtl() {
  return src([
    './public/assets/css/**/*.css',
    '!./public/assets/css/**/*.rtl.css',
    '!./public/assets/css/**/*.min.css',
  ])
    .pipe(rtlcss())
    .pipe(rename({ suffix: '.rtl' }))
    .pipe(dest('./public/assets/css'));
}

/**
 * Minifies produced JS files.
 */
function minifyJs() {
  return pipeline(
    src(['./public/assets/js/*.js', '!./public/assets/js/*.min.js']),
    terser(),
    rename({ suffix: '.min' }),
    dest((file) => {
      return file.base;
    })
  );
}

/**
 * Minified produced CSS files.
 */
function minifyCss() {
  return pipeline(
    src(['./public/assets/css/*.css', '!./public/assets/css/*.min.css']),
    cleanCss(),
    rename({ suffix: '.min' }),
    dest((file) => {
      return file.base;
    })
  );
}

function watchSass() {
  watch(
    './public/assets/scss/**/*.scss',
    // makeCssRtl must be invoked before minifyCss
    series(compilePublicSass, makeCssRtl, minifyCss)
  );
}

function watchJs() {
  watch(
    ['./public/assets/js/*.js', '!./public/assets/js/*.min.js'],
    series(minifyJs)
  );
}

// Watches internal source codes.
exports.watch = parallel(watchSass, watchJs);

// Builds source codes.
exports.build = series(
  clearCompiledVendorsDir,
  parallel(
    compileBootstrap,
    compileFontAwesome,
    copyFontIranStyle,
    copyFontIranFonts
  ),
  parallel(buildVendorsJs, buildVendorsCss, compilePublicSass),
  deleteFontDirs,
  copyWebFontsFromFontAwesome,
  minifyJs,
  // makeCssRtl must be invoked before minifyCss
  makeCssRtl,
  minifyCss
);
