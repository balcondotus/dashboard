import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { IOContext } from './contexts';
import { IO } from './services';
import { App } from './components';
import './setupApp';

ReactDOM.render(
  <React.StrictMode>
    <IOContext.Provider value={new IO()}>
      <App />
    </IOContext.Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
