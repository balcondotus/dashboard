import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { ButtonGroup, Form } from 'react-bootstrap';
import { FormattedNumber, FormattedMessage } from 'react-intl';
import './PredefinedInput.scss';

const PredefinedInput = ({
  predefinedInputs,
  inputsLabel,
  customInputLabel,
  value,
  displayCustomInput,
  onValueUpdated,
  minValue,
  maxValue,
}) => {
  const [selected, setSelected] = useState(value);
  const customInputRef = useRef();

  useEffect(() => {
    onValueUpdated(selected);
  }, [selected]);

  /**
   * Determines whether the item should be selected or not.
   *
   * @param {any} v
   */
  const shouldSelect = (v) => {
    if (v === undefined) return !predefinedInputs.includes(selected);
    return selected === v;
  };

  /**
   * Handles custom item click and key press events.
   * Pass no argument to check against the custom item.
   *
   * @param {React.ChangeEvent} e
   * @param {any} item
   */
  const handleCustomClick = (e) => {
    setSelected(customInputRef.current.value);
    customInputRef.current.focus();
  };

  /**
   * Handles predefined items click and key press events.
   *
   * @param {React.ChangeEvent} e
   * @param {any} item
   */
  const handlePredefinedClick = (e, item) => {
    e.preventDefault();
    setSelected(item);
  };

  return (
    <div className="predefined-input-component">
      <ButtonGroup>
        {predefinedInputs.map((item) => {
          return (
            <div
              key={item}
              role="button"
              tabIndex={0}
              onKeyPress={(e) => handlePredefinedClick(e, item)}
              onClick={(e) => handlePredefinedClick(e, item)}
              className={`predefined p-1 ${shouldSelect(item) ? 'active' : ''}`}
            >
              <p className="item">
                {typeof item === 'number' ? (
                  <FormattedNumber unit="minute" value={item} />
                ) : (
                  <FormattedMessage id={item} />
                )}
              </p>
              <p className="small text-muted caption">{inputsLabel}</p>
            </div>
          );
        })}
        {displayCustomInput ? (
          <div
            role="button"
            tabIndex={0}
            className={`predefined custom ml-2 p-0 ${
              shouldSelect() ? 'active' : ''
            }`}
            onKeyPress={handleCustomClick}
            onClick={handleCustomClick}
          >
            <p className="item">
              <Form.Control
                type={typeof value === 'number' ? 'number' : 'text'}
                defaultValue={shouldSelect() ? selected : ''}
                min={typeof value === 'number' ? minValue : null}
                max={typeof value === 'number' ? maxValue : null}
                ref={customInputRef}
                onChange={(e) => {
                  setSelected(e.target.value);
                }}
                className="m-0 p-0 px-1 pt-1 dir-ltr"
                placeholder="-"
              />
            </p>
            <p className="small text-muted caption p-0 px-1">
              {customInputLabel}
            </p>
          </div>
        ) : null}
      </ButtonGroup>
    </div>
  );
};

PredefinedInput.propTypes = {
  predefinedInputs: PropTypes.arrayOf(
    (propValue, key, componentName, location, propFullName) => {
      const type = typeof propValue[key];
      if (type !== 'number' && type !== 'string') {
        return new Error(
          `Invalid prop \`${propFullName}\` supplied to` +
            ` \`${componentName}\`. Validation failed.`
        );
      }
    }
  ).isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  inputsLabel: PropTypes.string.isRequired,
  displayCustomInput: PropTypes.bool,
  onValueUpdated: PropTypes.func.isRequired,
  minValue: PropTypes.number,
  maxValue: PropTypes.number,
  customInputLabel: PropTypes.string.isRequired,
};

PredefinedInput.defaultProps = {
  displayCustomInput: true,
  minValue: 0,
  maxValue: 0,
};

export default PredefinedInput;
