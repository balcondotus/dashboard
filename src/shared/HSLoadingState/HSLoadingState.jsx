import React, { Component } from 'react';
import PropTypes from 'prop-types';
import jQuery from 'jquery';

export default class HSLoadingState extends Component {
  HSLoadingStateHTML;
  HSLoadingStateObj;

  componentDidMount() {
    // Must be wrapped by jQuery to work.
    const invokerId = this.props.id;
    jQuery(function () {
      // Initialize HSLoadingState JavaScript library.
      this.HSLoadingStateObj = new window.HSLoadingState(
        $(`#${invokerId}`)
      ).init();
    });
  }

  render() {
    return React.createElement(this.props.as, {
      'data-hs-loading-state-options': JSON.stringify({
        targetEl: this.props.targetEl,
        targetElStyles: this.props.targetElStyles,
        removeLoaderDelay: this.props.removeLoaderDelay,
      }),
      className: this.props.className,
      id: this.props.id,
      'data-toggle': this.props.toggle,
      href: this.props.href,
      role: this.props.role,
      ref: (e) => {
        this.HSLoadingStateHTML = e;
      },
      children: this.props.children,
    });
  }
}

HSLoadingState.propTypes = {
  id: PropTypes.string.isRequired,
  targetEl: PropTypes.string.isRequired,
  toggle: PropTypes.string,
  /**
   * Build the HTML element as this type
   *
   * @default `div`
   */
  as: PropTypes.string,
};

HSLoadingState.defaultProps = {
  as: 'div',
};
