import React from 'react';
import PropTypes from 'prop-types';
import styles from './Avatar.module.scss';

const Avatar = ({ className, imgSrc, imgAlt }) => {
  return (
    <div
      className={`${styles['avatar']} ${styles['avatar-sm']} ${styles['avatar-circle']} ${className}`}
    >
      <img className={styles['avatar-img']} src={imgSrc} alt={imgAlt} />
    </div>
  );
};

Avatar.propTypes = {
  imgSrc: PropTypes.string.isRequired,
  imgAlt: PropTypes.string,
};

Avatar.defaultProps = {
  imgAlt: '',
};

export default Avatar;
