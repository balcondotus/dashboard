import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import './FormattedDuration.scss';

const FormattedDuration = ({ value, children, unitDisplay }) => {
  const durationHours = Math.floor(value / 60);
  const durationMinutes = value % 60;
  const Intl = useIntl();

  const renderHours = (hours) =>
    Intl.formatNumber(hours, {
      style: 'unit',
      unit: 'hour',
      unitDisplay,
    });

  const renderMinutes = (minutes) =>
    Intl.formatNumber(minutes, {
      style: 'unit',
      unit: 'minute',
      unitDisplay,
    });

  if (durationHours <= 0 && durationMinutes <= 0) {
    return children(renderMinutes(durationMinutes));
  }
  if (durationHours <= 0 && durationMinutes > 0) {
    return children(renderMinutes(durationMinutes));
  }
  if (durationHours > 0 && durationMinutes <= 0) {
    return children(renderHours(durationHours));
  }

  return children(
    Intl.formatList([
      renderHours(durationHours),
      renderMinutes(durationMinutes),
    ])
  );
};

FormattedDuration.propTypes = {
  value: PropTypes.number.isRequired,
  unitDisplay: PropTypes.oneOf(['long', 'narrow', 'short']),
};

FormattedDuration.defaultProps = {
  unitDisplay: 'long',
};

export default FormattedDuration;
