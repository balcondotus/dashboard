import React from 'react';
import PropTypes from 'prop-types';
import style from './SwitchBox.module.scss';

const SwitchBox = ({ defaultChecked, onChecked, id }) => {
  return (
    <div className={style.wrapper}>
      <input
        type="checkbox"
        id={id}
        className={style.switchbox}
        defaultChecked={defaultChecked}
        onChange={onChecked}
      />
      <label htmlFor={id} />
    </div>
  );
};

SwitchBox.propTypes = {
  defaultChecked: PropTypes.bool,
  onChecked: PropTypes.func,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

SwitchBox.defaultProps = {
  defaultChecked: false,
  onChecked: undefined,
};

export default SwitchBox;
