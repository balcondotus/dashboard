import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class HSFullScreen extends Component {
  HSFullScreenHTML;
  HSFullScreenObj;

  componentDidMount() {
    // Initialize HSFullscreen JavaScript library.
    this.HSFullScreenObj = new window.HSFullscreen(
      $(this.HSFullScreenHTML)
    ).init();
  }

  render() {
    return React.createElement(this.props.as, {
      'data-hs-fullscreen-options': JSON.stringify({
        targetEl: this.props.targetEl,
      }),
      className: this.props.className,
      ref: (e) => {
        this.HSFullScreenHTML = e;
      },
      children: this.props.children,
    });
  }
}

HSFullScreen.propTypes = {
  /**
   * Element which will be fullscreen
   *
   * @default `null`
   */
  targetEl: PropTypes.string.isRequired,
  /**
   * Disable scroll
   *
   * @default `.hs-fullscreen-on`
   */
  preventScrollClassName: PropTypes.string,
  /**
   * Class for target element
   *
   * @default `.hs-fullscreen`
   */
  toggleClassName: PropTypes.string,
  /**
   * Element for `preventScrollClassName`
   *
   * @default `body`
   */
  mainContainerSelector: PropTypes.string,
  /**
   * Build the HTML element as this type
   *
   * @default `div`
   */
  as: PropTypes.string,
};

HSFullScreen.defaultProps = {
  mainContainerSelector: 'body',
  toggleClassName: '.hs-fullscreen',
  preventScrollClassName: '.hs-fullscreen-on',
  as: 'div',
};
