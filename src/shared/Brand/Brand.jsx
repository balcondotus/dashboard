import React from 'react';

const Brand = ({ className, style }) => {
  return (
    <a
      href={process.env.REACT_APP_BALCON_WEBSITE_URI}
      target="_self"
      tabIndex={-1}
      className={className}
    >
      <img
        className="z-index-2"
        src="/assets/images/brand-logo.png?v=mtDO30pLMo"
        style={style}
      />
    </a>
  );
};

Brand.propTypes = {};

export default Brand;
