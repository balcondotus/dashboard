import React, { useRef, useEffect } from 'react';
import { FilePond } from 'react-filepond';
import PropTypes from 'prop-types';
import { withAuthUser } from 'hoc';
import { APP_CONST, REST_CONST } from 'consts';
import { toast } from 'react-toastify';
import { useIntl } from 'react-intl';
import { crossStorage } from 'utils';
import _ from 'lodash';
import Bugsnag from '@bugsnag/js';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import 'filepond/dist/filepond.min.css';
import './UploadableAvatar.module.scss';
import { FileOrigin } from 'filepond';

const UploadableAvatar = ({ authUser, onFileUploaded, ...props }) => {
  const intl = useIntl();
  const filePondRef = useRef();

  useEffect(() => {
    // Provide FilePond with user's avatar
    if (!_.isUndefined(authUser.profile.avatar)) {
      filePondRef.current.addFile(authUser.profile.avatar);
    }
  }, []);

  const onProcessFile = (error, file) => {
    if (error) {
      toast.error(
        intl.formatMessage({ id: 'profile.toast.error.unknown_error_occurred' })
      );
    } else {
      if (file.origin === FileOrigin.INPUT) {
        // Avatar has saved temporarily
        // It's time to make it permanently
        onFileUploaded(file.serverId);
      }
    }
  };

  const onError = (error, file, status) => {
    try {
      if (error instanceof Error) {
        toast.error(error.message);
      } else {
        toast.error(error.main);
      }
    } catch (e) {
      // TODO: Remove this try/catch if everything works as expected.
      Bugsnag.notify(
        new Error(
          `There is still an exception here: ${JSON.stringify({
            error,
            file,
            status,
          })}`
        )
      );

      toast.error(
        intl.formatMessage({ id: 'profile.toast.error.unknown_error_occurred' })
      );
    }
  };

  return (
    <FilePond
      ref={filePondRef}
      labelIdle='<span class="filepond--label-action"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="fa-search fa-w-16 fa-fw" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg></span>'
      imagePreviewHeight={120}
      onerror={onError}
      onprocessfile={onProcessFile}
      imageCropAspectRatio="1:1"
      stylePanelLayout="compact circle"
      maxFileSize="5MB"
      className="avatar-img"
      allowDrop={false}
      allowPaste={false}
      disabled={props.disabled || false}
      chunkUploads
      chunkForce
      chunkSize={500000}
      server={{
        url:
          process.env.REACT_APP_BALCON_BACKEND_URI +
          REST_CONST.RESOURCE.UPLOAD_AVATAR,
        headers: {
          Authorization: `Bearer ${crossStorage.getItem(
            APP_CONST.STORAGE_TOKEN_KEY,
            true
          )}`,
        },
      }}
      labelMaxFileSizeExceeded={intl.formatMessage({
        id: 'profile.form.avatar.max_file_exceeded.label',
      })}
      labelMaxFileSize={intl.formatMessage({
        id: 'profile.form.avatar.max_file_size.label',
      })}
      labelFileTypeNotAllowed={intl.formatMessage({
        id: 'profile.form.avatar.file_type_not_allowed.label',
      })}
      fileValidateTypeLabelExpectedTypes={intl.formatMessage({
        id: 'profile.form.avatar.expected_types.label',
      })}
      acceptedFileTypes={['image/jpeg', 'image/png', 'image/webp']}
      iconUndo='<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="trash-alt" class="fa-trash-alt fa-w-14 fa-fw" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>'
      iconRetry='<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="undo-alt" class="fa-undo-alt fa-w-16 fa-fw" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M255.545 8c-66.269.119-126.438 26.233-170.86 68.685L48.971 40.971C33.851 25.851 8 36.559 8 57.941V192c0 13.255 10.745 24 24 24h134.059c21.382 0 32.09-25.851 16.971-40.971l-41.75-41.75c30.864-28.899 70.801-44.907 113.23-45.273 92.398-.798 170.283 73.977 169.484 169.442C423.236 348.009 349.816 424 256 424c-41.127 0-79.997-14.678-110.63-41.556-4.743-4.161-11.906-3.908-16.368.553L89.34 422.659c-4.872 4.872-4.631 12.815.482 17.433C133.798 479.813 192.074 504 256 504c136.966 0 247.999-111.033 248-247.998C504.001 119.193 392.354 7.755 255.545 8z"></path></svg>'
      iconProcess='<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cloud-upload-alt" class="fa-cloud-upload-alt fa-w-20 fa-fw" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z"></path></svg>'
      iconRemove='<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="trash-alt" class="fa-trash-alt fa-w-14 fa-fw" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>'
      styleButtonRemoveItemPosition="bottom right"
      styleButtonProcessItemPosition="bottom left"
      styleLoadIndicatorPosition="center"
      styleProgressIndicatorPosition="center"
    />
  );
};

UploadableAvatar.propTypes = {
  authUser: PropTypes.object.isRequired,
  onFileUploaded: PropTypes.func.isRequired,
};

export default withAuthUser(UploadableAvatar);
