import React, { Component } from 'react';
import PropTypes from 'prop-types';
import jQuery from 'jquery';

export default class HSUnfold extends Component {
  HSUnfoldHTML;
  HSUnfoldObj;

  componentDidMount() {
    // Must be wrapped by jQuery to work.
    const invokerId = this.props.invokerId;
    jQuery(function () {
      // Initialize HSUnfold JavaScript library.
      this.HSUnfoldObj = new window.HSUnfold(`#${invokerId}`).init();
    });
  }

  render() {
    return React.createElement(this.props.as, {
      'data-hs-unfold-options': JSON.stringify({
        target: this.props.target,
        type: this.props.type,
      }),
      href: this.props.as === 'a' ? '#' : undefined,
      className: `${this.props.className}`,
      id: this.props.invokerId,
      ref: (e) => {
        this.HSUnfoldHTML = e;
      },
      onClick: (e) => {
        e.preventDefault();
      },
      children: this.props.children,
    });
  }
}

HSUnfold.propTypes = {
  target: PropTypes.string.isRequired,
  type: PropTypes.string,
  /**
   * Build the HTML element as this type
   *
   * @default `div`
   */
  as: PropTypes.string,
};

HSUnfold.defaultProps = {
  type: undefined,
  as: 'div',
};
