import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { IO } from 'services';
import { appConfig } from 'configs';
import { withIO } from 'hoc';
import { toast } from 'react-toastify';
import _ from 'lodash';
import { useIntl } from 'react-intl';
import styles from './OAuthButton.module.scss';
import { Button } from 'react-bootstrap';

const OAuthButton = ({
  provider,
  io,
  action,
  children,
  redirectTo,
  disabled,
  onClick,
  ...props
}) => {
  const [shouldDisable, setShouldDisable] = useState(disabled || false);
  const intl = useIntl();

  const handleClick = () => {
    if (!_.isUndefined(onClick)) onClick();

    let uri = null;

    setShouldDisable(true);

    if (['signin', 'signup', 'link'].includes(action)) {
      switch (provider) {
        case appConfig.supportedOauthProvider.google:
          uri =
            `${appConfig.AUTH.GOOGLE.URI}` +
            `?client_id=${appConfig.AUTH.GOOGLE.CLIENT_ID}` +
            `&redirect_uri=${encodeURIComponent(
              appConfig.AUTH.GOOGLE.REDIRECT_URI
            )}` +
            `&scope=${appConfig.AUTH.GOOGLE.SCOPE.join(' ')}` +
            `&prompt=${appConfig.AUTH.GOOGLE.PROMPT}` +
            `&response_type=${appConfig.AUTH.GOOGLE.RESPONSE_TYPE}` +
            `&include_granted_scopes=${appConfig.AUTH.GOOGLE.INCLUDE_GRANTED_SCOPES}`;
          break;
        case appConfig.supportedOauthProvider.facebook:
          uri =
            `${appConfig.AUTH.FACEBOOK.URI}` +
            `?client_id=${appConfig.AUTH.FACEBOOK.CLIENT_ID}` +
            `&redirect_uri=${appConfig.AUTH.FACEBOOK.REDIRECT_URI}` +
            `&scope=${appConfig.AUTH.FACEBOOK.SCOPE.join(' ')}` +
            `&response_type=${appConfig.AUTH.FACEBOOK.RESPONSE_TYPE}` +
            `&granted_scopes=${appConfig.AUTH.FACEBOOK.INCLUDE_GRANTED_SCOPES}`;
          break;
        default:
          throw new Error(`Provider (${provider}) is not implemented`);
      }

      uri += '&state=';
      const states = { provider, redirectTo, action };

      uri += JSON.stringify(states);

      window.location.assign(uri);
    } else {
      io.socket.unlinkOauth(provider, (result) => {
        if (result.error) {
          toast.error(result.error.message);
        } else {
          toast.success(
            intl.formatMessage({ id: 'account.toast.success.oauth_unlinked' })
          );
        }

        setShouldDisable(false);
      });
    }
  };

  const determineLogo = () => {
    switch (provider) {
      case 'google':
        return (
          <svg width="20" height="20" fill="none" viewBox="0 0 22 20">
            <path
              fill="#4285F4"
              fillRule="evenodd"
              d="M19.981 10.21c0-.654-.06-1.283-.172-1.886h-8.911v3.566h5.092c-.22 1.152-.886 2.128-1.888 2.782v2.313h3.058c1.79-1.605 2.821-3.968 2.821-6.776z"
              clipRule="evenodd"
            />
            <path
              fill="#34A853"
              fillRule="evenodd"
              d="M10.898 19.219c2.555 0 4.696-.826 6.262-2.234l-3.058-2.313c-.847.553-1.931.88-3.204.88-2.465 0-4.55-1.621-5.295-3.8H2.442v2.388c1.557 3.013 4.757 5.079 8.456 5.079z"
              clipRule="evenodd"
            />
            <path
              fill="#FBBC05"
              fillRule="evenodd"
              d="M5.603 11.752A5.414 5.414 0 015.307 10c0-.607.107-1.198.296-1.752V5.86H2.442a9.025 9.025 0 000 8.28l3.161-2.388z"
              clipRule="evenodd"
            />
            <path
              fill="#EA4335"
              fillRule="evenodd"
              d="M10.898 4.448c1.389 0 2.636.465 3.617 1.378l2.714-2.644c-1.64-1.487-3.78-2.4-6.331-2.4-3.699 0-6.9 2.065-8.456 5.078l3.161 2.388c.744-2.178 2.83-3.8 5.295-3.8z"
              clipRule="evenodd"
            />
          </svg>
        );
      case 'facebook':
        return (
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fab"
            data-icon="facebook-f"
            className="svg-inline--fa fa-facebook-f fa-w-10"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            color="#3b5998"
          >
            <path
              fill="currentColor"
              d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"
            />
          </svg>
        );

      default:
        throw new Error(`${provider} is not implemented`);
    }
  };

  return (
    <Button {...props} onClick={handleClick}>
      <span className={styles.logo}>{determineLogo()}</span>
      <span className={styles.label}>{children}</span>
    </Button>
  );
};

OAuthButton.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
  provider: PropTypes.oneOf(
    Object.keys(appConfig.supportedOauthProvider).map((provider) =>
      provider.toLowerCase()
    )
  ).isRequired,
  action: PropTypes.string.isRequired,
  children: PropTypes.node,
  redirectTo: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

OAuthButton.defaultProps = {
  children: undefined,
  disabled: false,
  onClick: undefined,
};

export default withIO(OAuthButton);
