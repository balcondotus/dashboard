export const appConfig = {
  contactDetail: {
    email: 'hello@balcon.us',
    phoneNumber: '+982128428629',
  },
  AUTH: {
    GOOGLE: {
      URI: 'https://accounts.google.com/o/oauth2/v2/auth',
      CLIENT_ID: process.env.REACT_APP_GOOGLE_CLIENT_ID,
      REDIRECT_URI: `${process.env.REACT_APP_BALCON_DASHBOARD_URI}/oauth/redirect`,
      RESPONSE_TYPE: 'token',
      SCOPE: [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile',
      ],
      PROMPT: 'consent',
      INCLUDE_GRANTED_SCOPES: true,
    },
    FACEBOOK: {
      URI: 'https://www.facebook.com/v6.0/dialog/oauth',
      CLIENT_ID: process.env.REACT_APP_FACEBOOK_CLIENT_ID,
      REDIRECT_URI: `${process.env.REACT_APP_BALCON_DASHBOARD_URI}/oauth/redirect`,
      RESPONSE_TYPE: 'token',
      SCOPE: ['email'],
      INCLUDE_GRANTED_SCOPES: true,
    },
  },
  supportedOauthProvider: { facebook: 'facebook', google: 'google' },
  validation: {
    minPassword: 6,
    phoneVerificationCodeLength: 6,
    usernameMinLength: 3,
    usernameMaxLength: 15,
    eventRateMaxLength: 9999999,
    usernamePattern: /^[a-z0-9_]+$/i,
  },
  supportedLocales: ['fa', 'en'],
  allowedDisputeHours: 48,
};
