import React from 'react';
import { I18nContext } from 'contexts';

export default (Component) => (props) => (
  <I18nContext.Consumer>
    {(value) => <Component i18n={value} {...props} />}
  </I18nContext.Consumer>
);
