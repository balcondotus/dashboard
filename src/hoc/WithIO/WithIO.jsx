import React from 'react';
import { IOContext } from 'contexts';

export default (Component) => (props) => (
  <IOContext.Consumer>{(io) => <Component {...props} io={io} />}</IOContext.Consumer>
);
