import React from 'react';
import { Route, Redirect, useLocation } from 'react-router-dom';
import { AuthUserContext } from 'contexts';
import { APP_CONST } from 'consts';
import qs from 'query-string';
import _ from 'lodash';

const GuardRoute = ({ component: Component, ...rest }) => {
  const location = useLocation();
  const queryParams = qs.parse(location.search);

  return (
    <AuthUserContext.Consumer>
      {(authUser) => (
        <Route
          {...rest}
          render={(props) =>
            !authUser ? (
              <Component {...props} />
            ) : _.isUndefined(queryParams.redirectTo) ? (
              <Redirect
                to={{
                  pathname: APP_CONST.ROUTES.HOME,
                  state: { from: props.location },
                }}
              />
            ) : (
              (window.location.href = queryParams.redirectTo)
            )
          }
        />
      )}
    </AuthUserContext.Consumer>
  );
};

export default GuardRoute;
