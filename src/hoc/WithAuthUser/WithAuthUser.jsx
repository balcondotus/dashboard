import React from "react";
import { AuthUserContext } from "../../contexts";

export default (Component) => {
  function WithAuthUser(props) {
    return (
      <AuthUserContext.Consumer>
        {(authUser) => <Component {...props} authUser={authUser} />}
      </AuthUserContext.Consumer>
    );
  }
  return WithAuthUser;
};
