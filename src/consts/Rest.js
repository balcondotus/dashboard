export const REST_CONST = {
  RESOURCE: {
    SIGNIN: '/account/signin',
    SIGNUP: '/account/signup',
    FORGOT_PASSWORD: '/account/password/forgot',
    RESET_PASSWORD: '/account/password/reset',
    MEETING: {
      CANCEL: '/meeting/cancel',
      DISPUTE: '/meeting/dispute',
    },
    VERIFY_EMAIL: '/account/email/verify',
    AUTH: {
      GOOGLE: '/oauth/google',
      FACEBOOK: '/oauth/facebook',
    },
    UPLOAD_AVATAR: '/account/upload/avatar',
  },
};
