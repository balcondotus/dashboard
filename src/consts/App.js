export const APP_CONST = {
  STORAGE_AUTH_USER_KEY: 'authUser',
  STORAGE_TOKEN_KEY: 'token',
  ROUTES: {
    HOME: '/',
    SIGNIN: '/signin',
    SIGNUP: '/signup',
    OAUTH_REDIRECT: '/oauth/redirect',
    SIGNOUT: '/signout',
    FORGOT_PASSWORD: '/password/forgot',
    RESET_PASSWORD: '/password/reset',
    PAYMENTS: '/payments',
    SETTINGS: '/settings',
    NOTIFICATION_SETTINGS: '/settings/notification',
    ACCOUNT_SETTINGS: '/settings/account',
    FINANCE_SETTINGS: '/settings/finance',
    PROFILE_SETTINGS: '/settings/profile',
    CALENDAR_SETTINGS: '/settings/calendar',
    VERIFY_EMAIL: '/email/verify',
    TERMS_OF_USE: `${process.env.REACT_APP_BALCON_WEBSITE_URI}/terms-of-use`,
    PRIVACY: `${process.env.REACT_APP_BALCON_WEBSITE_URI}/privacy`,
    FAQ: `${process.env.REACT_APP_BALCON_WEBSITE_URI}/faq`,
    ABOUT: `${process.env.REACT_APP_BALCON_WEBSITE_URI}/about`,
    BLOG: 'https://virgool.io/balcondotus',
    /**
     * @param {String} id Unique ID including userID or username
     */
    USER_PROFILE: (id) => {
      return `${process.env.REACT_APP_BALCON_WEBSITE_URI}/${id}`;
    },
    MEETING: {
      DISPUTE: (meetingId) => {
        return `${process.env.REACT_APP_BALCON_MEETING_URI}/${meetingId}/dispute`;
      },
      JOIN: (meetingId) => {
        return `${process.env.REACT_APP_BALCON_MEETING_URI}/${meetingId}`;
      },
      RESCHEDULE: (hostIdentifier, meetingId) => {
        return `${process.env.REACT_APP_BALCON_WEBSITE_URI}/${hostIdentifier}?reschedule=${meetingId}`;
      },
      SCHEDULE: (userIdentifier) => {
        return `${process.env.REACT_APP_BALCON_WEBSITE_URI}/${userIdentifier}`;
      },
      CANCEL: (meetingId) => {
        return `${process.env.REACT_APP_BALCON_MEETING_URI}/${meetingId}/cancel`;
      },
    },
    SOCIAL_MEDIA: {
      INSTAGRAM: 'https://www.instagram.com/balcondotus/',
      TWITTER: 'https://www.twitter.com/balcondotus/',
      LINKEDIN: 'https://www.linkedin.com/company/balcondotus/',
    },
  },
};
