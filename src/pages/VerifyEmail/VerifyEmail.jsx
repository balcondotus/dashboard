import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { IO } from 'services';
import { parse } from 'query-string';
import { toast } from 'react-toastify';
import { Container, Alert, Row, Col } from 'react-bootstrap';
import { withIO } from 'hoc';
import { FormattedMessage, useIntl } from 'react-intl';
import _ from 'lodash';

const VerifyEmail = ({ io, location }) => {
  const [error, setError] = useState(null);
  const [verified, setVerified] = useState(false);
  const intl = useIntl();

  const verify = (token) => {
    setError(null);

    const toastId = toast.info(
      intl.formatMessage({ id: 'verify_email.toast.info.verifying' }),
      { autoClose: false }
    );

    io.rest
      .verifyEmail(token)
      .then((response) => {
        if (response.error) {
          setError(response.error.message);
          toast.update(toastId, {
            type: toast.TYPE.ERROR,
            autoClose: true,
            render: intl.formatMessage({
              id: 'verify_email.toast.error.something_wrong_happened',
            }),
          });
        } else {
          setError(null);
          setVerified(true);

          toast.update(toastId, {
            type: toast.TYPE.SUCCESS,
            autoClose: true,
            render: intl.formatMessage({
              id: 'verify_email.toast.success.verified',
            }),
          });
        }
      })
      .catch((e) => {
        setError(e.message);
        toast.update(toastId, {
          type: toast.TYPE.ERROR,
          autoClose: true,
          render: intl.formatMessage({
            id: 'verify_email.toast.error.something_wrong_happened',
          }),
        });
      });
  };

  useEffect(() => {
    const queries = parse(location.search);
    const { token } = queries;

    if (!_.isString(token))
      return setError(
        intl.formatMessage({
          id: 'verify_email.toast.error.no_token',
        })
      );

    verify(token);
  }, []);

  return (
    <Container>
      <Row>
        {error ? (
          <Col xs={12}>
            <Alert variant="danger">{error}</Alert>
          </Col>
        ) : (
          <Col xs={12}>
            {verified ? (
              <>
                <h2>
                  <FormattedMessage id="verify_email.verified.title" />
                </h2>
                <p className="text-muted">
                  <FormattedMessage id="verify_email.verified.desc" />
                </p>
              </>
            ) : (
              <>
                <h2>
                  <FormattedMessage id="verify_email.verifying.title" />
                </h2>
                <p className="text-muted">
                  <FormattedMessage id="verify_email.verifying.desc" />
                </p>
              </>
            )}
          </Col>
        )}
      </Row>
    </Container>
  );
};

VerifyEmail.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
  location: PropTypes.object.isRequired,
};

export default withIO(VerifyEmail);
