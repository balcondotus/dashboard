import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const Empty = ({ tab }) => {
  return (
    <div className="card-body text-center">
      <img
        className="avatar avatar-xxl mb-3"
        src="/assets/images/yelling.svg"
        alt="No Meetings"
      />
      <p>
        <FormattedMessage id="meetings.row.empty.no_meetings" />
      </p>
    </div>
  );
};

Empty.propTypes = {
  tab: PropTypes.any.isRequired,
};

export default Empty;
