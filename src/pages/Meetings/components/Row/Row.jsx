import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Container, Row as BRow, Col, Dropdown } from 'react-bootstrap';
import {
  faSignInAlt,
  faEllipsisV,
  faSyncAlt,
} from '@fortawesome/free-solid-svg-icons';
import { DateUtils } from 'utils';
import { appConfig } from 'configs';
import { withAuthUser } from 'hoc';
import {
  FormattedTime,
  FormattedDate,
  FormattedMessage,
  useIntl,
} from 'react-intl';
import { APP_CONST } from 'consts';
import { Action, CancelModal, DisputeModal } from './components';
import {
  faCalendar,
  faFlag,
  faTrashAlt,
} from '@fortawesome/free-regular-svg-icons';
import { HSUnfold } from 'shared';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Row.scss';
import _ from 'lodash';
import moment from 'moment';

const Row = ({ meeting, authUser, onCancel, onDispute }) => {
  const intl = useIntl();
  const otherPerson =
    authUser._id === meeting.invitee._id ? meeting.host : meeting.invitee;
  const [cancelMeeting, setCancelMeeting] = useState();
  const [disputeMeeting, setDisputeMeeting] = useState();

  return (
    <div className="row-component">
      <Container
        fluid
        className={`meeting-wrapper py-4 px-4 ${
          meeting.status === 'CANCELED' ? 'decoration-line-through' : ''
        }`}
      >
        <div className="hs-unfold d-md-none">
          <HSUnfold
            className="btn btn-sm btn-icon btn-ghost-secondary rounded-circle"
            target={`#meetingOptionsDropdown${meeting._id}`}
            invokerId={`meetingOptionsDropdown${meeting._id}Invoker`}
            type="css-animation"
          >
            <FontAwesomeIcon icon={faEllipsisV} />
          </HSUnfold>
          <div
            id={`meetingOptionsDropdown${meeting._id}`}
            className="hs-unfold-content dropdown-unfold dropdown-menu hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated"
            data-hs-target-height="0"
            data-hs-unfold-content=""
            data-hs-unfold-content-animation-in="slideInUp"
            data-hs-unfold-content-animation-out="fadeOut"
            style={{ animationDuration: '300ms' }}
          >
            <Dropdown.Header>
              <FormattedMessage id="meetings.row.dropdown.options.meeting_options.label" />
            </Dropdown.Header>

            <Dropdown.Item
              href={APP_CONST.ROUTES.MEETING.JOIN(meeting._id)}
              target="_blank"
              className={`${
                meeting.status === 'CREATED' &&
                !DateUtils.isPast(meeting.time.end)
                  ? 'd-block'
                  : 'd-none'
              }`}
            >
              <FontAwesomeIcon
                icon={faSignInAlt}
                fixedWidth
                className="dropdown-item-icon"
              />
              <FormattedMessage id="meetings.row.dropdown.options.join_meeting.label" />
            </Dropdown.Item>

            <Dropdown.Item
              onClick={(e) => setCancelMeeting(meeting)}
              className={`${
                meeting.status === 'CREATED' &&
                !DateUtils.isPast(meeting.time.start)
                  ? 'd-block'
                  : 'd-none'
              }`}
            >
              <FontAwesomeIcon
                icon={faTrashAlt}
                fixedWidth
                className="dropdown-item-icon"
              />
              <FormattedMessage id="meetings.row.dropdown.options.cancel_meeting.label" />
            </Dropdown.Item>

            <Dropdown.Item
              href={APP_CONST.ROUTES.MEETING.RESCHEDULE(
                meeting.host.identifier,
                meeting._id
              )}
              target="_blank"
              className={`${
                meeting.status === 'CREATED' &&
                !DateUtils.isPast(meeting.time.end)
                  ? 'd-block'
                  : 'd-none'
              }`}
            >
              <FontAwesomeIcon
                icon={faSyncAlt}
                fixedWidth
                className="dropdown-item-icon"
              />
              <FormattedMessage id="meetings.row.dropdown.options.reschedule.label" />
            </Dropdown.Item>

            <Dropdown.Item
              href={APP_CONST.ROUTES.MEETING.SCHEDULE(otherPerson.identifier)}
              target="_blank"
              className={`${
                (meeting.status === 'CREATED' &&
                  DateUtils.isPast(meeting.time.end)) ||
                meeting.status !== 'CREATED'
                  ? 'd-block'
                  : 'd-none'
              }`}
            >
              <FontAwesomeIcon
                icon={faCalendar}
                fixedWidth
                className="dropdown-item-icon"
              />
              <FormattedMessage id="meetings.row.dropdown.options.book_again.label" />
            </Dropdown.Item>

            <Dropdown.Divider
              className={`${
                meeting.status === 'CREATED' &&
                !DateUtils.isHoursPast(
                  meeting.time.start,
                  appConfig.allowedDisputeHours
                )
                  ? 'd-block'
                  : 'd-none'
              }`}
            />

            <Dropdown.Header
              className={`${
                meeting.status === 'CREATED' &&
                !DateUtils.isHoursPast(
                  meeting.time.start,
                  appConfig.allowedDisputeHours
                )
                  ? 'd-block'
                  : 'd-none'
              }`}
            >
              <FormattedMessage id="meetings.row.dropdown.options.report.label" />
            </Dropdown.Header>
            <Dropdown.Item
              className={`${
                meeting.status === 'CREATED' &&
                moment
                  .utc()
                  .within(
                    moment.range(
                      moment.utc(meeting.time.start),
                      moment
                        .utc(meeting.time.start)
                        .add(appConfig.allowedDisputeHours, 'hour')
                    )
                  )
                  ? 'd-block'
                  : 'd-none'
              }`}
              onClick={(e) => setDisputeMeeting(meeting)}
              target="_blank"
            >
              <FontAwesomeIcon
                icon={faFlag}
                className="dropdown-item-icon"
                fixedWidth
              />
              <FormattedMessage id="meetings.row.dropdown.options.dispute.label" />
            </Dropdown.Item>
          </div>
        </div>
        <BRow className="mb-3">
          <Col className="text-right">
            <span className="d-block h4 mb-0 text-success">
              <FormattedTime
                value={meeting.time.start}
                hour="2-digit"
                minute="2-digit"
              />
            </span>
            <span className="d-block text-secondary">
              <FormattedMessage id="meetings.row.start_time.label" />
            </span>
          </Col>

          <Col className="column-divider text-left">
            <span className="d-block h4 mb-0">
              <FormattedTime
                value={meeting.time.end}
                hour="2-digit"
                minute="2-digit"
              />
            </span>
            <span className="d-block text-secondary">
              <FormattedMessage id="meetings.row.end_time.label" />
            </span>
          </Col>
        </BRow>
        <BRow className="align-items-center">
          <Col xs={12} sm={8} md={8} lg={10}>
            <h5 className="name font-weight-bold mb-2 text-center text-sm-left">
              {otherPerson.profile.fullName}
            </h5>
            <p className="answer text-secondary text-center text-sm-left">
              {meeting.answers[0].answer}
            </p>
            <div>
              {meeting.status === 'RESCHEDULED' ? (
                <>
                  <div className="my-0 small">
                    <span className="text-info font-weight-bold">
                      <FormattedMessage
                        id="meetings.user_changed_meeting_time"
                        values={{
                          name: meeting.rescheduled.user.profile.firstName,
                        }}
                      />
                    </span>
                    <p>{meeting.rescheduled.reason}</p>
                  </div>
                  <div className="my-0 small">
                    <span className="text-info font-weight-bold">
                      <FormattedMessage id="meetings.meeting.new_time" />
                    </span>
                    <p>
                      <FormattedDate
                        value={meeting.rescheduled.rescheduledTo.time.start}
                        day="numeric"
                        month="long"
                        year="numeric"
                        hour="2-digit"
                        minute="2-digit"
                      />
                    </p>
                  </div>
                </>
              ) : null}
              {meeting.status === 'CANCELED' ? (
                <div className="my-0 small">
                  <span className="text-info font-weight-bold">
                    <FormattedMessage
                      id="meetings.user_canceled_meeting"
                      values={{ name: meeting.canceled.user.profile.firstName }}
                    />
                  </span>
                  <p>{meeting.canceled.reason}</p>
                </div>
              ) : null}
            </div>
          </Col>
          <Col
            xs={12}
            sm={4}
            md={4}
            lg={2}
            className="actions-wrapper text-center d-none d-md-block"
          >
            <div className="d-flex align-items-center mt-4 mt-md-0 justify-content-around">
              <Action
                show={
                  meeting.status === 'CREATED' &&
                  !DateUtils.isPast(meeting.time.end)
                }
                tooltip={intl.formatMessage({
                  id: 'meetings.list.item.reschedule.tooltip',
                })}
                icon={faSyncAlt}
                link={APP_CONST.ROUTES.MEETING.RESCHEDULE(
                  meeting.host.identifier,
                  meeting._id
                )}
              />
              <Action
                show={
                  meeting.status === 'CREATED' &&
                  !DateUtils.isPast(meeting.time.start)
                }
                tooltip={intl.formatMessage({
                  id: 'meetings.list.item.cancel.tooltip',
                })}
                icon={faTrashAlt}
                onClick={(e) => setCancelMeeting(meeting)}
              />
              <Action
                show={
                  (meeting.status === 'CREATED' &&
                    DateUtils.isPast(meeting.time.end)) ||
                  meeting.status !== 'CREATED'
                }
                tooltip={intl.formatMessage(
                  {
                    id: 'meetings.list.item.book_time.tooltip',
                  },
                  { otherPartyFirstName: otherPerson.profile.firstName }
                )}
                link={APP_CONST.ROUTES.MEETING.SCHEDULE(otherPerson.identifier)}
                icon={faCalendar}
              />
              <Action
                show={
                  meeting.status === 'CREATED' &&
                  !DateUtils.isPast(meeting.time.end)
                }
                tooltip={intl.formatMessage({
                  id: 'meetings.list.item.join_meeting.tooltip',
                })}
                link={APP_CONST.ROUTES.MEETING.JOIN(meeting._id)}
                icon={faSignInAlt}
              />
              <Action
                show={
                  meeting.status === 'CREATED' &&
                  moment
                    .utc()
                    .within(
                      moment.range(
                        moment.utc(meeting.time.start),
                        moment
                          .utc(meeting.time.start)
                          .add(appConfig.allowedDisputeHours, 'hour')
                      )
                    )
                }
                tooltip={intl.formatMessage({
                  id: 'meetings.list.item.dispute.tooltip',
                })}
                onClick={(e) => setDisputeMeeting(meeting)}
                icon={faFlag}
              />
            </div>
          </Col>
        </BRow>
      </Container>
      <CancelModal
        show={!_.isUndefined(cancelMeeting)}
        meeting={cancelMeeting}
        onHide={() => setCancelMeeting(undefined)}
        onCancel={(canceledMeeting) => {
          onCancel(canceledMeeting);
        }}
      />
      <DisputeModal
        show={!_.isUndefined(disputeMeeting)}
        meeting={disputeMeeting}
        onHide={() => setDisputeMeeting(undefined)}
        onDispute={(disputedMeeting) => {
          onDispute(disputedMeeting);
        }}
      />
    </div>
  );
};

Row.propTypes = {
  meeting: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  onDispute: PropTypes.func.isRequired,
};

export default withAuthUser(Row);
