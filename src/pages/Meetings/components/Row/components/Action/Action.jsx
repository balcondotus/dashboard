import React from 'react';
import PropTypes from 'prop-types';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import _ from 'lodash';

const Action = ({ show, tooltip, tooltipPlacement, link, icon, onClick }) => (
  <div className={`${!show ? 'd-none' : 'd-block'}`}>
    <OverlayTrigger
      placement={tooltipPlacement}
      delay={{ show: 250, hide: 400 }}
      overlay={<Tooltip>{tooltip}</Tooltip>}
    >
      <Button
        as={_.isUndefined(link) ? 'button' : 'a'}
        href={link}
        variant="white"
        size="sm"
        className="btn text-primary"
        target="_blank"
        onClick={(e) => {
          if (!_.isUndefined(onClick)) {
            e.preventDefault();
            onClick(e);
          }
        }}
      >
        <FontAwesomeIcon icon={icon} />
      </Button>
    </OverlayTrigger>
  </div>
);

Action.propTypes = {
  show: PropTypes.bool,
  tooltip: PropTypes.string.isRequired,
  tooltipPlacement: PropTypes.string,
  link: PropTypes.string,
  icon: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

Action.defaultProps = {
  tooltipPlacement: 'top',
  show: true,
  link: undefined,
};

export default Action;
