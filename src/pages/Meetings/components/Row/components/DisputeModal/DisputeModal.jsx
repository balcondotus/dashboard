import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal } from 'react-bootstrap';
import { withAuthUser, withIO } from 'hoc';
import { toast } from 'react-toastify';
import { FormattedMessage, useIntl } from 'react-intl';
import { compose } from 'recompose';
import moment from 'moment';

const DisputeModal = ({ io, meeting, show, onHide, onDispute, authUser }) => {
  const [isDisputing, setIsDisputing] = useState(false);
  const [reason, setReason] = useState();
  const intl = useIntl();
  const otherParty =
    meeting?.host._id === authUser._id ? meeting?.invitee : meeting?.host;

  useEffect(() => {
    if (!show) {
      // Reset to defaults when it hides.
      setIsDisputing(false);
    }
  }, [show]);

  const disputeMeeting = (e) => {
    e.preventDefault();

    setIsDisputing(true);

    io.rest
      .disputeMeeting(meeting._id, reason)
      .then((result) => {
        if (result.error) {
          toast.error(result.error.message);
        } else {
          toast.success(
            intl.formatMessage({
              id: 'meetings.modal.dispute_meeting.toast.success.disputed',
            })
          );

          if (!_.isUndefined(onDispute)) {
            onDispute(result.data);
          }
          onHide();
        }
      })
      .catch((e) => {
        toast.error(e.message);
      })
      .finally(() => setIsDisputing(false));
  };

  return (
    <Modal show={show} onHide={onHide}>
      <Form onSubmit={disputeMeeting} disabled={isDisputing} autoComplete="off">
        <Modal.Header>
          <Modal.Title>
            <FormattedMessage
              values={{
                otherPartyFirstName: otherParty?.profile.firstName,
                meetingDate: moment.utc(meeting?.time.start).toDate(),
              }}
              id="meetings.modal.dispute_meeting.title"
            />
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group controlId="reasonFormControl">
            <Form.Label>
              <FormattedMessage id="meetings.modal.dispute_meeting.form.reason.label" />
            </Form.Label>
            <Form.Control
              as="textarea"
              required
              style={{ minHeight: '100px' }}
              dir="auto"
              onChange={(e) => setReason(e.target.value)}
              maxLength={255}
              placeholder={intl.formatMessage({
                id: 'meetings.modal.dispute_meeting.form.reason.placeholder',
              })}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer className="d-flex align-items-center justify-content-end">
          <Button
            type="button"
            variant="white"
            disabled={isDisputing}
            style={{ transition: 'none' }}
            onClick={onHide}
          >
            <FormattedMessage id="meetings.modal.dispute_meeting.form.btn.cancel" />
          </Button>
          <Button type="submit" variant="danger" disabled={isDisputing}>
            <FormattedMessage id="meetings.modal.dispute_meeting.form.btn.dispute_meeting" />
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

DisputeModal.propTypes = {
  meeting: PropTypes.object,
  show: PropTypes.bool,
  onDispute: PropTypes.func,
  onHide: PropTypes.func.isRequired,
};

DisputeModal.defaultProps = {
  show: false,
  meeting: undefined,
  onDispute: undefined,
};

export default compose(withIO, withAuthUser)(DisputeModal);
