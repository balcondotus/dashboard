import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal } from 'react-bootstrap';
import { withAuthUser, withIO } from 'hoc';
import { toast } from 'react-toastify';
import { FormattedMessage, useIntl } from 'react-intl';
import { compose } from 'recompose';
import moment from 'moment';

const CancelModal = ({ io, meeting, show, onHide, onCancel, authUser }) => {
  const [isCanceling, setIsCanceling] = useState(false);
  const [reason, setReason] = useState();
  const intl = useIntl();
  const otherParty =
    meeting?.host._id === authUser._id ? meeting?.invitee : meeting?.host;

  useEffect(() => {
    if (!show) {
      // Reset to defaults when it hides.
      setIsCanceling(false);
    }
  }, [show]);

  const cancelMeeting = (e) => {
    e.preventDefault();

    setIsCanceling(true);

    io.rest
      .cancelMeeting(meeting._id, reason)
      .then((result) => {
        if (result.error) {
          toast.error(result.error.message);
        } else {
          toast.success(
            intl.formatMessage({
              id: 'meetings.modal.cancel_meeting.toast.success.canceled',
            })
          );

          if (!_.isUndefined(onCancel)) {
            onCancel(result.data);
          }
          onHide();
        }
      })
      .catch((e) => {
        toast.error(e.message);
      })
      .finally(() => setIsCanceling(false));
  };

  return (
    <Modal show={show} onHide={onHide}>
      <Form onSubmit={cancelMeeting} disabled={isCanceling} autoComplete="off">
        <Modal.Header>
          <Modal.Title>
            <FormattedMessage
              values={{
                otherPartyFirstName: otherParty?.profile.firstName,
                meetingDate: moment.utc(meeting?.time.start).toDate(),
              }}
              id="meetings.modal.cancel_meeting.title"
            />
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group controlId="reasonFormControl">
            <Form.Label>
              <FormattedMessage id="meetings.modal.cancel_meeting.form.reason.label" />
            </Form.Label>
            <Form.Control
              as="textarea"
              required
              style={{ minHeight: '100px' }}
              dir="auto"
              onChange={(e) => setReason(e.target.value)}
              maxLength={255}
              placeholder={intl.formatMessage({
                id: 'meetings.modal.cancel_meeting.form.reason.placeholder',
              })}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer className="d-flex align-items-center justify-content-end">
          <Button
            type="button"
            variant="white"
            disabled={isCanceling}
            style={{ transition: 'none' }}
            onClick={onHide}
          >
            <FormattedMessage id="meetings.modal.cancel_meeting.form.btn.cancel" />
          </Button>
          <Button type="submit" variant="danger" disabled={isCanceling}>
            <FormattedMessage id="meetings.modal.cancel_meeting.form.btn.cancel_meeting" />
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

CancelModal.propTypes = {
  meeting: PropTypes.object,
  show: PropTypes.bool,
  onCancel: PropTypes.func,
  onHide: PropTypes.func.isRequired,
};

CancelModal.defaultProps = {
  show: false,
  meeting: undefined,
  onCancel: undefined,
};

export default compose(withIO, withAuthUser)(CancelModal);
