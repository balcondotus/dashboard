import React, { useState, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import { Container, Row as BRow, Card, Nav, Col } from 'react-bootstrap';
import _ from 'lodash';
import { Helmet } from 'react-helmet-async';
import moment from 'jalali-moment';
import { withIO, withAuthUser } from 'hoc';
import { IO } from 'services';
import {
  FormattedDate,
  FormattedMessage,
  FormattedNumber,
  useIntl,
} from 'react-intl';
import { compose } from 'recompose';
import { APP_CONST } from 'consts';
import { Link } from 'react-router-dom';
import { TAB } from './Tab';
import { Empty, Row } from './components';
import './Meetings.scss';
import { MEETINGS_REDUCER_TYPE } from './MeetingsReducerTypes';
import BeatLoader from 'react-spinners/BeatLoader';

function meetingsReducer(state, action) {
  const { type, data } = action;
  switch (type) {
    case MEETINGS_REDUCER_TYPE.RESET:
      return data;
    case MEETINGS_REDUCER_TYPE.UPDATE:
      const updatedMeetings = _.clone(state);
      const formattedMeetingTime = moment
        .utc(data.time.start)
        .format('YYYY-MM-DD');
      const meetingIndex = _.findIndex(
        updatedMeetings[formattedMeetingTime],
        (meeting) => meeting._id === data._id
      );

      updatedMeetings[formattedMeetingTime][meetingIndex] = data;

      return updatedMeetings;
    default:
      throw new Error(`${type} is not implemented`);
  }
}

const Meeting = ({ io, authUser }) => {
  const [activeTab, setActiveTab] = useState(TAB.UPCOMING);
  const [meetings, dispatchMeetings] = useReducer(meetingsReducer, {});
  const [fetching, setFetching] = useState(false);
  const intl = useIntl();

  const _handleIncomingMeetings = (result) => {
    dispatchMeetings({ type: MEETINGS_REDUCER_TYPE.RESET, data: result.data });

    setFetching(false);
  };

  useEffect(() => {
    if (fetching) return;
    setFetching(true);

    switch (activeTab) {
      case TAB.UPCOMING:
        io.socket.getMeetings(
          moment.utc().toDate(),
          null,
          _handleIncomingMeetings
        );
        break;
      case TAB.PAST:
        io.socket.getMeetings(
          null,
          moment.utc().toDate(),
          _handleIncomingMeetings
        );
        break;
      default:
        throw new Error(`Tab ${activeTab} is not implemented`);
    }
  }, [activeTab]);

  const prepareContent = () => {
    if (fetching) {
      return (
        <div className="text-center py-5">
          <BeatLoader size={20} color="#377dff" />
        </div>
      );
    }

    if (_.isEmpty(meetings)) {
      return <Empty tab={activeTab} />;
    }

    return Object.entries(meetings).map(([date, groupedMeetings]) => {
      return (
        <React.Fragment key={date}>
          <div className="datetime-wrapper">
            <Col className="py-3 px-4 bg-light">
              <FormattedDate
                value={date}
                month="long"
                day="numeric"
                year="numeric"
              />
            </Col>
          </div>
          {groupedMeetings.map((meeting) => {
            return (
              <Row
                meeting={meeting}
                key={meeting._id}
                onCancel={(canceledMeeting) =>
                  dispatchMeetings({
                    type: MEETINGS_REDUCER_TYPE.UPDATE,
                    data: canceledMeeting,
                  })
                }
                onDispute={(disputedMeeting) =>
                  dispatchMeetings({
                    type: MEETINGS_REDUCER_TYPE.UPDATE,
                    data: disputedMeeting,
                  })
                }
              />
            );
          })}
        </React.Fragment>
      );
    });
  };

  return (
    <div className="meeting-component">
      <Helmet>
        <title>{intl.formatMessage({ id: 'meetings.title' })}</title>
      </Helmet>

      <Container>
        <BRow>
          <Col>
            <ul className="step step-md step-centered">
              <li className="step-item">
                <div className="step-content-wrapper">
                  <span className="step-icon step-icon-soft-primary">
                    <FormattedNumber value={1} />
                  </span>
                  <div className="step-content">
                    <h4 className="step-title">
                      <FormattedMessage id="meetings.steps.first.heading" />
                    </h4>
                    <p className="text-body">
                      <FormattedMessage
                        id="meetings.steps.first.subheading"
                        values={{
                          profileSettingsLink: (chunks) => (
                            <Link to={APP_CONST.ROUTES.PROFILE_SETTINGS}>
                              {chunks}
                            </Link>
                          ),
                        }}
                      />
                    </p>
                  </div>
                </div>
              </li>

              <li className="step-item">
                <div className="step-content-wrapper">
                  <span className="step-icon step-icon-soft-primary">
                    <FormattedNumber value={2} />
                  </span>
                  <div className="step-content">
                    <h4 className="step-title">
                      <FormattedMessage id="meetings.steps.second.heading" />
                    </h4>
                    <p className="text-body">
                      <FormattedMessage
                        id="meetings.steps.second.subheading"
                        values={{
                          calendarSettingsLink: (chunks) => (
                            <Link to={APP_CONST.ROUTES.CALENDAR_SETTINGS}>
                              {chunks}
                            </Link>
                          ),
                          profileSettingsLink: (chunks) => (
                            <Link to={APP_CONST.ROUTES.PROFILE_SETTINGS}>
                              {chunks}
                            </Link>
                          ),
                        }}
                      />
                    </p>
                  </div>
                </div>
              </li>

              <li className="step-item">
                <div className="step-content-wrapper">
                  <span className="step-icon step-icon-soft-primary">
                    <FormattedNumber value={3} />
                  </span>
                  <div className="step-content">
                    <h4 className="step-title">
                      <FormattedMessage id="meetings.steps.third.heading" />
                    </h4>
                    <p className="text-body">
                      <FormattedMessage
                        id="meetings.steps.third.subheading"
                        values={{
                          profileLink: (chunks) => (
                            <a
                              href={APP_CONST.ROUTES.USER_PROFILE(
                                authUser.identifier
                              )}
                              target="blank"
                            >
                              {chunks}
                            </a>
                          ),
                        }}
                      />
                    </p>
                  </div>
                </div>
              </li>
            </ul>
          </Col>
        </BRow>
        <BRow>
          <Col>
            <Card>
              <Card.Header className="bg-white p-0">
                <div className="m-0 d-flex align-items-center">
                  <Nav.Link
                    onClick={() => setActiveTab(TAB.UPCOMING)}
                    className={`border-0 ${
                      activeTab === TAB.UPCOMING ? 'active' : ''
                    }`}
                  >
                    <FormattedMessage id="meetings.nav.upcoming.title" />
                  </Nav.Link>
                  <Nav.Link
                    onClick={() => setActiveTab(TAB.PAST)}
                    className={`border-0 ${
                      activeTab === TAB.PAST ? 'active' : ''
                    }`}
                  >
                    <FormattedMessage id="meetings.nav.past.title" />
                  </Nav.Link>
                </div>
              </Card.Header>
              <Card.Body className={`p-0 ${fetching ? 'bg-light' : ''}`}>
                {prepareContent()}
              </Card.Body>
            </Card>
          </Col>
        </BRow>
      </Container>
    </div>
  );
};

Meeting.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withAuthUser, withIO)(Meeting);
