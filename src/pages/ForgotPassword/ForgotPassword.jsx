import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Container, Form, Button, Row, Col, Card } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { IO } from 'services';
import { withI18n, withIO } from 'hoc';
import { Helmet } from 'react-helmet-async';
import { FormattedMessage, useIntl } from 'react-intl';
import { APP_CONST } from 'consts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { compose } from 'recompose';
import { Brand } from 'shared';
import validator from 'validator';

const ForgotPassword = ({ io, i18n }) => {
  const [email, setEmail] = useState('');
  const [isSending, setIsSending] = useState(false);
  const intl = useIntl();

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const forgotPassword = async (e) => {
    e.preventDefault();

    setIsSending(true);

    try {
      const result = await io.rest.forgotPassword(email);

      if (!result.error)
        return toast.success(
          intl.formatMessage({
            id: 'forgot_password.toast.success.sent',
          })
        );

      toast.error(result.error.message);
    } catch (e) {
      toast.error(e.message);
    } finally {
      setIsSending(false);
    }
  };

  /**
   * Checks whether the form and submit button should be disabled or not.
   */
  const shouldDisableForm = () => isSending || !validator.isEmail(email);

  return (
    <Container className="py-5">
      <Helmet>
        <title>{intl.formatMessage({ id: 'forgot_password.title' })}</title>
      </Helmet>

      <Row className="justify-content-center">
        <Col xs={12} className="text-center mb-5">
          <Brand style={{ width: '8rem' }} />
        </Col>
        <Col xs={12} md={7} lg={5}>
          <Card className="card-lg mb-5">
            <Card.Body>
              <Form disabled={shouldDisableForm()} onSubmit={forgotPassword}>
                <div className="text-center">
                  <div className="mb-5">
                    <h1 className="display-4">
                      <FormattedMessage id="forgot_password.form.title" />
                    </h1>
                    <p>
                      <FormattedMessage id="forgot_password.form.subtitle" />
                    </p>
                  </div>
                </div>
                <Form.Group controlId="formEmail">
                  <Form.Label>
                    <FormattedMessage id="forgot_password.input.email.label" />
                  </Form.Label>
                  <Form.Control
                    type="email"
                    autoComplete="email"
                    required
                    tabIndex={1}
                    size="lg"
                    disabled={isSending}
                    dir="ltr"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>

                <Button
                  type="submit"
                  size="lg"
                  block
                  tabIndex={2}
                  disabled={shouldDisableForm()}
                  variant="primary"
                >
                  <FormattedMessage id="forgot_password.do_btn.recover_password.title" />
                </Button>
                <div className="text-center mt-2">
                  <Link
                    to={APP_CONST.ROUTES.SIGNIN}
                    replace
                    tabIndex={3}
                    className="btn btn-link"
                  >
                    <FontAwesomeIcon
                      icon={i18n.isRTL ? faChevronRight : faChevronLeft}
                    />{' '}
                    <FormattedMessage id="forgot_password.signin.label" />
                  </Link>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

ForgotPassword.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withIO, withI18n)(ForgotPassword);
