import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';
import { withIO } from 'hoc';
import { IO } from 'services';
import {
  Container,
  Row,
  Col,
  Card,
  Button,
  Modal,
  ListGroup,
} from 'react-bootstrap';
import { toast } from 'react-toastify';
import { FormattedDate, FormattedMessage, useIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faDownload,
  faPrint,
  faReceipt,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import momentJalali from 'jalali-moment';
import _ from 'lodash';
import { faEye } from '@fortawesome/free-regular-svg-icons';
import { appConfig } from 'configs';
import { formatIncompletePhoneNumber } from 'libphonenumber-js';
import BeatLoader from 'react-spinners/BeatLoader';

const Payments = ({ io }) => {
  const [loading, setLoading] = useState(false);
  const [payments, setPayments] = useState([]);
  const [selectedPayment, setSelectedPayment] = useState();
  const intl = useIntl();

  useEffect(() => {
    setLoading(true);

    io.socket.getPayments((result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        setPayments(result.data);
      }

      setLoading(false);
    });
  }, []);

  const determineAmountColor = (amount, status) => {
    if (status === 'SUCCEEDED') {
      if (amount > 0) return 'text-success';
      return 'text-danger';
    }

    return null;
  };

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'payments.title' })}</title>
      </Helmet>

      <Container>
        <Card
          as="a"
          className="card-border-left border-info mb-3 mb-lg-5"
          href="#"
        >
          <Card.Body>
            <Card.Title as="h3">
              <FormattedMessage id="payments.notice.deadline.heading" />
            </Card.Title>
            <Card.Text as="p">
              <FormattedMessage
                id="payments.notice.deadline.desc"
                values={{
                  deadlineDate: momentJalali
                    .from('1399/08/01', 'fa', 'YYYY/MM/DD')
                    .toDate(),
                  b: (chunks) => <strong>{chunks}</strong>,
                }}
              />
            </Card.Text>
          </Card.Body>
        </Card>

        <div className={`text-center py-5 ${loading ? 'd-block' : 'd-none'}`}>
          <BeatLoader size={20} color="#377dff" />
        </div>

        <div className={`${loading ? 'd-none' : 'd-block'}`}>
          <Card className={`${_.isEmpty(payments) ? 'd-block' : 'd-none'}`}>
            <Card.Body className="card-body-height card-body-centered">
              <img
                className="avatar avatar-xxl mb-3"
                src="/assets/images/yelling.svg"
                alt="No payments"
              />
              <Card.Title as="h3">
                <FormattedMessage id="payments.empty_list.heading" />
              </Card.Title>
              <Card.Text>
                <FormattedMessage id="payments.empty_list.desc" />
              </Card.Text>
            </Card.Body>
          </Card>

          <Card className={`${_.isEmpty(payments) ? 'd-none' : 'd-block'}`}>
            <Card.Header>
              <Card.Title as="h4" className="card-header-title">
                <FormattedMessage id="payments.table.heading" />
              </Card.Title>
            </Card.Header>
            <div className="table-responsive">
              <table
                className={`table table-borderless table-thead-bordered table-nowrap table-align-middle card-table ${
                  _.isEmpty(payments) ? 'd-none' : 'd-table'
                }`}
              >
                <thead className="thead-light">
                  <tr>
                    <th>
                      <FormattedMessage id="payments.table.reference.title" />
                    </th>
                    <th>
                      <FormattedMessage id="payments.table.status.title" />
                    </th>
                    <th>
                      <FormattedMessage id="payments.table.amount.title" />
                    </th>
                    <th>
                      <FormattedMessage id="payments.table.date.title" />
                    </th>
                    <th>
                      <FormattedMessage id="payments.table.invoice.title" />
                    </th>
                    <th style={{ width: '5%' }}></th>
                  </tr>
                </thead>
                <tbody>
                  {payments.map((payment) => {
                    return (
                      <React.Fragment key={payment._id}>
                        <tr>
                          <td>
                            {_.isNull(payment.transactionCode) ? (
                              '-'
                            ) : (
                              <a href="#">
                                <FormattedMessage
                                  id="payments.table.row.reference"
                                  values={{
                                    reference: payment.transactionCode,
                                  }}
                                />
                              </a>
                            )}
                          </td>
                          <td>
                            <FormattedMessage
                              id={`payments.table.row.status.${payment.status.toLowerCase()}.label`}
                            />
                          </td>
                          <td
                            className={determineAmountColor(
                              payment.amount,
                              payment.status
                            )}
                          >
                            <FormattedMessage
                              id="payments.table.row.amount"
                              values={{ amount: payment.amount }}
                            />
                          </td>
                          <td>
                            <a href="#">
                              <FormattedDate
                                value={payment.createdAt}
                                year="numeric"
                                month="long"
                                day="numeric"
                              />
                            </a>
                          </td>
                          <td>
                            <Button
                              as="a"
                              size="sm"
                              variant="white"
                              href="#"
                              disabled
                            >
                              <span className="d-flex align-items-center">
                                <FontAwesomeIcon
                                  icon={faDownload}
                                  className="mr-2"
                                />
                                <FormattedMessage id="payments.table.row.invoice.btn.pdf" />
                              </span>
                            </Button>
                          </td>
                          <td>
                            <Button
                              as="a"
                              size="sm"
                              variant="white"
                              href="#"
                              onClick={() => {
                                setSelectedPayment(payment);
                              }}
                            >
                              <span className="d-flex align-items-center">
                                <FontAwesomeIcon
                                  icon={faEye}
                                  className="mr-2"
                                />
                                <FormattedMessage id="payments.table.row.invoice.btn.details" />
                              </span>
                            </Button>
                          </td>
                        </tr>
                      </React.Fragment>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </Card>
        </div>
        <Modal
          show={!_.isUndefined(selectedPayment)}
          onHide={() => {
            setSelectedPayment(undefined);
          }}
          centered
        >
          <div className="modal-top-cover bg-dark text-center">
            <figure className="position-absolute right-0 bottom-0 left-0 ie-modal-curved-shape">
              <svg
                preserveAspectRatio="none"
                xmlns="http://www.w3.org/2000/svg"
                x="0px"
                y="0px"
                viewBox="0 0 1920 100.1"
                style={{ marginBottom: '-2px' }}
              >
                <path
                  fill="#fff"
                  d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"
                ></path>
              </svg>
            </figure>

            <div className="modal-close">
              <button
                type="button"
                className="btn btn-icon btn-sm btn-ghost-light"
                onClick={() => {
                  setSelectedPayment(undefined);
                }}
                data-dismiss="modal"
                aria-label="Close"
                style={{ transition: 'none' }}
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
          </div>

          <div className="modal-top-cover-icon">
            <span className="icon icon-lg icon-light icon-circle icon-centered shadow-soft">
              <FontAwesomeIcon icon={faReceipt} />
            </span>
          </div>

          <Modal.Body className="pt-3 pb-sm-5 px-sm-5">
            <div className="text-center mb-5">
              <h2 className="mb-1">
                <FormattedMessage id="payments.modal.invoice.title" />
              </h2>
              <span className="d-block">
                <FormattedMessage
                  id="payments.modal.invoice.subtitle"
                  values={{ paymentId: selectedPayment?._id }}
                />
              </span>
            </div>

            <Row className="mb-6">
              <Col md={4} className="mb-3">
                <small className="text-cap">
                  <FormattedMessage
                    id={
                      selectedPayment?.amount < 0
                        ? 'payments.modal.invoice.payment_amount.label'
                        : 'payments.modal.invoice.received_amount.label'
                    }
                  />
                </small>
                <span className="text-dark">
                  <FormattedMessage
                    id="payments.table.row.amount"
                    values={{ amount: selectedPayment?.amount }}
                  />
                </span>
              </Col>

              <Col md={4} className="mb-3">
                <small className="text-cap">
                  <FormattedMessage
                    id={
                      selectedPayment?.amount < 0
                        ? 'payments.modal.invoice.payment_date.label'
                        : 'payments.modal.invoice.received_date.label'
                    }
                  />
                </small>
                <span className="text-dark">
                  <FormattedDate
                    value={selectedPayment?.createdAt}
                    year="2-digit"
                    month="long"
                    day="2-digit"
                  />
                </span>
              </Col>
              <Col md={4} className="mb-3">
                <small className="text-cap">
                  <FormattedMessage id="payments.modal.invoice.description.label" />
                </small>
                <span className="text-dark text-break">
                  {selectedPayment?.description}
                </span>
              </Col>
            </Row>

            <small className="text-cap mb-2">
              <FormattedMessage id="payments.modal.invoice.summary.title" />
            </small>

            <ListGroup className="mb-4">
              <ListGroup.Item className="text-dark">
                <div className="d-flex justify-content-between align-items-center">
                  <span>
                    <FormattedMessage id="payments.modal.invoice.table.row.transaction_amount.label" />
                  </span>
                  <span>
                    <FormattedMessage
                      id="payments.modal.invoice.table.row.transaction_amount.value"
                      values={{ value: selectedPayment?.amount }}
                    />
                  </span>
                </div>
              </ListGroup.Item>

              <ListGroup.Item className="text-dark">
                <div className="d-flex justify-content-between align-items-center">
                  <span>
                    <FormattedMessage id="payments.modal.invoice.table.row.transaction_fee.label" />
                  </span>
                  <span>
                    <FormattedMessage
                      id="payments.modal.invoice.table.row.transaction_fee.value"
                      values={{ value: 0 }}
                    />
                  </span>
                </div>
              </ListGroup.Item>

              <ListGroup.Item className="text-dark">
                <div className="d-flex justify-content-between align-items-center">
                  <span>
                    <FormattedMessage id="payments.modal.invoice.table.row.tax_fee.label" />
                  </span>
                  <span>
                    <FormattedMessage
                      id="payments.modal.invoice.table.row.tax_fee.value"
                      values={{ value: 0 }}
                    />
                  </span>
                </div>
              </ListGroup.Item>

              <ListGroup.Item className="list-group-item-light text-dark">
                <div className="d-flex justify-content-between align-items-center">
                  <span className="font-weight-bold">
                    <FormattedMessage
                      id={
                        selectedPayment?.amount < 0
                          ? 'payments.modal.invoice.table.row.total_payment_amount.label'
                          : 'payments.modal.invoice.table.row.total_received_amount.label'
                      }
                    />
                  </span>
                  <span className="font-weight-bold">
                    <FormattedMessage
                      id="payments.modal.invoice.table.row.total_amount.value"
                      values={{ value: selectedPayment?.amount }}
                    />
                  </span>
                </div>
              </ListGroup.Item>
            </ListGroup>

            <div className="d-flex justify-content-end">
              <Button
                size="sm"
                variant="white"
                className="mr-2"
                href="#"
                disabled
              >
                <span className="d-flex align-items-center">
                  <FontAwesomeIcon icon={faDownload} className="mr-2" />
                  <FormattedMessage id="payments.modal.invoice.btn.save_pdf" />
                </span>
              </Button>
              <Button size="sm" variant="white" href="#" disabled>
                <span className="d-flex align-items-center">
                  <FontAwesomeIcon icon={faPrint} className="mr-2" />
                  <FormattedMessage id="payments.modal.invoice.btn.print_invoice" />
                </span>
              </Button>
            </div>

            <hr className="my-5" />

            <p className="modal-footer-text">
              <FormattedMessage
                id="payments.modal.invoice.footer.contact_us"
                values={{
                  emailAddress: appConfig.contactDetail.email,
                  phoneNumber: formatIncompletePhoneNumber(
                    appConfig.contactDetail.phoneNumber
                  ),
                  emailLink: (chunks) => (
                    <a
                      href={`mailto:${appConfig.contactDetail.email}`}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      {chunks}
                    </a>
                  ),
                  telLink: (chunks) => (
                    <a
                      className="dir-ltr"
                      target="_blank"
                      rel="noreferrer noopener"
                      href={`tel:${appConfig.contactDetail.phoneNumber}`}
                    >
                      {chunks}
                    </a>
                  ),
                }}
              />
            </p>
          </Modal.Body>
        </Modal>
      </Container>
    </>
  );
};

Payments.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(Payments);
