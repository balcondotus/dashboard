import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { REST_CONST, APP_CONST } from 'consts';
import { withIO } from 'hoc';
import { IO } from 'services';
import { FormattedMessage, useIntl } from 'react-intl';
import _ from 'lodash';
import { Col, Container, Row } from 'react-bootstrap';

const OAuthRedirect = ({ io }) => {
  const history = useHistory();
  const intl = useIntl();

  useEffect(() => {
    const hash = history.location.hash
      .replace(/^#\/?/, '')
      .split('&')
      .reduce((hash, currentHash) => {
        const separatorIndex = currentHash.indexOf('=');
        if (separatorIndex !== -1) {
          hash[currentHash.substring(0, separatorIndex)] = decodeURIComponent(
            currentHash.substring(separatorIndex + 1)
          );
        }

        return hash;
      }, {});

    if (
      _.isUndefined(hash.state) ||
      !_.isString(hash.state) ||
      _.isUndefined(hash.access_token)
    )
      return history.replace(APP_CONST.ROUTES.HOME);

    hash.state = JSON.parse(hash.state);

    if (hash.access_token) {
      io.rest
        .authenticate(
          REST_CONST.RESOURCE.AUTH[hash.state.provider.toUpperCase()],
          { access_token: hash.access_token, expires_in: hash.expires_in }
        )
        .then((result) => {
          if (result.response.error) {
            toast.error(result.response.error.message);
          } else {
            if (hash.state.action === 'link') {
              toast.success(
                intl.formatMessage({
                  id: 'auth_callback.toast.success.linked',
                })
              );
            } else {
              if (result.status === 200) {
                // Doesn't need to say hi
              } else if (result.status === 201) {
                toast.success(
                  intl.formatMessage({
                    id: 'auth_callback.toast.success.signed_up',
                  })
                );
              } else {
                throw new Error(`Status ${result.status} is not implemented`);
              }
            }
          }

          if (hash.state.redirectTo.startsWith('http')) {
            window.location.href = hash.state.redirectTo;
          } else {
            history.replace(hash.state.redirectTo);
          }
        })
        .catch((e) => {
          toast.error(e.message);
          history.replace(APP_CONST.ROUTES.SIGNIN);
        });
    }
  }, []);

  return (
    <Container fluid className="h-100">
      <Row className="h-100 align-items-center justify-content-center text-center">
        <Col className="text-center">
          <h1 className="display-3 font-weight-bold d-block">
            <FormattedMessage id="auth_callback.wait.title" />
          </h1>
          <p className="lead mt-3">
            <FormattedMessage id="auth_callback.wait.desc" />
          </p>
        </Col>
      </Row>
    </Container>
  );
};

OAuthRedirect.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(OAuthRedirect);
