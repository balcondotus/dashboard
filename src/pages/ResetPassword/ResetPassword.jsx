import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { Col, Row, Button, Form, Container, Card } from 'react-bootstrap';
import { parse } from 'query-string';
import { IO } from 'services';
import { withI18n, withIO } from 'hoc';
import { appConfig } from 'configs';
import { Helmet } from 'react-helmet-async';
import { ValidationUtils } from 'utils';
import { FormattedMessage, useIntl } from 'react-intl';
import './ResetPassword.scss';
import { Brand } from 'shared';
import { Link } from 'react-router-dom';
import { APP_CONST } from 'consts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { compose } from 'recompose';

const ResetPassword = ({ i18n, io, ...props }) => {
  const [newPassword, setNewPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const [isSaving, setIsSaving] = useState(false);
  const [resetToken, setResetToken] = useState(null);
  const intl = useIntl();

  useEffect(() => {
    const queries = parse(props.location.search);
    const { token } = queries;

    if (!token) {
      toast.error(
        intl.formatMessage({ id: 'reset_password.toast.error.no_token' })
      );
      setIsSaving(true);
    } else {
      setResetToken(token);
    }

    return () => {};
  }, []);

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const resetPassword = async (e) => {
    e.preventDefault();

    setIsSaving(true);

    try {
      const result = await io.rest.resetPassword(resetToken, newPassword);

      if (!result.error)
        return toast.success(
          intl.formatMessage({
            id: 'reset_password.toast.success.done',
          })
        );

      return toast.error(result.error.message);
    } catch (e) {
      toast.error(e.message);
    } finally {
      setIsSaving(false);
    }
  };

  /**
   * Checks whether the form and submit button should be disabled or not.
   */
  const shouldDisableForm = () =>
    isSaving ||
    !ValidationUtils.validatePassword(newPassword) ||
    !ValidationUtils.validatePassword(confirmPassword) ||
    newPassword !== confirmPassword;

  return (
    <Container className="py-5">
      <Helmet>
        <title>{intl.formatMessage({ id: 'reset_password.title' })}</title>
      </Helmet>
      <Row className="justify-content-center">
        <Col xs={12} className="text-center mb-5">
          <Brand style={{ width: '8rem' }} />
        </Col>
        <Col xs={12} md={7} lg={5}>
          <Card className="card-lg mb-5">
            <Card.Body>
              <Form onSubmit={resetPassword} disabled={shouldDisableForm()}>
                <div className="text-center">
                  <div className="mb-5">
                    <h1 className="display-4">
                      <FormattedMessage id="reset_password.form.title" />
                    </h1>
                    <p>
                      <FormattedMessage id="reset_password.form.subtitle" />
                    </p>
                  </div>
                </div>
                <Form.Group controlId="newPasswordFormControl">
                  <Form.Label>
                    <FormattedMessage id="reset_password.form.new_password.label" />
                  </Form.Label>
                  <Form.Control
                    type="password"
                    minLength={appConfig.validation.minPassword}
                    autoComplete="new-password"
                    dir="ltr"
                    size="lg"
                    tabIndex={1}
                    required
                    disabled={isSaving}
                    onChange={(e) => setNewPassword(e.target.value)}
                  />
                </Form.Group>

                <Form.Group controlId="passwordConfirmationFormControl">
                  <Form.Label>
                    <FormattedMessage id="reset_password.input.confirm_password.label" />
                  </Form.Label>
                  <Form.Control
                    type="password"
                    autoComplete="new-password"
                    dir="ltr"
                    tabIndex={2}
                    size="lg"
                    disabled={isSaving}
                    required
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </Form.Group>

                <Button
                  type="submit"
                  block
                  size="lg"
                  tabIndex={3}
                  disabled={shouldDisableForm()}
                  variant="primary"
                >
                  <FormattedMessage id="reset_password.do.btn.reset_password.title" />
                </Button>
                <div className="text-center mt-2">
                  <Link
                    to={APP_CONST.ROUTES.SIGNIN}
                    replace
                    tabIndex={4}
                    className="btn btn-link"
                  >
                    <FontAwesomeIcon
                      icon={i18n.isRTL ? faChevronRight : faChevronLeft}
                    />{' '}
                    <FormattedMessage id="reset_password.signin.label" />
                  </Link>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

ResetPassword.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withIO, withI18n)(ResetPassword);
