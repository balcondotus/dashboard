import React from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';
import { FormattedMessage, useIntl } from 'react-intl';
import { APP_CONST } from 'consts';
import { Col, Container, Row } from 'react-bootstrap';

const NotFound = () => {
  const intl = useIntl();

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'not_found.title' })}</title>
      </Helmet>

      <Container fluid className="content">
        <Row className="align-items-sm-center py-sm-10">
          <Col sm={6}>
            <div className="text-center text-sm-right mr-sm-4 mb-5 mb-sm-0">
              <img
                className="w-60 w-sm-100 mx-auto"
                src="/assets/images/think.svg"
                alt="Not Found"
                style={{ maxWidth: '15rem' }}
              />
            </div>
          </Col>

          <Col sm={6} md={4} className="text-center text-sm-left">
            <h1 className="display-1 mb-0">
              <FormattedMessage id="not_found.heading" />
            </h1>
            <p className="lead">
              <FormattedMessage id="not_found.subheading" />
            </p>
            <Link
              to={APP_CONST.ROUTES.HOME}
              replace
              className="btn btn-primary"
            >
              <FormattedMessage id="not_found.go_back_to_home" />
            </Link>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default NotFound;
