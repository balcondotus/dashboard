import React, { useState } from 'react';
import { Container, Button, Row, Col, Form, Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { IO } from 'services';
import { Helmet } from 'react-helmet-async';
import { Brand, OAuthButton } from 'shared';
import { appConfig } from 'configs';
import { withIO } from 'hoc';
import { ValidationUtils } from 'utils';
import { FormattedMessage, useIntl } from 'react-intl';
import { APP_CONST } from 'consts';
import qs from 'query-string';
import validator from 'validator';

const SignUp = ({ io }) => {
  const location = useLocation();
  const [isSigning, setIsSigning] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [agreeTerms, setAgreeTerms] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const intl = useIntl();
  const { redirectTo } = qs.parse(location.search);

  /**
   * Signs up with email and password.
   *
   * @param {React.FormEvent<HTMLFormElement>} event
   */
  const signUpWithEmailPassword = async (event) => {
    event.preventDefault();

    setIsSigning(true);

    try {
      const result = await io.rest.signup(email, password, firstName, lastName);

      if (result.error) throw new Error(result.error.message);
    } catch (e) {
      toast.error(e.message);
    } finally {
      setIsSigning(false);
    }
  };

  /**
   * Checks whether the form and submit button should be disabled or not.
   */
  const shouldDisableForm = () =>
    isSigning ||
    !validator.isEmail(email) ||
    !ValidationUtils.validatePassword(password) ||
    !agreeTerms ||
    !firstName ||
    !lastName ||
    password !== confirmPassword;

  return (
    <Container className="py-5">
      <Helmet>
        <title>
          {intl.formatMessage({
            id: 'signup.title',
          })}
        </title>
      </Helmet>

      <Row className="justify-content-center">
        <Col xs={12} className="text-center mb-5">
          <Brand style={{ width: '8rem' }} />
        </Col>
        <Col xs={12} md={7} lg={5}>
          <Card className="card-lg mb-5">
            <Card.Body>
              <Form
                onSubmit={signUpWithEmailPassword}
                disabled={shouldDisableForm()}
              >
                <div className="text-center">
                  <div className="mb-5">
                    <h1 className="display-4">
                      <FormattedMessage id="sign_up.form.title" />
                    </h1>
                    <p>
                      <FormattedMessage
                        id="sign_up.form.subtitle"
                        values={{
                          signinLink: (chunks) => (
                            <Link
                              to={APP_CONST.ROUTES.SIGNIN}
                              replace
                              tabIndex={-1}
                            >
                              {chunks}
                            </Link>
                          ),
                        }}
                      />
                    </p>
                  </div>
                </div>
                <OAuthButton
                  type="button"
                  variant="white"
                  provider={appConfig.supportedOauthProvider.google}
                  action="signup"
                  tabIndex={1}
                  disabled={isSigning}
                  size="lg"
                  block
                  redirectTo={
                    redirectTo
                      ? encodeURIComponent(redirectTo)
                      : APP_CONST.ROUTES.HOME
                  }
                  onClick={() => {
                    setIsSigning(true);
                  }}
                >
                  <FormattedMessage id="signup.form.btn.signup.google.label" />
                </OAuthButton>
                <br />
                <OAuthButton
                  as="a"
                  variant="white"
                  provider={appConfig.supportedOauthProvider.facebook}
                  action="signin"
                  size="lg"
                  tabIndex={2}
                  block
                  className="mb-4"
                  disabled={isSigning}
                  redirectTo={
                    redirectTo
                      ? encodeURIComponent(redirectTo)
                      : APP_CONST.ROUTES.HOME
                  }
                  onClick={() => {
                    setIsSigning(true);
                  }}
                >
                  <FormattedMessage id="signup.form.btn.signup.facebook.label" />
                </OAuthButton>
                <span className="divider text-muted mb-4 text-capitalize">
                  <FormattedMessage id="sign_up.form.or.label" />
                </span>
                <Form.Row>
                  <Form.Label htmlFor="firstNameFormControl">
                    <FormattedMessage id="sign_up.form.fullname.label" />
                  </Form.Label>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col} controlId="firstNameFormControl">
                    <Form.Control
                      disabled={isSigning}
                      type="text"
                      autoComplete="given-name"
                      dir="auto"
                      size="lg"
                      tabIndex={3}
                      placeholder={intl.formatMessage({
                        id: 'signup.form.firstname.placeholder',
                      })}
                      required
                      onChange={(e) => setFirstName(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group as={Col} controlId="lastNameFormControl">
                    <Form.Control
                      disabled={isSigning}
                      type="text"
                      dir="auto"
                      size="lg"
                      tabIndex={4}
                      autoComplete="family-name"
                      required
                      placeholder={intl.formatMessage({
                        id: 'signup.form.lastname.placeholder',
                      })}
                      onChange={(e) => setLastName(e.target.value)}
                    />
                  </Form.Group>
                </Form.Row>

                <Form.Group controlId="emailFormControl">
                  <Form.Label>
                    <FormattedMessage id="sign_up.form.email.label" />
                  </Form.Label>
                  <Form.Control
                    disabled={isSigning}
                    type="email"
                    required
                    tabIndex={5}
                    size="lg"
                    autoComplete="email"
                    dir="ltr"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="passwordFormControl">
                  <Form.Label>
                    <FormattedMessage id="sign_up.form.password.label" />
                  </Form.Label>
                  <Form.Control
                    required
                    disabled={isSigning}
                    size="lg"
                    tabIndex={6}
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setPassword(e.target.value)}
                    type="password"
                    autoComplete="new-password"
                    dir="ltr"
                  />
                </Form.Group>
                <Form.Group controlId="passwordConfirmationFormControl">
                  <Form.Label>
                    <FormattedMessage id="sign_up.form.confirm_password.label" />
                  </Form.Label>
                  <Form.Control
                    required
                    disabled={isSigning}
                    size="lg"
                    tabIndex={7}
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    type="password"
                    autoComplete="new-password"
                    dir="ltr"
                  />
                </Form.Group>
                <Form.Group>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      tabIndex={8}
                      id="agreeTermsFormControl"
                      onChange={(e) => {
                        setAgreeTerms(e.target.checked);
                      }}
                      name="agreeTermsFormControl"
                      required
                    />
                    <label
                      className="custom-control-label font-size-sm text-muted"
                      htmlFor="agreeTermsFormControl"
                    >
                      <FormattedMessage
                        id="signup.form.policies.label"
                        values={{
                          tos: (chunks) => (
                            <a
                              target="blank"
                              tabIndex={-1}
                              href={APP_CONST.ROUTES.TERMS_OF_USE}
                            >
                              {chunks}
                            </a>
                          ),
                          privacy: (chunks) => (
                            <a
                              target="blank"
                              tabIndex={-1}
                              href={APP_CONST.ROUTES.PRIVACY}
                            >
                              {chunks}
                            </a>
                          ),
                        }}
                      />
                    </label>
                  </div>
                </Form.Group>
                <Button
                  type="submit"
                  size="lg"
                  tabIndex={9}
                  block
                  disabled={shouldDisableForm()}
                  variant="primary"
                >
                  <FormattedMessage id="signup.form.btn.signup.email.label" />
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

SignUp.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(SignUp);
