import React, { useState } from 'react';
import { Container, Button, Row, Col, Form, Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';
import { IO } from 'services';
import { Helmet } from 'react-helmet-async';
import { Brand, OAuthButton } from 'shared';
import { appConfig } from 'configs';
import { withIO } from 'hoc';
import qs from 'query-string';
import { ValidationUtils } from 'utils';
import { FormattedMessage, useIntl } from 'react-intl';
import { APP_CONST } from 'consts';
import validator from 'validator';

const SignIn = ({ io }) => {
  const location = useLocation();
  const [isSigning, setIsSigning] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState();
  const intl = useIntl();
  const { redirectTo } = qs.parse(location.search);

  /**
   * Signs in with email and password.
   *
   * @param {React.FormEvent<HTMLFormElement>} event
   */
  const signInWithEmailPassword = async (event) => {
    event.preventDefault();

    setIsSigning(true);

    try {
      const result = await io.rest.signin(email, password);

      if (result.error) throw new Error(result.error.message);
    } catch (e) {
      toast.error(e.message);
    } finally {
      setIsSigning(false);
    }
  };

  /**
   * Checks whether the form and submit button should be disabled or not.
   */
  const shouldDisableForm = () =>
    isSigning ||
    !validator.isEmail(email) ||
    !ValidationUtils.validatePassword(password);

  return (
    <Container className="py-5">
      <Helmet>
        <title>
          {intl.formatMessage({
            id: 'signin.title',
          })}
        </title>
      </Helmet>
      <Row className="justify-content-center">
        <Col xs={12} className="text-center mb-5">
          <Brand style={{ width: '8rem' }} />
        </Col>
        <Col xs={12} md={7} lg={5}>
          <Card className="card-lg mb-5">
            <Card.Body>
              <Form
                onSubmit={signInWithEmailPassword}
                disabled={shouldDisableForm()}
              >
                <div className="text-center">
                  <div className="mb-5">
                    <h1 className="display-4">
                      <FormattedMessage id="sign_in.form.title" />
                    </h1>
                    <p>
                      <FormattedMessage
                        id="sign_in.form.subtitle"
                        values={{
                          signupLink: (chunks) => (
                            <Link
                              to={APP_CONST.ROUTES.SIGNUP}
                              replace
                              tabIndex={-1}
                            >
                              {chunks}
                            </Link>
                          ),
                        }}
                      />
                    </p>
                  </div>
                </div>
                <OAuthButton
                  type="button"
                  variant="white"
                  provider={appConfig.supportedOauthProvider.google}
                  action="signin"
                  tabIndex={1}
                  disabled={isSigning}
                  size="lg"
                  block
                  redirectTo={
                    redirectTo
                      ? encodeURIComponent(redirectTo)
                      : location.state?.from || APP_CONST.ROUTES.HOME
                  }
                  onClick={() => {
                    setIsSigning(true);
                  }}
                >
                  <FormattedMessage id="signin.form.btn.signin.google.label" />
                </OAuthButton>
                <br />
                <OAuthButton
                  as="a"
                  variant="white"
                  provider={appConfig.supportedOauthProvider.facebook}
                  action="signin"
                  size="lg"
                  tabIndex={2}
                  block
                  className="mb-4"
                  disabled={isSigning}
                  redirectTo={
                    redirectTo
                      ? encodeURIComponent(redirectTo)
                      : location.state?.from || APP_CONST.ROUTES.HOME
                  }
                  onClick={() => {
                    setIsSigning(true);
                  }}
                >
                  <FormattedMessage id="signin.form.btn.signin.facebook.label" />
                </OAuthButton>
                <span className="divider text-muted mb-4 text-capitalize">
                  <FormattedMessage id="sign_in.form.or.label" />
                </span>
                <Form.Group controlId="emailFormControl">
                  <Form.Label>
                    <FormattedMessage id="sign_in.form.email.label" />
                  </Form.Label>
                  <Form.Control
                    disabled={isSigning}
                    type="email"
                    required
                    tabIndex={3}
                    size="lg"
                    autoComplete="email"
                    dir="ltr"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="passwordFormControl">
                  <Form.Label className="d-flex align-items-center justify-content-between">
                    <span>
                      <FormattedMessage id="sign_in.form.password.label" />
                    </span>
                    <Link
                      to={APP_CONST.ROUTES.FORGOT_PASSWORD}
                      replace
                      tabIndex={-1}
                      className="input-label-secondary"
                    >
                      <FormattedMessage id="signin.form.btn.forgot_password.label" />
                    </Link>
                  </Form.Label>
                  <Form.Control
                    required
                    disabled={isSigning}
                    size="lg"
                    tabIndex={4}
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setPassword(e.target.value)}
                    type="password"
                    autoComplete="current-password"
                    dir="ltr"
                  />
                </Form.Group>

                <Button
                  type="submit"
                  size="lg"
                  tabIndex={5}
                  block
                  disabled={shouldDisableForm()}
                  variant="primary"
                >
                  <FormattedMessage id="signin.form.btn.signin.email.label" />
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

SignIn.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(SignIn);
