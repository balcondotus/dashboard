import {
  faBell,
  faCalendarAlt,
  faCreditCard,
  faIdCard,
  faUser,
} from '@fortawesome/free-regular-svg-icons';
import { APP_CONST } from 'consts';

export const NAVS = [
  {
    to: APP_CONST.ROUTES.PROFILE_SETTINGS,
    titleTranslationKey: 'settings.nav.profile.title',
    icon: faIdCard,
  },
  {
    to: APP_CONST.ROUTES.CALENDAR_SETTINGS,
    titleTranslationKey: 'settings.nav.calendar.title',
    icon: faCalendarAlt,
  },
  {
    to: APP_CONST.ROUTES.FINANCE_SETTINGS,
    titleTranslationKey: 'settings.nav.payment.title',
    icon: faCreditCard,
  },
  {
    to: APP_CONST.ROUTES.NOTIFICATION_SETTINGS,
    titleTranslationKey: 'settings.nav.notifications.title',
    icon: faBell,
  },
  {
    to: APP_CONST.ROUTES.ACCOUNT_SETTINGS,
    titleTranslationKey: 'settings.nav.account.title',
    icon: faUser,
  },
];
