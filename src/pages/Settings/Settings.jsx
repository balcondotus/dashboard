import React, { useEffect } from 'react';
import { Nav, Container, Row, Col, Navbar, Button } from 'react-bootstrap';
import { Route } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { FormattedMessage } from 'react-intl';
import { NAVS } from './Navs';
import $ from 'jquery';
import { ROUTES } from './Routes';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import jQuery from 'jquery';

const NavToggle = React.forwardRef(({ children, onClick }, ref) => (
  <Button
    type="button"
    block
    variant="white"
    aria-expanded={false}
    aria-label="Toggle navigation"
    aria-controls="navbar-toggle-default"
    data-toggle="collapse"
    data-target="#navbar-toggle-default"
    className="navbar-toggler mb-3"
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </Button>
));

const Settings = () => {
  useEffect(() => {
    jQuery(function () {
      // initialization of sticky block
      $('.js-sticky-block').each(function () {
        // FIXME: It throws an exception and must be fixed before releasing
        // TODO: Wrap it into a component when it fixed.
        // new window.HSStickyBlock($(this)).init();
      });
    });
  });

  return (
    <div className="settings-component">
      <Container>
        <Row>
          <Col xs={12} sm={12} md={4} lg={3} xl={2}>
            <Navbar expand="md" className="navbar-vertical mb-3 mb-lg-5">
              <Navbar.Toggle as={NavToggle}>
                <span className="d-flex justify-content-between align-items-center">
                  <span className="h5 mb-0">
                    <FormattedMessage id="settings.nav_menu.heading" />
                  </span>

                  <span className="navbar-toggle-default">
                    <FontAwesomeIcon icon={faBars} />
                  </span>

                  <span className="navbar-toggle-toggled">
                    <FontAwesomeIcon icon={faTimes} />
                  </span>
                </span>
              </Navbar.Toggle>

              <Navbar.Collapse id="responsive-navbar-nav">
                <Nav
                  navbar
                  as="ul"
                  className="js-sticky-block navbar-nav-lg nav-tabs card card-navbar-nav"
                  data-hs-sticky-block-options='{
                       "parentSelector": "#responsive-navbar-nav",
                       "breakpoint": "md",
                       "startPoint": "#responsive-navbar-nav",
                       "endPoint": "#stickyBlockEndPoint",
                       "stickyOffsetTop":95
                     }'
                >
                  {NAVS.map((item, index) => {
                    return (
                      <Nav.Item as="li" key={item.to}>
                        <LinkContainer exact to={item.to}>
                          <Nav.Link>
                            <FontAwesomeIcon
                              icon={item.icon}
                              fixedWidth
                              className="nav-icon"
                            />
                            <FormattedMessage id={item.titleTranslationKey} />
                          </Nav.Link>
                        </LinkContainer>
                      </Nav.Item>
                    );
                  })}
                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </Col>
          <Col xs={12} sm={12} md={8} lg={9} xl={10}>
            {ROUTES.map((route) => {
              return (
                <Route
                  key={route.path}
                  path={route.path}
                  exact
                  component={route.component}
                />
              );
            })}
            <div id="stickyBlockEndPoint"></div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

Settings.propTypes = {};

export default Settings;
