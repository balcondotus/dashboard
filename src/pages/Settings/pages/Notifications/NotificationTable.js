// Order of methods is important!
export const notificationTable = {
  meeting: {
    labelKey: 'notifications.table.row.meeting.label',
    methods: ['email', 'sms', 'balcon'],
  },
  reminder: {
    labelKey: 'notifications.table.row.reminder.label',
    methods: ['email', 'sms', 'balcon'],
  },
  payment: {
    labelKey: 'notifications.table.row.payments.label',
    methods: ['email', 'sms', 'balcon'],
  },
  account: {
    labelKey: 'notifications.table.row.account.label',
    methods: ['email', 'sms', 'balcon'],
  },
  product: {
    labelKey: 'notifications.table.row.product.label',
    methods: ['email', null, 'balcon'],
  },
  newsletter: {
    labelKey: 'notifications.table.row.newsletter.label',
    methods: ['email', null, null],
  },
  survey: {
    labelKey: 'notifications.table.row.survey.label',
    methods: ['email', null, null],
  },
  offer: {
    labelKey: 'notifications.table.row.offer.label',
    methods: ['email', null, 'balcon'],
  },
};
