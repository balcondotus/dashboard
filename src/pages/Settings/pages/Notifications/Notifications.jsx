import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Table, Form, Card } from 'react-bootstrap';
import { compose } from 'recompose';
import { toast } from 'react-toastify';
import { withAuthUser, withIO } from 'hoc';
import { IO } from 'services';
import { Helmet } from 'react-helmet-async';
import { FormattedMessage, useIntl } from 'react-intl';
import { notificationTable } from './NotificationTable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGlobeAsia, faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';

const Notifications = ({ io, authUser }) => {
  const intl = useIntl();

  const isEnabled = (notification, method) => {
    return authUser.notifications[notification.toLowerCase()].includes(
      method.toUpperCase()
    );
  };

  const saveNotification = (notification, method) => {
    io.socket.saveNotificationChange(notification, method, (result) => {
      if (result.error) return toast.error(result.error.message);
    });
  };

  return (
    <>
      <Helmet>
        <title>
          {intl.formatMessage({ id: 'notifications.settings.title' })}
        </title>
      </Helmet>
      <Card id="notificationsSection">
        <Card.Header>
          <Card.Title as="h4">
            <FormattedMessage id="notifications.heading" />
          </Card.Title>
        </Card.Header>

        <Form>
          <Table
            responsive
            className="table-thead-bordered table-nowrap table-align-middle card-table"
          >
            <thead className="thead-light">
              <tr>
                <th>
                  <FormattedMessage id="notifications.table.item.heading" />
                </th>
                <th className="text-center">
                  <div className="mb-1">
                    <FontAwesomeIcon icon={faEnvelope} size="2x" />
                  </div>
                  <FormattedMessage id="notifications.table.email.heading" />
                </th>
                <th className="text-center">
                  <div className="mb-1">
                    <FontAwesomeIcon icon={faMobileAlt} size="2x" />
                  </div>
                  <FormattedMessage id="notifications.table.sms.heading" />
                </th>
                <th className="text-center">
                  <div className="mb-1">
                    <FontAwesomeIcon icon={faGlobeAsia} size="2x" />
                  </div>
                  <FormattedMessage id="notifications.table.balcon.heading" />
                </th>
              </tr>
            </thead>

            <tbody>
              {Object.entries(notificationTable).map(
                ([notification, notificationObj]) => {
                  return (
                    <Fragment key={notification}>
                      <tr>
                        <td>
                          <FormattedMessage id={notificationObj.labelKey} />
                        </td>
                        {notificationObj.methods.map((method, index) => {
                          const id = `${notification}-${method}-notification`;

                          return (
                            <td className="text-center" key={index}>
                              {!method ? null : (
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id={id}
                                    checked={isEnabled(notification, method)}
                                    onChange={() =>
                                      saveNotification(notification, method)
                                    }
                                  />

                                  <label
                                    className="custom-control-label"
                                    htmlFor={id}
                                  ></label>
                                </div>
                              )}
                            </td>
                          );
                        })}
                      </tr>
                    </Fragment>
                  );
                }
              )}
            </tbody>
          </Table>
        </Form>
        <Card.Footer>
          <span className="small text-info">
            <FormattedMessage id="notifications.table.footer.we_send_notification_immediately" />
          </span>
        </Card.Footer>
      </Card>
    </>
  );
};

Notifications.propTypes = {
  authUser: PropTypes.any.isRequired,
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withIO, withAuthUser)(Notifications);
