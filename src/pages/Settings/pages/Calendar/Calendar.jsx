import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withIO } from 'hoc';
import { IO } from 'services';
import { Card, Tab, Button, Nav } from 'react-bootstrap';
import { FormattedMessage, useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';
import { toast } from 'react-toastify';
import { Availabilities, Settings } from './components';
import { HSLoadingState } from 'shared';
import BeatLoader from 'react-spinners/BeatLoader';

const Calendar = ({ io }) => {
  const [saving, setSaving] = useState(false);
  const [loading, setLoading] = useState(true);
  const [availabilities, setAvailabilities] = useState([]);
  const [eventSettings, setEventSettings] = useState();
  const intl = useIntl();

  useEffect(() => {
    setLoading(true);

    io.socket.getDefaultEvent((result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        const { data } = result;

        setEventSettings(data);
        setAvailabilities(data.availabilities);
      }

      setLoading(false);
    });
  }, []);

  const applyUpdatedAvailabilities = (updatedAvailabilities) => {
    setAvailabilities(updatedAvailabilities);
  };

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const saveChanges = (e) => {
    e.preventDefault();

    // TODO: Validate advanced settings

    setSaving(true);

    io.socket.saveCalendarSettings(
      availabilities,
      eventSettings.duration,
      eventSettings.frequency,
      eventSettings.buffer,
      eventSettings.meetingsPerDay,
      eventSettings.noticeOfSchedule,
      eventSettings.dateRange,
      (result) => {
        if (result.error) {
          toast.error(result.error.message);
        } else {
          toast.success(
            intl.formatMessage({ id: 'calendar.settings.toast.success.saved' })
          );
        }

        setSaving(false);
      }
    );
  };

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'calendar.settings.title' })}</title>
      </Helmet>

      {loading ? (
        <div className="text-center py-5">
          <BeatLoader size={20} color="#377dff" />
        </div>
      ) : (
        <Card>
          <Card.Header>
            <Card.Title as="h4">
              <FormattedMessage id="settings.calendar.heading" />
            </Card.Title>

            <div className="text-center">
              <Nav
                as="ul"
                className="nav-segment nav-sm-down-break"
                role="tablist"
              >
                <Nav.Item as="li">
                  <HSLoadingState
                    as="a"
                    className="nav-link active"
                    id="availabilities-tab"
                    toggle="pill"
                    href="#availabilities"
                    role="tab"
                    targetEl="#availabilities"
                    removeLoaderDelay={500}
                  >
                    <FormattedMessage id="calendar.nav.availabilities.title" />
                  </HSLoadingState>
                </Nav.Item>
                <Nav.Item as="li">
                  <HSLoadingState
                    as="a"
                    id="settings-tab"
                    className="nav-link"
                    toggle="pill"
                    href="#settings"
                    role="tab"
                    targetEl="#settings"
                    removeLoaderDelay={500}
                  >
                    <FormattedMessage id="calendar.nav.settings.title" />
                  </HSLoadingState>
                </Nav.Item>
              </Nav>
            </div>
          </Card.Header>

          <Card.Body>
            <Tab.Content>
              <Tab.Pane active id="availabilities" role="tabpanel">
                <Availabilities
                  availabilities={availabilities}
                  onApply={applyUpdatedAvailabilities}
                />
              </Tab.Pane>

              <Tab.Pane id="settings" role="tabpanel">
                <Settings settings={eventSettings} />
              </Tab.Pane>
            </Tab.Content>
          </Card.Body>
          <Card.Footer className="text-right">
            <Button type="button" disabled={saving} onClick={saveChanges}>
              <FormattedMessage id="calendar.settings.form.btn.save" />
            </Button>
          </Card.Footer>
        </Card>
      )}
    </>
  );
};

Calendar.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(Calendar);
