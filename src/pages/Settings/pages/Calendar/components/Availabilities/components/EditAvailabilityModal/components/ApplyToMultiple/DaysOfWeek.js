import _ from 'lodash';

const DAYS_OF_WEEK = [
  {
    number: 0,
    name: 'sun',
    translationKey: 'sunday.title',
  },
  { number: 1, name: 'mon', translationKey: 'monday.title' },
  { number: 2, name: 'tue', translationKey: 'tuesday.title' },
  { number: 4, name: 'thu', translationKey: 'thursday.title' },
  { number: 5, name: 'fri', translationKey: 'friday.title' },
  { number: 6, name: 'sat', translationKey: 'saturday.title' },
  { number: 3, name: 'wed', translationKey: 'wednesday.title' },
];

export const sortedDaysOfWeek = (locale) => {
  if (locale === 'fa') {
    return _.sortBy(DAYS_OF_WEEK, (value) => {
      if (value.number === 6) return -1;
      return value.number;
    });
  }
  if (locale === 'en') {
    return _.sortBy(DAYS_OF_WEEK, (value) => value.number);
  }

  throw new Error(`Locale (${locale}) is not implemented`);
};
