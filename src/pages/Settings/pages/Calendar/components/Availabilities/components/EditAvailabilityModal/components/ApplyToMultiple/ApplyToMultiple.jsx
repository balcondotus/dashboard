import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  ToggleButtonGroup,
  ToggleButton,
  Container,
  Modal,
  Button,
} from 'react-bootstrap';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { withI18n } from 'hoc';
import { DayPickerSingleDateController, CalendarDay } from 'react-dates';
import { AvailabilityUtils, DateUtils } from 'utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { sortedDaysOfWeek } from './DaysOfWeek';
import { TOGGLE_BUTTONS } from './ToggleButtons';
import { CalendarInfo } from './components';
import './ApplyToMultiple.scss';

// TODO: minDate is not supported yet
// https://github.com/airbnb/react-dates/issues/1997

const ApplyToMultiple = ({
  date,
  onClose,
  onApply,
  onGoBack,
  tempIntervals,
  i18n,
  availabilities,
}) => {
  const [activeToggle, setActiveToggle] = useState(TOGGLE_BUTTONS.DAYS_OF_WEEK);
  const [selectedDates, updateSelectedDates] = useState([date]);
  const [selectedDaysOfWeek, setSelectedDaysOfWeek] = useState([
    DateUtils.getDateWeekDayName(date),
  ]);

  const handleActiveToggleChange = (value) => {
    // Reset others when a new toggle selected.
    if (value === TOGGLE_BUTTONS.DAYS_OF_WEEK) updateSelectedDates([date]);
    else if (value === TOGGLE_BUTTONS.SPECIFIC)
      setSelectedDaysOfWeek([DateUtils.getDateWeekDayName(date)]);
    else throw new Error(`Toggle (${value}) is not implemented`);

    setActiveToggle(value);
  };

  const clearSelectedDates = () => {
    updateSelectedDates([selectedDates[0]]);
  };

  const updateAvailabilities = (toggle) => {
    let updatedAvailabilities = _.clone(availabilities);

    if (toggle === TOGGLE_BUTTONS.DAYS_OF_WEEK) {
      selectedDaysOfWeek.forEach((weekDay) => {
        updatedAvailabilities = AvailabilityUtils.updateWeekDayIntervals(
          updatedAvailabilities,
          weekDay,
          tempIntervals
        );
      });
    } else if (toggle === TOGGLE_BUTTONS.SPECIFIC) {
      selectedDates.forEach((selectedDate) => {
        updatedAvailabilities = AvailabilityUtils.updateDateIntervals(
          updatedAvailabilities,
          selectedDate,
          tempIntervals
        );
      });
    } else throw new Error(`Toggle ${toggle} is not implemented`);

    return updatedAvailabilities;
  };

  const applyChanges = () => {
    const updatedAvailabilities = updateAvailabilities(activeToggle);
    onApply(updatedAvailabilities);
  };

  const showActiveToggle = () => {
    if (activeToggle === TOGGLE_BUTTONS.DAYS_OF_WEEK) {
      return (
        <>
          <ToggleButtonGroup
            type="checkbox"
            vertical
            onChange={setSelectedDaysOfWeek}
            defaultValue={selectedDaysOfWeek}
          >
            {sortedDaysOfWeek(i18n.locale).map((weekDay) => {
              // eslint-disable-next-line no-param-reassign
              weekDay.name = weekDay.name.toLowerCase();

              return (
                <ToggleButton
                  value={weekDay.name.toLowerCase()}
                  key={weekDay.name}
                  disabled={
                    selectedDaysOfWeek[0] === weekDay.name.toLowerCase()
                  }
                >
                  <FormattedMessage id={weekDay.translationKey} />
                </ToggleButton>
              );
            })}
          </ToggleButtonGroup>
          <div className="text-left">
            <p className="text-info small mb-0 mt-4">
              <FormattedMessage id="calendar.edit_availabilities.days_of_week.text.select_as_much_as_you_want" />
            </p>
          </div>
        </>
      );
    }
    if (activeToggle === TOGGLE_BUTTONS.SPECIFIC) {
      return (
        <DayPickerSingleDateController
          onDateChange={(changedDate) => {
            const newDates = selectedDates.includes(changedDate.toISOString())
              ? selectedDates.filter((d) => !changedDate.isSame(moment(d)))
              : [...selectedDates, changedDate.toISOString()];

            updateSelectedDates(newDates);
          }}
          isRTL={i18n.isRTL}
          renderMonthElement={({ month }) =>
            DateUtils.formatDateBasedOnLocale(
              month,
              i18n.locale,
              'MMMM YYYY',
              'jMMMM jYYYY'
            )
          }
          renderCalendarDay={(props) => {
            const { day, modifiers } = props;
            if (!day || !modifiers) return <CalendarDay {...props} />;

            if (selectedDates.includes(day.toISOString())) {
              modifiers.add('selected');
            } else {
              modifiers.delete('selected');
            }

            return <CalendarDay {...props} modifiers={modifiers} />;
          }}
          isDayBlocked={(day) => moment(date).isSame(day, 'date')}
          noBorder
          initialVisibleMonth={() => moment(date)}
          focused
          renderDayContents={(day) =>
            DateUtils.formatDateBasedOnLocale(day, i18n.locale, 'D', 'jD')
          }
          numberOfMonths={1}
          renderCalendarInfo={() => (
            <CalendarInfo
              selectedDatesCount={selectedDates.length}
              onClearSelectedDates={clearSelectedDates}
            />
          )}
          hideKeyboardShortcutsPanel
        />
      );
    }

    throw new Error(`Active toggle (${activeToggle}) is not implemented.`);
  };

  return (
    <div className="apply-to-multiple-component">
      <Modal.Header>
        <Modal.Title className="d-flex align-items-center">
          <FontAwesomeIcon
            icon={i18n.isRTL ? faChevronRight : faChevronLeft}
            onClick={onGoBack}
            className="back mr-2"
          />
          <FormattedMessage id="calendar.edit_availabilities.more_options.title" />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="text-center">
        <Container fluid>
          <ToggleButtonGroup
            type="radio"
            name="activeToggle"
            defaultValue={activeToggle}
            onChange={handleActiveToggleChange}
            className="btn-block"
          >
            <ToggleButton
              value={TOGGLE_BUTTONS.SPECIFIC}
              variant="outline-secondary"
            >
              <FormattedMessage id="calendar.edit_availabilities.specific_dates.label" />
            </ToggleButton>
            <ToggleButton
              value={TOGGLE_BUTTONS.DAYS_OF_WEEK}
              variant="outline-secondary"
            >
              <FormattedMessage id="calendar.edit_availabilities.over_days_of_week.label" />
            </ToggleButton>
          </ToggleButtonGroup>

          <div className="mt-4">{showActiveToggle()}</div>
        </Container>
      </Modal.Body>
      <Modal.Footer className="justify-content-between align-items-center">
        <Button
          type="button"
          variant="white"
          onClick={onClose}
          style={{ transition: 'none' }}
        >
          <FormattedMessage id="calendar.edit_availabilities.btn.cancel.label" />
        </Button>
        <Button
          type="button"
          variant="primary"
          className="order-1"
          onClick={applyChanges}
        >
          <FormattedMessage id="calendar.edit_availabilities.btn.apply.label" />
        </Button>
      </Modal.Footer>
    </div>
  );
};

ApplyToMultiple.propTypes = {
  date: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  tempIntervals: PropTypes.array.isRequired,
  availabilities: PropTypes.object.isRequired,
  onApply: PropTypes.func.isRequired,
  onGoBack: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
};

export default withI18n(ApplyToMultiple);
