import React, { useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import { IntervalUtils, AvailabilityUtils, DateUtils } from 'utils';
import _ from 'lodash';
import {
  faQuestionCircle,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';
import {
  Modal,
  Button,
  Container,
  Table,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import { FormattedMessage, useIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { INTERVALS_ACTIONS } from './IntervalsActions';
import { APPLY_TYPES } from './ApplyTypes';
import moment from 'moment';
import { IMaskInput } from 'react-imask';
import './EditIntervals.scss';

const EditIntervals = ({
  onMoreOptionsClick,
  onClose,
  onApply,
  availabilities,
  date,
  tempIntervals,
}) => {
  const intl = useIntl();
  const [intervals, updateIntervals] = useReducer((prevIntervals, action) => {
    const { type, value } = action;

    switch (type) {
      case INTERVALS_ACTIONS.RESET:
        return value;

      case INTERVALS_ACTIONS.UPDATE: {
        const newIntervals = [...prevIntervals];
        const intervalIndex = newIntervals.findIndex(
          (element) => element.id === value.id
        );
        newIntervals[intervalIndex] = value;
        return newIntervals;
      }

      case INTERVALS_ACTIONS.REMOVE:
        return prevIntervals.filter((interval) => interval.id !== value);

      case INTERVALS_ACTIONS.ADD:
        return [
          ...prevIntervals,
          {
            // Static key
            id: prevIntervals.length,
            start: {
              value: null,
              isValid: true,
            },
            end: {
              value: null,
              isValid: true,
            },
            errors: [],
          },
        ];

      case INTERVALS_ACTIONS.CLEAR:
        return [];

      default:
        throw new Error(`Given interval action type ${type} is not supported`);
    }
  }, []);

  /**
   * Populate intervals with predefined availabilities.
   */
  useEffect(() => {
    const givenDateIntervals = [];

    const rawGivenDateIntervals = _.isEmpty(tempIntervals)
      ? AvailabilityUtils.findIntervals(availabilities, date)
      : tempIntervals;

    rawGivenDateIntervals.forEach((availability) => {
      givenDateIntervals.push({
        id: givenDateIntervals.length,
        start: {
          value: availability.start,
          isValid: true,
        },
        end: {
          value: availability.end,
          isValid: true,
        },
        errors: [],
      });
    });

    updateIntervals({
      type: INTERVALS_ACTIONS.RESET,
      value: givenDateIntervals,
    });

    return () => {
      updateIntervals([]);
    };
  }, [availabilities]);

  const intervalsToAvailabilities = (type) => {
    const rawIntervals = IntervalUtils.flattenIntervals(_.clone(intervals));

    let updatedAvailabilities = _.clone(availabilities);

    if (type === APPLY_TYPES.CURRENT_DAY) {
      updatedAvailabilities = AvailabilityUtils.updateDateIntervals(
        updatedAvailabilities,
        date,
        rawIntervals
      );
    } else if (type === APPLY_TYPES.DAY_OF_WEEK) {
      updatedAvailabilities = AvailabilityUtils.updateWeekDayIntervals(
        updatedAvailabilities,
        DateUtils.getDateWeekDayName(date),
        rawIntervals
      );
    } else throw new Error(`Apply type (${type}) is not implemented`);

    return updatedAvailabilities;
  };

  /**
   * Validates intervals agains overlapping.
   */
  const validateOverlapping = () => {
    const overlappingIntervals = IntervalUtils.findOverlapingIntervals(
      intervals
    );

    if (_.isEmpty(overlappingIntervals)) return true;

    overlappingIntervals.forEach((interval) => {
      // Mark the interval has failed.
      interval.start.isValid = false;
      interval.end.isValid = false;
      interval.errors.push('times_are_overlapping.error');

      updateIntervals({
        type: INTERVALS_ACTIONS.UPDATE,
        value: interval,
      });
    });

    return false;
  };

  /**
   * Removed given interval
   *
   * @param {Object} interval
   */
  const removeInterval = (interval) => {
    updateIntervals({
      type: INTERVALS_ACTIONS.REMOVE,
      value: interval.id,
    });
  };

  /**
   * Adds a new interval safely.
   */
  const addInterval = () => {
    updateIntervals({
      type: INTERVALS_ACTIONS.ADD,
    });
  };

  /**
   * Validate intervals.
   *
   * @returns {Boolean} `true` means valid.
   */
  const validateIntervals = () => {
    if (_.isEmpty(intervals)) return true;

    const invalidIntervals = [];

    intervals.forEach((interval, index) => {
      // Clear errors if it has then validate once again.
      if (!_.isEmpty(interval.errors)) {
        interval.start.isValid = true;
        interval.end.isValid = true;
        interval.errors = [];
      }

      if (!IntervalUtils.testTime(interval.start.value)) {
        interval.start.isValid = false;
        interval.errors.push('incorrect_start_time.error');
      }

      if (!IntervalUtils.testTime(interval.end.value)) {
        interval.end.isValid = false;
        interval.errors.push('incorrect_end_time.error');
      }

      if (interval.start.isValid) {
        if (!IntervalUtils.isTimeWithinDate(date, interval.start.value)) {
          interval.start.isValid = false;
          interval.errors.push('start_time_not_within_date.error');
        }
      }

      if (interval.end.isValid) {
        if (!IntervalUtils.isTimeWithinDate(date, interval.end.value)) {
          interval.end.isValid = false;
          interval.errors.push('end_time_not_within_date.error');
        }
      }

      if (interval.start.isValid && interval.end.isValid) {
        const startInterval = IntervalUtils.makeMoment(
          date,
          interval.start.value
        );
        const endInterval = IntervalUtils.makeMoment(date, interval.end.value);

        if (startInterval.isSame(endInterval)) {
          interval.start.isValid = false;
          interval.end.isValid = false;
          interval.errors.push('start_time_same_end_time.error');
        } else if (startInterval.isAfter(endInterval)) {
          interval.start.isValid = false;
          interval.end.isValid = false;
          interval.errors.push('start_time_after_end_time.error');
        }
      }

      if (!_.isEmpty(interval.errors)) {
        invalidIntervals.push(index);
        updateIntervals({
          type: INTERVALS_ACTIONS.UPDATE,
          value: interval,
        });
      }
    });

    if (_.isEmpty(invalidIntervals)) {
      return validateOverlapping();
    }

    return false;
  };

  /**
   * Applies changes safely.
   */
  const applyChanges = (type) => {
    if (validateIntervals()) {
      onApply(intervalsToAvailabilities(type));
    }
  };

  /**
   * Requests to open more options page safely.
   *
   * @param {React.ChangeEvent} e
   */
  const openMultipleDaysPage = (e) => {
    e.preventDefault();

    if (validateIntervals()) {
      onMoreOptionsClick(IntervalUtils.flattenIntervals(_.clone(intervals)));
    }
  };

  /**
   * Clears intervals.
   *
   * @param {React.ChangeEvent} e
   */
  const clearIntervals = (e) => {
    e.preventDefault();

    updateIntervals({ type: INTERVALS_ACTIONS.CLEAR });
  };

  const markIntervalAsValid = (interval) => {
    interval.start.isValid = true;
    interval.end.isValid = true;
    interval.errors = [];

    updateIntervals({ type: INTERVALS_ACTIONS.UPDATE, value: interval });
  };

  return (
    <div className="edit-intervals-component">
      <>
        <Modal.Header className="align-items-center justify-content-between">
          <Modal.Title>
            <FormattedMessage id="calendar.edit_availabilities.title" />
          </Modal.Title>
          <OverlayTrigger
            delay={{ show: 250, hide: 400 }}
            overlay={
              <Tooltip style={{ zIndex: 1000 }}>
                <FormattedMessage id="calendar.edit_availabilities.intervals.tooltip" />
              </Tooltip>
            }
          >
            <FontAwesomeIcon icon={faQuestionCircle} />
          </OverlayTrigger>
        </Modal.Header>
        <Modal.Body className="text-center">
          <Container fluid>
            {!_.isEmpty(intervals) ? (
              <Table responsive borderless className="times mb-2">
                <thead>
                  <tr>
                    <th className="font-weight-bold p-0">
                      <FormattedMessage id="calendar.edit_availabilities.intervals.from.label" />
                    </th>
                    <th className="p-0">&nbsp;</th>
                    <th className="font-weight-bold p-0">
                      <FormattedMessage id="calendar.edit_availabilities.intervals.to.label" />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {intervals.map((interval) => {
                    return (
                      <React.Fragment key={interval.id}>
                        <tr>
                          <td>
                            <IMaskInput
                              mask="00:00"
                              type="text"
                              placeholder="xx:xx"
                              value={interval.start.value}
                              maxLength={5}
                              className={`dir-ltr form-control ${
                                !interval.start.isValid ? 'is-invalid' : ''
                              }`}
                              onFocus={() => {
                                markIntervalAsValid(interval);
                              }}
                              onAccept={(value) => {
                                interval.start.value = value;
                              }}
                            />
                          </td>
                          <td>-</td>
                          <td>
                            <IMaskInput
                              mask="00:00"
                              type="text"
                              placeholder="xx:xx"
                              value={interval.end.value}
                              maxLength={5}
                              className={`dir-ltr form-control ${
                                !interval.end.isValid ? 'is-invalid' : ''
                              }`}
                              onFocus={() => {
                                markIntervalAsValid(interval);
                              }}
                              onAccept={(value) => {
                                interval.end.value = value;
                              }}
                            />
                          </td>
                          <td>
                            <FontAwesomeIcon
                              icon={faTrashAlt}
                              className="text-danger remove"
                              onClick={() => {
                                removeInterval(interval);
                              }}
                            />
                          </td>
                        </tr>
                        {!_.isEmpty(interval.errors) ? (
                          <tr className="text-left">
                            <td colSpan={3} className="small text-danger">
                              {interval.errors.map((error, errorIndex) => {
                                return (
                                  // eslint-disable-next-line react/no-array-index-key
                                  <div key={errorIndex}>
                                    <FormattedMessage id={error} />
                                  </div>
                                );
                              })}
                            </td>
                          </tr>
                        ) : null}
                      </React.Fragment>
                    );
                  })}
                </tbody>
              </Table>
            ) : (
              <div className="unavailable mb-2">
                <FormattedMessage id="calendar.edit_availabilities.intervals.no_intervals.label" />
              </div>
            )}
            <div className="d-flex align-items-center justify-content-between">
              <Button
                type="button"
                variant="outline-primary"
                size="sm"
                className="d-block"
                onClick={addInterval}
              >
                <FormattedMessage id="calendar.edit_availabilities.intervals.btn.add.label" />
              </Button>
              <Button
                type="button"
                variant="outline-primary"
                size="sm"
                className="d-block"
                disabled={_.isEmpty(intervals)}
                onClick={clearIntervals}
              >
                <FormattedMessage id="calendar.edit_availabilities.intervals.btn.clear.label" />
              </Button>
            </div>
            <div className="d-flex flex-column mt-4">
              <Button
                type="button"
                variant="primary"
                className="d-block mb-2"
                onClick={() => {
                  applyChanges(APPLY_TYPES.CURRENT_DAY);
                }}
              >
                <FormattedMessage
                  id="calendar.edit_availabilities.intervals.btn.apply_for_selected_day.label"
                  values={{ date: moment(date).toDate() }}
                />
              </Button>
              <Button
                type="button"
                variant="primary"
                className="d-block mb-2"
                onClick={() => {
                  applyChanges(APPLY_TYPES.DAY_OF_WEEK);
                }}
              >
                <FormattedMessage
                  id="calendar.edit_availabilities.intervals.btn.apply_for_same_weekday.label"
                  values={{ date: moment(date).toDate() }}
                />
              </Button>
            </div>
          </Container>
        </Modal.Body>
        <Modal.Footer className="justify-content-between align-items-center">
          <Button
            type="button"
            variant="white"
            onClick={onClose}
            style={{ transition: 'none' }}
          >
            <FormattedMessage id="calendar.edit_availabilities.intervals.btn.cancel.label" />
          </Button>
          <button
            className="btn-as-text text-primary order-1"
            type="button"
            onClick={openMultipleDaysPage}
          >
            <FormattedMessage id="calendar.edit_availabilities.intervals.btn.see_more_options.label" />
          </button>
        </Modal.Footer>
      </>
    </div>
  );
};

EditIntervals.propTypes = {
  date: PropTypes.string.isRequired,
  availabilities: PropTypes.object.isRequired,
  onMoreOptionsClick: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onApply: PropTypes.func.isRequired,
  tempIntervals: PropTypes.array.isRequired,
};

export default EditIntervals;
