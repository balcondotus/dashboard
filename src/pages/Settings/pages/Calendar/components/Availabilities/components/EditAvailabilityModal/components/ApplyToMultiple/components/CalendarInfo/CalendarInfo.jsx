import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber, FormattedMessage } from 'react-intl';

const CalendarInfo = ({ selectedDatesCount, onClearSelectedDates }) => {
  return (
    <div className="d-flex align-items-center justify-content-between small">
      <div className="text-secondary">
        <FormattedNumber value={selectedDatesCount}>
          {(count) => {
            return (
              <FormattedMessage
                id="calendar.edit_availabilities.specific_dates.total_selected.label"
                values={{ count }}
              />
            );
          }}
        </FormattedNumber>
      </div>
      <button
        type="button"
        className="btn-as-text text-primary"
        onClick={onClearSelectedDates}
      >
        <FormattedMessage id="calendar.edit_availabilities.specific_dates.btn.clear.label" />
      </button>
    </div>
  );
};

CalendarInfo.propTypes = {
  selectedDatesCount: PropTypes.number.isRequired,
  onClearSelectedDates: PropTypes.func.isRequired,
};

export default CalendarInfo;
