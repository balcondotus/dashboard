import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-bootstrap';
import { ApplyToMultiple, EditIntervals } from './components';
import './EditAvailabilityModal.scss';

const EditAvailabilityModal = ({
  date,
  availabilities,
  onApply,
  onClose,
  show,
}) => {
  const [isMoreOptionsShown, showMoreOptions] = useState(false);
  const [tempIntervals, setTempIntervals] = useState([]);

  const onApplyEditIntervals = (newAvailabilities) => {
    onApply(newAvailabilities);
  };

  /**
   * Apply more options changes.
   *
   * @param {Array} newAvailabilities
   */
  const onApplyMoreOptions = (newAvailabilities) => {
    onApply(newAvailabilities);
  };

  const onShowMoreOptions = (intervals) => {
    setTempIntervals(intervals);
    showMoreOptions(true);
  };

  const onShowEditIntervals = () => {
    showMoreOptions(false);
  };

  return (
    <Modal
      show={show}
      centered
      backdrop="static"
      className="edit-availability-modal-component"
    >
      {!isMoreOptionsShown ? (
        <EditIntervals
          date={date}
          availabilities={availabilities}
          onClose={onClose}
          tempIntervals={tempIntervals}
          onMoreOptionsClick={onShowMoreOptions}
          onApply={onApplyEditIntervals}
        />
      ) : (
        <ApplyToMultiple
          date={date}
          onClose={onClose}
          availabilities={availabilities}
          tempIntervals={tempIntervals}
          onApply={onApplyMoreOptions}
          onGoBack={onShowEditIntervals}
        />
      )}
    </Modal>
  );
};

EditAvailabilityModal.propTypes = {
  onApply: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  availabilities: PropTypes.object.isRequired,
  date: PropTypes.string.isRequired,
};

export default EditAvailabilityModal;
