import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import FullCalendar from '@fullcalendar/react';
import { RRuleSet, RRule } from 'rrule';
import dayGridPlugin from '@fullcalendar/daygrid';
import rrulePlugin from '@fullcalendar/rrule';
import { IntervalUtils } from 'utils';
import moment from 'moment';
import interactionPlugin from '@fullcalendar/interaction';
import { withI18n } from 'hoc';
import faLocale from '@fullcalendar/core/locales/fa';
import { EditAvailabilityModal } from './components';
import './Availabilities.module.scss';

const Availabilities = ({ availabilities, onApply, i18n }) => {
  const [isEditModalShown, showEditModal] = useState(false);
  const [clickedDate, setClickedDate] = useState(undefined);
  const [events, setEvents] = useState([]);

  /**
   * Populate events with well-formatted availabilities.
   */
  useEffect(() => {
    const topRuleSet = [];

    Object.entries(availabilities).forEach(
      ([weekday, weekdayAvailabilities]) => {
        weekdayAvailabilities.times.forEach((weekDayInterval) => {
          const weekRuleSet = new RRuleSet();
          const intervalDuration = IntervalUtils.formatDuration(
            IntervalUtils.getDurationFromInterval(
              weekDayInterval.start,
              weekDayInterval.end
            )
          );

          weekRuleSet.rrule(
            new RRule({
              freq: RRule.WEEKLY,
              byweekday: RRule[weekday.substr(0, 2).toUpperCase()],
              // Date to the past (1970-01-01) causes displaying wrong timezone
              dtstart: moment
                .utc(`${weekDayInterval.start}`, 'HH:mm')
                .toDate(),
            })
          );

          // Add specific dates into exclusion
          Object.entries(weekdayAvailabilities.specifics).forEach(
            ([specificDate]) => {
              weekRuleSet.exdate(
                moment
                  .utc(`${specificDate}T${weekDayInterval.start}`)
                  .toDate()
              );
            }
          );

          topRuleSet.push({
            rrule: weekRuleSet.toString(),
            duration: intervalDuration,
          });
        });

        // Add specific datetimes
        Object.entries(weekdayAvailabilities.specifics).forEach(
          ([specificDate, specificDateIntervals]) => {
            specificDateIntervals.forEach((specificDateInterval) => {
              topRuleSet.push({
                rrule: new RRule({
                  freq: RRule.DAILY,
                  count: 1,
                  dtstart: moment
                    .utc(`${specificDate}T${specificDateInterval.start}`)
                    .toDate(),
                }).toString(),
                duration: IntervalUtils.formatDuration(
                  IntervalUtils.getDurationFromInterval(
                    specificDateInterval.start,
                    specificDateInterval.end
                  )
                ),
              });
            });
          }
        );
      }
    );

    setEvents(topRuleSet);
  }, [availabilities]);

  const onModalClose = () => {
    showEditModal(false);
    setClickedDate(undefined);
  };

  /**
   *
   * @param {Date} selectedDate
   */
  const onModalOpen = (selectedDate) => {
    // Pass the selcted day in local time
    setClickedDate(moment(selectedDate).utc().format('YYYY-MM-DD'));
    showEditModal(true);
  };

  const onApplyAvailabilities = (newAvailabilities) => {
    // Calendar updates natively here.
    onApply(newAvailabilities);
    onModalClose();
  };

  return (
    <>
      <FullCalendar
        plugins={[dayGridPlugin, rrulePlugin, interactionPlugin]}
        timeZone="UTC"
        events={events}
        locale={i18n.locale}
        locales={[faLocale]}
        eventBackgroundColor="transparent"
        eventTextColor="#6c757d"
        displayEventTime
        dateClick={({ date }) => {
          onModalOpen(date);
        }}
        eventClick={({ event }) => {
          onModalOpen(event.start);
        }}
        displayEventEnd
        headerToolbar={{
          start: 'title',
          center: '',
          end: 'prev,next',
        }}
        eventBorderColor="transparent"
        dayHeaderFormat={{
          weekday: 'short',
        }}
        height="auto"
        aspectRatio={1}
      />
      {isEditModalShown ? (
        <EditAvailabilityModal
          availabilities={availabilities}
          onClose={onModalClose}
          show={isEditModalShown}
          date={clickedDate}
          onApply={onApplyAvailabilities}
        />
      ) : null}
    </>
  );
};

Availabilities.propTypes = {
  availabilities: PropTypes.object.isRequired,
  onApply: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
};

export default withI18n(Availabilities);
