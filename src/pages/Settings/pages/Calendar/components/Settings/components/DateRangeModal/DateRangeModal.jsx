import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, Form, InputGroup } from 'react-bootstrap';
import moment from 'jalali-moment';
import { FormattedMessage, useIntl } from 'react-intl';
import { DateRangePicker } from 'react-dates';
import { DateUtils } from 'utils';
import { withI18n } from 'hoc';
import { DATE_RANGE_TYPES } from './DateRangeTypes';
import './DateRangeModal.module.scss';

const DateRangeModal = ({ show, select, onClose, onSave, i18n }) => {
  const [selectedDateRangeType, setSelectedDateRangeType] = useState(
    select.type
  );
  const [dateRangeTypeValue, setDateRangeTypeValue] = useState(select.value);
  const [focusedInput, setFocusedInput] = useState();
  const intl = useIntl();

  useEffect(() => {
    if (!show) {
      // Reset to defaults on hide
      setSelectedDateRangeType(undefined);
      setDateRangeTypeValue(undefined);
      setFocusedInput(undefined);
    } else {
      setSelectedDateRangeType(select.type);
      setDateRangeTypeValue(select.value);
    }
  }, [show]);

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>
          <FormattedMessage id="calendar.settings.date_range.title" />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group>
          <Form.Label>
            <FormattedMessage id="calendar.settings.date_range.form.date_range.label" />
          </Form.Label>
          <InputGroup className="input-group-sm-down-break">
            {DATE_RANGE_TYPES.map((dataRangeType, index) => (
              <div className="form-control" key={dataRangeType.value}>
                <div className="custom-control custom-radio">
                  <input
                    type="radio"
                    className="custom-control-input"
                    name="userAccountTypeRadio"
                    value={dataRangeType.value}
                    defaultChecked={select.type === dataRangeType.value}
                    id={dataRangeType.value}
                    onChange={(e) => {
                      const { value } = e.target;

                      if (value === 'ROLLING') {
                        setDateRangeTypeValue(
                          select.type === 'ROLLING' ? select.value : 1
                        );
                      } else if (value === 'RANGE') {
                        setDateRangeTypeValue(
                          select.type === 'RANGE' ? select.value : null
                        );
                      } else {
                        setDateRangeTypeValue(null);
                      }

                      setSelectedDateRangeType(value);
                    }}
                  />
                  <label
                    className="custom-control-label"
                    htmlFor={dataRangeType.value}
                  >
                    <FormattedMessage id={dataRangeType.label} />
                  </label>
                </div>
              </div>
            ))}
          </InputGroup>
        </Form.Group>
        <Form.Group
          controlId="formDateRangeValue"
          className={`mb-0 ${
            selectedDateRangeType === 'ROLLING' ? 'd-block' : 'd-none'
          }`}
        >
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text>
                <FormattedMessage id="settings.calendar.advanced.modal.date_range.rolling_days.prepend.label" />
              </InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              type="number"
              defaultValue={dateRangeTypeValue}
              onChange={(e) => {
                setDateRangeTypeValue(Number(e.target.value));
              }}
              min={1}
              max={365}
              placeholder={60}
            />
            <InputGroup.Append>
              <InputGroup.Text>
                <FormattedMessage id="settings.calendar.advanced.modal.date_range.rolling_days.append.label" />
              </InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
        </Form.Group>
        <Form.Group
          controlId="formDateRangeValue"
          className={`mb-0 text-center ${
            selectedDateRangeType === 'RANGE' ? 'd-block' : 'd-none'
          }`}
        >
          <DateRangePicker
            onDatesChange={({ startDate, endDate }) => {
              setDateRangeTypeValue({
                start: startDate?.utc().toISOString(),
                end: endDate?.utc().toISOString(),
              });
            }}
            onFocusChange={(fi) => {
              setFocusedInput(fi);
            }}
            focusedInput={focusedInput}
            isRTL={i18n.isRTL}
            startDatePlaceholderText={intl.formatMessage({
              id: 'calendar.settings.date_range.form.range.start.placeholder',
            })}
            endDatePlaceholderText={intl.formatMessage({
              id: 'calendar.settings.date_range.form.range.end.placeholder',
            })}
            hideKeyboardShortcutsPanel
            showClearDates
            endDateId="endDateRangeValue"
            startDateId="startDateRangeValue"
            renderMonthElement={({ month }) =>
              DateUtils.formatDateBasedOnLocale(
                month,
                i18n.locale,
                'MMMM YYYY',
                'jMMMM jYYYY'
              )
            }
            renderDayContents={(day) =>
              DateUtils.formatDateBasedOnLocale(day, i18n.locale, 'D', 'jD')
            }
            numberOfMonths={1}
            displayFormat={
              i18n.locale === 'fa' ? 'jD jMMMM jYYYY' : 'MMMM D, YYYY'
            }
            reopenPickerOnClearDates
            showDefaultInputIcon
            startDate={
              dateRangeTypeValue?.start
                ? moment.utc(dateRangeTypeValue.start)
                : null
            }
            endDate={
              dateRangeTypeValue?.end
                ? moment.utc(dateRangeTypeValue.end)
                : null
            }
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="white"
          onClick={onClose}
          style={{ transition: 'none' }}
        >
          <FormattedMessage id="calendar.settings.date_range.form.btn.cancel.label" />
        </Button>
        <Button
          variant="primary"
          style={{ transition: 'none' }}
          onClick={() => {
            onSave(selectedDateRangeType, dateRangeTypeValue);
          }}
        >
          <FormattedMessage id="calendar.settings.date_range.form.btn.apply.label" />
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

DateRangeModal.propTypes = {
  show: PropTypes.bool.isRequired,
  select: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
};

export default withI18n(DateRangeModal);
