export const DATE_RANGE_TYPES = [
  { value: 'ROLLING', label: 'date_range_type_rolling' },
  { value: 'RANGE', label: 'date_range_type_range' },
  { value: 'INDEFINITELY', label: 'date_range_type_indefinitely' },
];
