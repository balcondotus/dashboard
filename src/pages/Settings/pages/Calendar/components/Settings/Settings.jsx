import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Col,
  Form,
  FormGroup,
  InputGroup,
  OverlayTrigger,
  Row,
  Tooltip,
} from 'react-bootstrap';
import { FormattedMessage, FormattedNumber, useIntl } from 'react-intl';
import { PredefinedInput } from 'shared';
import { BUFFERS } from './Buffers';
import { FREQUENCIES } from './Frequencies';
import { DateRangeModal } from './components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import { withI18n } from 'hoc';
import Select from 'react-select';

const Settings = ({ i18n, settings }) => {
  const [showDateRangeModal, setShowDateRangeModal] = useState(false);
  const intl = useIntl();

  const makeDateRangeSettingCaption = () => {
    const { type, value } = settings.dateRange;

    switch (type) {
      case 'ROLLING':
        return (
          <span>
            <FormattedNumber
              style="unit"
              unit="day"
              value={value}
              useGrouping={false}
            >
              {(days) => {
                return (
                  <FormattedMessage
                    id="calendar.form.date_range.text.rolling"
                    values={{ days }}
                  />
                );
              }}
            </FormattedNumber>
          </span>
        );

      case 'RANGE':
        return (
          <span>
            <FormattedMessage
              id="calendar.form.date_range.text.range"
              values={{
                start: intl.formatDate(value.start, {
                  month: 'long',
                  day: 'numeric',
                  year: 'numeric',
                }),
                end: intl.formatDate(value.end, {
                  month: 'long',
                  day: 'numeric',
                  year: 'numeric',
                }),
              }}
            />
          </span>
        );
      case 'INDEFINITELY':
        return (
          <FormattedMessage id="calendar.form.date_range.text.indefinitely" />
        );
      default:
        throw new Error(`Date range type (${type}) is not implemented`);
    }
  };

  const bufferOptionsList = () => {
    return BUFFERS.map((item) => {
      const durationHours = Math.floor(item / 60);
      const durationMinutes = item % 60;
      var label;

      if (durationHours <= 0 && durationMinutes <= 0) {
        label = intl.formatNumber(durationMinutes, {
          style: 'unit',
          unit: 'minute',
          unitDisplay: 'long',
        });
      } else if (durationHours <= 0 && durationMinutes > 0) {
        label = intl.formatNumber(durationMinutes, {
          style: 'unit',
          unit: 'minute',
          unitDisplay: 'long',
        });
      } else if (durationHours > 0 && durationMinutes <= 0) {
        label = intl.formatNumber(durationHours, {
          style: 'unit',
          unit: 'hour',
          unitDisplay: 'long',
        });
      } else {
        label = intl.formatList([
          intl.formatNumber(durationHours, {
            style: 'unit',
            unit: 'hour',
            unitDisplay: 'long',
          }),
          intl.formatNumber(durationMinutes, {
            style: 'unit',
            unit: 'minute',
            unitDisplay: 'long',
          }),
        ]);
      }

      return {
        value: item,
        label: label,
      };
    });
  };

  const findBufferOptionFromList = (value) => {
    return bufferOptionsList().filter((buffer) => buffer.value === value);
  };

  const frequencyOptionsList = () => {
    return FREQUENCIES.map((item) => {
      return {
        value: item,
        label: intl.formatNumber(item, { unit: 'minute', style: 'unit' }),
      };
    });
  };

  const findFrequencyOptionFromList = (value) => {
    return frequencyOptionsList().filter(
      (frequency) => frequency.value === value
    );
  };

  return (
    <>
      <Row className="form-group">
        <Col xs={12} sm={4} md={3}>
          <label className="col-form-label input-label">
            <FormattedMessage id="calendar.settings.form.duration.label" />{' '}
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={
                <Tooltip>
                  <FormattedMessage id="calendar.settings.form.duration.desc" />
                </Tooltip>
              }
            >
              <FontAwesomeIcon
                icon={faQuestionCircle}
                className="ml-1 text-body"
              />
            </OverlayTrigger>
          </label>
        </Col>
        <Col xs={12} sm={8} md={9}>
          <PredefinedInput
            predefinedInputs={[15, 30, 45, 60]}
            inputsLabel={intl.formatMessage({ id: 'minute' })}
            customInputLabel={intl.formatMessage({
              id: 'minute',
            })}
            value={settings.duration}
            minValue={1}
            maxValue={720}
            onValueUpdated={(v) => {
              settings.duration = Number(v);
            }}
          />
        </Col>
      </Row>
      <Row className="form-group">
        <Col xs={12} sm={4} md={3}>
          <label className="col-form-label input-label">
            <FormattedMessage id="calendar.settings.form.date_range.label" />{' '}
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={<Tooltip>{makeDateRangeSettingCaption()}</Tooltip>}
            >
              <FontAwesomeIcon
                icon={faQuestionCircle}
                className="ml-1 text-body"
              />
            </OverlayTrigger>
          </label>
        </Col>
        <Col xs={12} sm={8} md={9}>
          <Button
            type="button"
            variant="white"
            onClick={() => {
              setShowDateRangeModal(true);
            }}
          >
            <FormattedMessage id="calendar.settings.form.date_range.btn.edit.label" />
          </Button>
        </Col>
      </Row>
      <Row className="form-group">
        <Col xs={12} sm={4} md={3}>
          <label className="col-form-label input-label" htmlFor="formFrequency">
            <FormattedMessage id="calendar.settings.form.frequency.label" />{' '}
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={
                <Tooltip>
                  <FormattedMessage id="calendar.settings.form.frequency.desc" />{' '}
                  <FormattedMessage id="calendar.settings.form.frequency.tooltip" />
                </Tooltip>
              }
            >
              <FontAwesomeIcon
                icon={faQuestionCircle}
                className="ml-1 text-body"
              />
            </OverlayTrigger>
          </label>
        </Col>
        <Col xs={12} sm={8} md={9}>
          <Select
            inputId="formFrequency"
            options={frequencyOptionsList()}
            defaultValue={findFrequencyOptionFromList(settings.frequency)}
            onChange={(selectedFrequency) => {
              settings.frequency = Number(selectedFrequency.value);
            }}
            isSearchable={false}
            isRtl={i18n.isRTL}
          />
        </Col>
      </Row>
      <Row className="form-group">
        <Col xs={12} sm={4} md={3}>
          <label
            htmlFor="formBufferBeforeMeeting"
            className="col-form-label input-label"
          >
            <FormattedMessage id="calendar.settings.form.buffers.label" />{' '}
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={
                <Tooltip>
                  <FormattedMessage id="calendar.settings.form.buffers.desc" />{' '}
                  <FormattedMessage id="calendar.settings.form.buffers.tooltip" />
                </Tooltip>
              }
            >
              <FontAwesomeIcon
                icon={faQuestionCircle}
                className="ml-1 text-body"
              />
            </OverlayTrigger>
          </label>
        </Col>
        <Col xs={12} sm={8} md={9}>
          <Row>
            <Col xs={12} sm>
              <Form.Group className="mr-2">
                <Form.Label htmlFor="formBufferAfterMeeting">
                  <FormattedMessage id="calendar.settings.form.buffers.before.label" />
                </Form.Label>
                <Select
                  inputId="formBufferBeforeMeeting"
                  options={bufferOptionsList()}
                  defaultValue={findBufferOptionFromList(
                    settings.buffer.before
                  )}
                  onChange={(selectedBuffer) => {
                    settings.buffer.before = Number(selectedBuffer.value);
                  }}
                  isSearchable={false}
                  isRtl={i18n.isRTL}
                />
              </Form.Group>
            </Col>
            <Col xs={12} sm>
              <Form.Group>
                <Form.Label>
                  <FormattedMessage id="calendar.settings.form.buffers.after.label" />
                </Form.Label>
                <Select
                  inputId="formBufferAfterMeeting"
                  options={bufferOptionsList()}
                  defaultValue={findBufferOptionFromList(settings.buffer.after)}
                  onChange={(selectedBuffer) => {
                    settings.buffer.after = Number(selectedBuffer.value);
                  }}
                  isSearchable={false}
                  isRtl={i18n.isRTL}
                />
              </Form.Group>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="form-group">
        <Col xs={12} sm={4} md={3}>
          <label
            className="col-form-label input-label"
            htmlFor="formMeetingsPerDay"
          >
            <FormattedMessage id="calendar.settings.form.max_meetings_per_day.label" />{' '}
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={
                <Tooltip>
                  <FormattedMessage id="calendar.settings.form.max_meetings_per_day.desc" />
                </Tooltip>
              }
            >
              <FontAwesomeIcon
                icon={faQuestionCircle}
                className="ml-1 text-body"
              />
            </OverlayTrigger>
          </label>
        </Col>
        <Col xs={12} sm={8} md={9}>
          <FormGroup>
            <InputGroup>
              <Form.Control
                type="number"
                min={0}
                id="formMeetingsPerDay"
                max={1440}
                defaultValue={settings.meetingsPerDay}
                onChange={(e) => {
                  settings.meetingsPerDay = Number(e.target.value);
                }}
                placeholder={intl.formatMessage({
                  id: 'calendar.settings.form.meetings_per_day.placeholder',
                })}
              />
              <InputGroup.Append>
                <InputGroup.Text>
                  <FormattedMessage id="settings.calendar.advanced.meetings_per_day.meeting.label" />
                </InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
            <Form.Text>
              <FormattedMessage id="calendar.settings.form.max_meetings_per_day.text.leave_blank_for_unlimited" />
            </Form.Text>
          </FormGroup>
        </Col>
      </Row>
      <Row className="form-group">
        <Col xs={12} sm={4} md={3}>
          <label
            className="col-form-label input-label"
            htmlFor="formNoticeOfSchedule"
          >
            <FormattedMessage id="calendar.settings.form.notice_of_schedule.label" />{' '}
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={
                <Tooltip>
                  <FormattedMessage id="calendar.settings.form.notice_of_schedule.desc" />{' '}
                  <FormattedMessage id="calendar.settings.form.notice_of_schedule.tooltip" />
                </Tooltip>
              }
            >
              <FontAwesomeIcon
                icon={faQuestionCircle}
                className="ml-1 text-body"
              />
            </OverlayTrigger>
          </label>
        </Col>
        <Col xs={12} sm={8} md={9}>
          <InputGroup>
            <Form.Control
              type="number"
              id="formNoticeOfSchedule"
              min={0}
              defaultValue={settings.noticeOfSchedule}
              onChange={(e) => {
                settings.noticeOfSchedule = Number(e.target.value);
              }}
              max={525600}
              placeholder={intl.formatMessage({
                id: 'calendar.settings.form.notice_of_schedule.placeholder',
              })}
            />
            <InputGroup.Append>
              <InputGroup.Text id="basic-addon2">
                <FormattedMessage id="settings.calendar.advanced.notice_of_schedule.minute.label" />
              </InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
          <Form.Text>
            <FormattedMessage id="calendar.settings.form.notice_of_schedule.text.leave_blank_for_unlimited" />
          </Form.Text>
        </Col>
      </Row>
      <DateRangeModal
        show={showDateRangeModal}
        select={settings.dateRange}
        onClose={() => {
          setShowDateRangeModal(false);
        }}
        onSave={(type, value) => {
          settings.dateRange = { type, value };
          setShowDateRangeModal(false);
        }}
      />
    </>
  );
};

Settings.propTypes = {
  settings: PropTypes.object,
};

export default withI18n(Settings);
