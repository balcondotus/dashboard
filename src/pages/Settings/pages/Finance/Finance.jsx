import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { IO } from 'services';
import { withIO, withAuthUser } from 'hoc';
import _ from 'lodash';
import {
  Form,
  Button,
  InputGroup,
  Row,
  Col,
  Card,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap';
import { compose } from 'recompose';
import { Helmet } from 'react-helmet-async';
import validator from 'validator';
import { toast } from 'react-toastify';
import { FormattedMessage, useIntl } from 'react-intl';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IMaskInput } from 'react-imask';

const Finance = ({ authUser, io }) => {
  const [saving, setSaving] = useState(false);
  const [iban, setIban] = useState(authUser.iban);
  const intl = useIntl();

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const save = (e) => {
    e.preventDefault();

    if (!_.isEmpty(iban) && !validator.isIBAN(iban))
      return toast.warn(
        intl.formatMessage({ id: 'payments.toast.warn.invalid_iban' })
      );

    setSaving(true);

    io.socket.saveFinanceSettings(iban, (result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        toast.success(
          intl.formatMessage({ id: 'payments.toast.success.saved' })
        );
      }

      setSaving(false);
    });
  };

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'payments.settings.title' })}</title>
      </Helmet>
      <Row>
        <Col xs={12}>
          <Form onSubmit={save} spellCheck={false} disabled={saving}>
            <Card>
              <Card.Header
                as="h4"
                className="d-flex align-items-center justify-content-between"
              >
                <span>
                  <FormattedMessage id="finance.finance.heading" />
                </span>
                <OverlayTrigger
                  delay={{ show: 250, hide: 400 }}
                  placement="top"
                  className="text-right"
                  overlay={
                    <Tooltip>
                      <FormattedMessage id="finance.finance.tooltip.secured" />
                    </Tooltip>
                  }
                >
                  <FontAwesomeIcon icon={faLock} />
                </OverlayTrigger>
              </Card.Header>
              <Card.Body>
                <Form.Row>
                  <Col xs={12} lg={8}>
                    <Form.Label htmlFor="ibanFormControl">
                      <FormattedMessage id="payments.form.iban.label" />
                    </Form.Label>
                    <Row className="align-items-center">
                      <Col xs={12} sm={8} md={8}>
                        <IMaskInput
                          mask="IR00 0000 0000 0000 0000 0000 00"
                          type="text"
                          id="ibanFormControl"
                          value={_.isString(iban) ? iban.substring(2) : null}
                          onAccept={(value, mask) => {
                            setIban(value);
                          }}
                          placeholder="IRxx xxxx xxxx xxxx xxxx xxxx xx"
                          className="dir-ltr form-control"
                        />
                      </Col>
                    </Row>
                  </Col>
                </Form.Row>
              </Card.Body>
              <Card.Footer className="text-right">
                <Button variant="primary" type="submit" disabled={saving}>
                  <FormattedMessage id="payments.form.btn.save.label" />
                </Button>
              </Card.Footer>
            </Card>
          </Form>
        </Col>
      </Row>
    </>
  );
};

Finance.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withIO, withAuthUser)(Finance);
