import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Button,
  OverlayTrigger,
  Tooltip,
  Card,
  Row,
  Col,
  FormControl,
  InputGroup,
  Media,
  ListGroup,
} from 'react-bootstrap';
import { compose } from 'recompose';
import {
  faExclamationTriangle,
  faCheck,
  faLock,
  faExternalLinkAlt,
} from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet-async';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import validator from 'validator';
import _ from 'lodash';
import { toast } from 'react-toastify';
import { appConfig } from 'configs';
import { withIO, withAuthUser, withI18n } from 'hoc';
import { OAuthButton } from 'shared';
import { IO } from 'services';
import { FormattedMessage, FormattedTime, useIntl } from 'react-intl';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import { PhoneVerificationModal } from './components';
import Select, { components } from 'react-select';
import { APP_CONST } from 'consts';
import { faFacebookF, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { TIME_ZONES } from 'utils/TimeZones/TimeZones';
import { findTimeZoneOptionFromList } from 'utils/TimeZones/TimeZones';
import { AsYouType, isValidNumber } from 'libphonenumber-js';

const Account = ({ io, authUser, i18n }) => {
  const history = useHistory();
  const [email, setEmail] = useState(authUser.email.email);
  const [currentPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [newPasswordConfirmation, setConfirmNewPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState(authUser.phone?.phone);
  const [timeZone, setTimeZone] = useState(authUser.timeZone);
  const [locale, setLocale] = useState(authUser.locale);
  const [saving, setSaving] = useState(false);
  const [deleting, setDeleting] = useState(false);
  const [verifyingEmail, setVerifyingEmail] = useState(false);
  const [verifyPhoneModalVisible, setVerifyPhoneModalVisible] = useState(false);
  const intl = useIntl();
  const [savingPassword, setSavingPassword] = useState(false);
  const [confirmAccountDeletion, setConfirmAccountDeletion] = useState(false);

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const deleteAccount = (e) => {
    e.preventDefault();

    if (!confirmAccountDeletion) {
      return toast.info(
        intl.formatMessage({
          id: 'settings.account.toast.error.confirm_account_deletion',
        })
      );
    }

    setDeleting(true);

    io.socket.deleteAccount((result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        toast.success(
          intl.formatMessage({ id: 'account.toast.success.deleted' })
        );
      }

      setDeleting(false);
    });
  };

  const changePassword = (e) => {
    e.preventDefault();

    if (_.isEmpty(currentPassword))
      return toast.warn(
        intl.formatMessage({ id: 'account.toast.warn.enter_current_password' })
      );

    if (
      _.isEmpty(newPassword) ||
      _.isEmpty(newPasswordConfirmation) ||
      newPassword !== newPasswordConfirmation
    )
      return toast.warn(
        intl.formatMessage({ id: 'account.toast.warn.confirm_new_password' })
      );

    setSavingPassword(true);

    io.socket.savePassword(currentPassword, newPassword, (result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        toast.success(
          intl.formatMessage({ id: 'account.toast.success.password_changed' })
        );
      }

      setSavingPassword(false);
    });
  };

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const verifyEmail = (e) => {
    e.preventDefault();

    setVerifyingEmail(true);

    io.socket.sendEmailVerification((result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        toast.success(
          intl.formatMessage({
            id: 'account.toast.success.email_verification_sent',
          })
        );
      }

      setVerifyingEmail(false);
    });
  };

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const saveChanges = (e) => {
    e.preventDefault();

    // Validation
    if (!validator.isEmail(email))
      return toast.warn(
        intl.formatMessage({
          id: 'account.toast.warn.invalid_email',
        })
      );

    if (!_.isEmpty(phoneNumber) && !isValidNumber(phoneNumber))
      return toast.warn(
        intl.formatMessage({ id: 'account.toast.warn.invalid_phone' })
      );

    if (_.isEmpty(timeZone) || _.isNull(moment.tz.zone(timeZone)))
      return toast.warn(
        intl.formatMessage({ id: 'account.toast.warn.invalid_timezone' })
      );

    if (_.isEmpty(locale) || !appConfig.supportedLocales.includes(locale))
      return toast.warn(
        intl.formatMessage({ id: 'account.toast.warn.invalid_locale' })
      );

    setSaving(true);

    io.socket.saveAccountChanges(
      phoneNumber,
      email,
      timeZone,
      locale,
      (result) => {
        if (result.error) {
          toast.error(result.error.message);
        } else {
          toast.success(
            intl.formatMessage({ id: 'account.toast.success.saved' })
          );
        }

        setSaving(false);
      }
    );
  };

  const isOauthLinked = (provider) => {
    return (
      authUser.oauths &&
      authUser.oauths.findIndex((oauth) => oauth.provider === provider) > -1
    );
  };

  const localeOptionsList = () => {
    return appConfig.supportedLocales.map((supportedLocale) => {
      return {
        value: supportedLocale,
        label: intl.formatDisplayName([supportedLocale], {
          type: 'language',
          style: 'long',
        }),
      };
    });
  };

  const findLocaleOptionFromList = (value) => {
    return localeOptionsList().filter(
      (localeOption) => localeOption.value === value
    );
  };

  const TimeZoneOption = (props) => (
    <components.Option
      {...props}
      className={`d-flex align-items-center justify-content-between`}
    />
  );

  const TimeZoneSingleValue = ({ children, ...props }) => (
    <components.SingleValue {...props}>
      {props.data.label}
    </components.SingleValue>
  );

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'account.settings.title' })}</title>
      </Helmet>
      <Card className="mb-3 mb-lg-5">
        <Card.Header
          as="h4"
          className="d-flex align-items-center justify-content-between"
        >
          <span>
            <FormattedMessage id="account.account_details.title" />
          </span>
          <OverlayTrigger
            delay={{ show: 250, hide: 400 }}
            placement="top"
            className="text-right"
            overlay={
              <Tooltip>
                <FormattedMessage id="account.account_details.tooltip.secured" />
              </Tooltip>
            }
          >
            <FontAwesomeIcon icon={faLock} />
          </OverlayTrigger>
        </Card.Header>
        <Card.Body>
          <Form onSubmit={saveChanges} spellCheck={false} disabled={saving}>
            <Form.Row>
              <Col xs={12} lg={8}>
                <Form.Label htmlFor="phoneFormControl">
                  <FormattedMessage id="account.form.phone.label" />
                </Form.Label>
                <Row className="align-items-center">
                  <Col xs={12} sm={8} md={8}>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <OverlayTrigger
                            delay={{ show: 250, hide: 400 }}
                            placement="top"
                            overlay={
                              <Tooltip hidden={authUser.phone?.verified}>
                                <FormattedMessage id="account.tooltip.unverified_phone" />
                              </Tooltip>
                            }
                          >
                            <FontAwesomeIcon
                              icon={
                                authUser.phone?.verified
                                  ? faCheck
                                  : faExclamationTriangle
                              }
                              color={
                                authUser.phone?.verified
                                  ? 'var(--success)'
                                  : 'var(--danger)'
                              }
                            />
                          </OverlayTrigger>
                        </InputGroup.Text>
                      </InputGroup.Prepend>

                      <FormControl
                        className="dir-ltr"
                        type="tel"
                        defaultValue={
                          _.isString(phoneNumber)
                            ? new AsYouType().input(phoneNumber)
                            : phoneNumber
                        }
                        autoComplete="tel"
                        id="phoneFormControl"
                        onChange={(e) => {
                          const formattedInput = new AsYouType().input(
                            e.target.value
                          );
                          e.target.value = formattedInput;
                          setPhoneNumber(formattedInput);
                        }}
                      />
                    </InputGroup>
                  </Col>
                  <Col xs={12} sm={4} md={4} className="mt-2 mt-sm-0">
                    <button
                      type="button"
                      disabled={_.isUndefined(authUser.phone?.phone)}
                      className={`ml-auto ml-sm-0 btn btn-sm btn-outline-primary ${
                        authUser.phone?.verified ? 'd-none' : 'd-block'
                      }`}
                      onClick={() => {
                        setVerifyPhoneModalVisible(true);
                      }}
                    >
                      <FormattedMessage id="account.form.verify_phone.label" />
                    </button>
                  </Col>
                </Row>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col xs={12} lg={8} className="mt-3">
                <Form.Label htmlFor="emailFormControl">
                  <FormattedMessage id="account.form.email.label" />
                </Form.Label>
                <Row className="align-items-center">
                  <Col xs={12} sm={8} md={8}>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>
                          <OverlayTrigger
                            delay={{ show: 250, hide: 400 }}
                            placement="top"
                            overlay={
                              <Tooltip hidden={authUser.email.verified}>
                                <FormattedMessage id="account.tooltip.unverified_email" />
                              </Tooltip>
                            }
                          >
                            <FontAwesomeIcon
                              icon={
                                authUser.email.verified
                                  ? faCheck
                                  : faExclamationTriangle
                              }
                              color={
                                authUser.email.verified
                                  ? 'var(--success)'
                                  : 'var(--danger)'
                              }
                            />
                          </OverlayTrigger>
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control
                        className="dir-ltr"
                        type="email"
                        dir="ltr"
                        defaultValue={email}
                        required
                        id="emailFormControl"
                        autoComplete="email"
                        placeholder={intl.formatMessage({
                          id: 'account.form.email.placeholder',
                        })}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </InputGroup>
                  </Col>
                  <Col xs={12} sm={4} md={4} className="mt-2 mt-sm-0">
                    <button
                      type="button"
                      disabled={verifyingEmail}
                      className={`btn btn-sm btn-outline-primary ml-auto ml-sm-0 ${
                        authUser.email.verified ? 'd-none' : 'd-block'
                      }`}
                      onClick={verifyEmail}
                    >
                      <FormattedMessage id="account.form.btn.verify_email" />
                    </button>
                  </Col>
                </Row>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col xs={12} sm={5} className="mt-3">
                <Form.Group>
                  <Form.Label htmlFor="formTimeZone">
                    <FormattedMessage id="account.form.timezone.label" />
                  </Form.Label>
                  <Select
                    options={TIME_ZONES}
                    required
                    inputId="formTimeZone"
                    isClearable={false}
                    isRtl={i18n.isRTL}
                    components={{
                      Option: TimeZoneOption,
                      SingleValue: TimeZoneSingleValue,
                    }}
                    noOptionsMessage={(obj) => (
                      <FormattedMessage id="account.form.timezone.no_such_timezone.label" />
                    )}
                    maxMenuHeight={200}
                    defaultValue={findTimeZoneOptionFromList(timeZone)}
                    onChange={(v) => setTimeZone(v.value)}
                    formatOptionLabel={(options, { context }) => (
                      <>
                        <span>{options.label}</span>
                        <span>
                          <FormattedTime
                            value={moment.now()}
                            timeZone={options.value}
                            hour="2-digit"
                            minute="2-digit"
                          />
                        </span>
                      </>
                    )}
                    styles={{
                      option: (
                        base,
                        { data, isDisabled, isFocused, isSelected }
                      ) => {
                        return {
                          ...base,
                          fontSize: '0.9rem',
                        };
                      },
                    }}
                  />
                  <Form.Text className="text-muted">
                    <FormattedMessage id="account.form.timezone.text.see_times_in_selected_timezone" />
                  </Form.Text>
                </Form.Group>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col xs={12} sm={5}>
                <Form.Group>
                  <Form.Label htmlFor="localeFormControl">
                    <FormattedMessage id="account.form.locale.label" />
                  </Form.Label>
                  <Select
                    inputId="localeFormControl"
                    options={appConfig.supportedLocales.map(
                      (supportedLocale) => {
                        return {
                          value: supportedLocale,
                          label: intl.formatMessage({
                            id: `locale_${supportedLocale}`,
                          }),
                        };
                      }
                    )}
                    required
                    isRtl={i18n.isRTL}
                    isSearchable={false}
                    autoComplete="language"
                    maxMenuHeight={200}
                    defaultValue={findLocaleOptionFromList(locale)}
                    onChange={(v) => setLocale(v.value)}
                  />
                </Form.Group>
              </Col>
            </Form.Row>
            <div className="text-right">
              <Button variant="primary" type="submit" disabled={saving}>
                <FormattedMessage id="account.form.btn.save.label" />
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>

      <Card className="mb-3 mb-lg-5">
        <Card.Header as="h4">
          <FormattedMessage id="account.change_password.title" />
        </Card.Header>

        <Card.Body>
          <Form
            onSubmit={changePassword}
            spellCheck={false}
            disabled={savingPassword}
          >
            <Form.Row>
              <Col xs={12} md={6}>
                <Form.Group controlId="formCurrentPassword">
                  <Form.Label>
                    <FormattedMessage id="account.form.password.label" />
                  </Form.Label>
                  <Form.Control
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setOldPassword(e.target.value)}
                    autoComplete="current-password"
                    dir="ltr"
                    type="password"
                  />
                </Form.Group>
              </Col>
            </Form.Row>

            <Form.Row>
              <Col xs={12} md={6}>
                <Form.Group controlId="formNewPassword">
                  <Form.Label>
                    <FormattedMessage id="account.form.new_password.label" />
                  </Form.Label>
                  <Form.Control
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    autoComplete="new-password"
                    dir="ltr"
                    type="password"
                  />
                </Form.Group>
              </Col>
              <Col xs={12} md={6} className="mt-2 mt-md-0">
                <Form.Group controlId="formNewPasswordConfirm">
                  <Form.Label>
                    <FormattedMessage id="account.form.confirm_new_password.label" />
                  </Form.Label>
                  <Form.Control
                    minLength={appConfig.validation.minPassword}
                    onChange={(e) => setConfirmNewPassword(e.target.value)}
                    type="password"
                    dir="ltr"
                    autoComplete="new-password"
                    placeholder={intl.formatMessage({
                      id: 'account.form.confirm_new_password.placeholder',
                    })}
                  />
                </Form.Group>
              </Col>
            </Form.Row>
            <div className="text-right">
              <Button variant="warning" type="submit" disabled={savingPassword}>
                <FormattedMessage id="account.form.btn.change_password" />
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>

      <Card className="mb-3 mb-lg-5">
        <Card.Header>
          <Card.Title as="h4">
            <FormattedMessage id="account.linked_social_networks.title" />
          </Card.Title>
        </Card.Header>

        <Card.Body>
          <Form>
            <ListGroup className="list-group-lg list-group-flush list-group-no-gutters">
              <ListGroup.Item>
                <Media>
                  <FontAwesomeIcon
                    icon={faGoogle}
                    className="list-group-icon mt-1"
                    fixedWidth
                  />

                  <Media.Body>
                    <Row className="align-items-center">
                      <Col sm className="mb-2 mb-sm-0">
                        <h5 className="mb-0">
                          <FormattedMessage id="account.linked_oauth_services.google.title" />
                        </h5>
                        <span className="font-size-sm">www.google.com</span>
                      </Col>

                      <Col sm="auto">
                        <OAuthButton
                          as="a"
                          size="sm"
                          variant="white"
                          href="#"
                          provider={appConfig.supportedOauthProvider.google}
                          redirectTo={history.location.pathname}
                          action={
                            isOauthLinked(
                              appConfig.supportedOauthProvider.google
                            )
                              ? 'unlink'
                              : 'link'
                          }
                        >
                          {isOauthLinked(
                            appConfig.supportedOauthProvider.google
                          ) ? (
                            <FormattedMessage id="account.linked_oauth_services.btn.disconnect" />
                          ) : (
                            <FormattedMessage id="account.linked_oauth_services.btn.connect" />
                          )}
                        </OAuthButton>
                      </Col>
                    </Row>
                  </Media.Body>
                </Media>
              </ListGroup.Item>

              <ListGroup.Item>
                <Media>
                  <FontAwesomeIcon
                    icon={faFacebookF}
                    className="list-group-icon mt-1"
                    fixedWidth
                  />

                  <Media.Body>
                    <Row className="align-items-center">
                      <Col sm className="mb-2 mb-sm-0">
                        <h5 className="mb-0">
                          <FormattedMessage id="account.linked_oauth_services.facebook.title" />
                        </h5>
                        <span className="font-size-sm">www.facebook.com</span>
                      </Col>

                      <Col sm="auto">
                        <OAuthButton
                          as="a"
                          size="sm"
                          variant="white"
                          href="#"
                          provider={appConfig.supportedOauthProvider.facebook}
                          redirectTo={history.location.pathname}
                          action={
                            isOauthLinked(
                              appConfig.supportedOauthProvider.facebook
                            )
                              ? 'unlink'
                              : 'link'
                          }
                        >
                          {isOauthLinked(
                            appConfig.supportedOauthProvider.facebook
                          ) ? (
                            <FormattedMessage id="account.linked_oauth_services.btn.disconnect" />
                          ) : (
                            <FormattedMessage id="account.linked_oauth_services.btn.connect" />
                          )}
                        </OAuthButton>
                      </Col>
                    </Row>
                  </Media.Body>
                </Media>
              </ListGroup.Item>
            </ListGroup>
          </Form>
        </Card.Body>
        <Card.Footer className="small text-info">
          <FormattedMessage id="account.form.oauth.text.use_for_signin" />
        </Card.Footer>
      </Card>

      <Card>
        <Card.Header>
          <Card.Title as="h4">
            <FormattedMessage id="account.delete_account.title" />
          </Card.Title>
        </Card.Header>

        <Card.Body>
          <Card.Text>
            <FormattedMessage id="settings.account.delete_account.desc" />
          </Card.Text>

          <Form.Group className="form-group">
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id="deleteAccountCheckbox"
                onChange={(e) => setConfirmAccountDeletion(e.target.checked)}
              />
              <label
                className="custom-control-label"
                htmlFor="deleteAccountCheckbox"
              >
                <FormattedMessage id="settings.account.delete_account.confirm_checkbox.label" />
              </label>
            </div>
          </Form.Group>

          <div className="d-flex justify-content-end flex-column flex-sm-row">
            <Button
              as="a"
              variant="white"
              className="mr-sm-2 mb-3 mb-sm-0"
              href={APP_CONST.ROUTES.PRIVACY}
              target="blank"
            >
              <span className="mr-2">
                <FormattedMessage id="settings.account.delete_account.btn.read_more" />
              </span>
              <FontAwesomeIcon icon={faExternalLinkAlt} />
            </Button>

            <Button
              type="button"
              variant="danger"
              disabled={deleting}
              onClick={deleteAccount}
            >
              <FormattedMessage id="account.form.btn.delete_account.label" />
            </Button>
          </div>
        </Card.Body>
      </Card>

      <PhoneVerificationModal
        show={verifyPhoneModalVisible}
        phoneNumber={phoneNumber}
        onHide={() => {
          setVerifyPhoneModalVisible(false);
        }}
      />
    </>
  );
};

Account.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
  authUser: PropTypes.any.isRequired,
};

export default compose(withIO, withAuthUser, withI18n)(Account);
