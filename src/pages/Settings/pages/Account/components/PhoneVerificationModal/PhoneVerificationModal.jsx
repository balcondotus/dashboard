import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withIO } from 'hoc';
import { IO } from 'services';
import { Modal, Button, Form, Col } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { appConfig } from 'configs';
import { FormattedMessage, useIntl } from 'react-intl';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { formatIncompletePhoneNumber } from 'libphonenumber-js';

const PhoneVerificationModal = ({ phoneNumber, io, show, onHide }) => {
  const [isVerifying, setIsVerifying] = useState();
  const [verificationCode, setVerificationCode] = useState();
  const [isVerified, setIsVerified] = useState();
  const intl = useIntl();

  useEffect(() => {
    // Reset props as it hides
    // This get called first time because the modal is placed directly inside of the parent, but that's fine.
    if (!show) {
      setIsVerifying(false);
      setVerificationCode(null);
      setIsVerified(false);
    }
  }, [show]);

  const sendVerificationCode = (e) => {
    if (e) e.preventDefault();

    if (isVerified || isVerifying) return;

    io.socket.sendPhoneVerification((result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        toast.info(
          intl.formatMessage({
            id: 'verify_phone.toast.info.verification_sent',
          })
        );
      }
    });
  };

  useEffect(() => {
    if (show) sendVerificationCode();
  }, [show]);

  const verify = () => {
    setIsVerifying(true);

    io.socket.verifyPhoneVerificationCode(verificationCode, (result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        setIsVerifying(false);
        setIsVerified(true);

        toast.success(
          intl.formatMessage({ id: 'verify_phone.toast.success.verified' })
        );
      }
    });
  };

  const shouldDisableForm = () => {
    return _.isEmpty(verificationCode) || isVerifying || isVerified;
  };

  return (
    <Modal centered show={show} onHide={onHide}>
      <Modal.Body className="p-sm-5 text-center">
        <div class="modal-close">
          <Button
            type="button"
            variant="icon"
            size="sm"
            className="btn-ghost-secondary"
            data-dismiss="modal"
            onClick={onHide}
            style={{ transition: 'none' }}
            aria-label="Close"
          >
            <FontAwesomeIcon icon={faTimes} />
          </Button>
        </div>
        <div className="mb-4">
          <img
            className="avatar avatar-xxl avatar-4by3"
            src="/assets/images/unlock.svg"
            alt="Enter Verification Code"
          />
        </div>

        <div className="mb-5">
          <h1 className="display-4">
            <FormattedMessage id="phone_verification_modal.title" />
          </h1>

          <p className="mb-0">
            <FormattedMessage
              id="phone_verification_modal.subtitle"
              values={{
                phoneNumber: formatIncompletePhoneNumber(phoneNumber),
              }}
            />
          </p>
        </div>

        <Form
          spellCheck={false}
          onSubmit={verify}
          disabled={shouldDisableForm()}
        >
          <Form.Group controlId="verificationCodeFormControl">
            <Form.Control
              className="dir-ltr"
              type="text"
              size="lg"
              minLength={appConfig.validation.phoneVerificationCodeLength}
              maxLength={appConfig.validation.phoneVerificationCodeLength}
              autoComplete="one-time-code"
              onChange={(e) => setVerificationCode(e.target.value)}
            />
          </Form.Group>

          <Button
            type="submit"
            size="lg"
            variant="primary"
            block
            disabled={shouldDisableForm()}
            variant={isVerified ? 'success' : 'primary'}
            onClick={verify}
          >
            {!isVerified ? (
              <FormattedMessage id="verify_phone.form.btn.verify.label" />
            ) : (
              <FormattedMessage id="verify_phone.form.btn.verified.label" />
            )}
          </Button>

          <div className="text-center mt-3">
            <FormattedMessage
              id="phone_verification_modal.resend.label"
              values={{
                resendLink: (chunks) => (
                  <a href="#" onClick={sendVerificationCode}>
                    {chunks}
                  </a>
                ),
              }}
            />
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

PhoneVerificationModal.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
  show: PropTypes.bool,
  onHide: PropTypes.func.isRequired,
  phoneNumber: PropTypes.string,
};

PhoneVerificationModal.defaultProps = {
  show: false,
  phoneNumber: undefined,
};

export default withIO(PhoneVerificationModal);
