import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withIO, withAuthUser, withI18n } from 'hoc';
import { IO } from 'services';
import {
  Form,
  Button,
  InputGroup,
  Card,
  OverlayTrigger,
  Tooltip,
  Col,
} from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import { appConfig } from 'configs';
import { UploadableAvatar } from 'shared';
import { toast } from 'react-toastify';
import { FormattedMessage, useIntl } from 'react-intl';
import { findGenderFromList, GENDERS } from './Genders';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import ReactQuill from 'react-quill';
import _ from 'lodash';
import { IMaskInput } from 'react-imask';
import Select from 'react-select';
import BeatLoader from 'react-spinners/BeatLoader';

// TODO: Limit Quill's text content to 65535
const Profile = ({ io, authUser, i18n }) => {
  const [saving, setSaving] = useState(false);
  const [username, setUsername] = useState(authUser.username);
  const [firstName, setFirstName] = useState(authUser.profile.firstName);
  const [lastName, setLastName] = useState(authUser.profile.lastName);
  const [gender, setGender] = useState(authUser.profile.gender);
  const [rate, setRate] = useState(authUser.rate);
  const [tagline, setTagline] = useState(authUser.tagline);
  const [description, setDescription] = useState(authUser.description);
  const [newAvatar, setNewAvatar] = useState(null);
  const intl = useIntl();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    io.socket.getDefaultEvent((result) => {
      if (result.error) {
        toast.error(result.error.message);
      } else {
        const { data } = result;

        setRate(data.payment.rate);
        setTagline(data.tagline);
        setDescription(_.unescape(data.description));
      }

      setLoading(false);
    });
  }, []);

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const saveChanges = (e) => {
    e.preventDefault();

    setSaving(true);

    io.socket.saveProfileSettings(
      username,
      firstName,
      lastName,
      gender,
      rate,
      tagline,
      description,
      newAvatar,
      (result) => {
        if (result.error) {
          toast.error(result.error.message);
        } else {
          toast.success(
            intl.formatMessage({ id: 'profile.toast.success.saved' })
          );

          // Reset to default to prevent saving the local temp file again
          setNewAvatar(undefined);
        }

        setSaving(false);
      }
    );
  };

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'profile.settings.title' })}</title>
      </Helmet>
      <div className={`text-center ${loading ? 'd-block' : 'd-none'}`}>
        <BeatLoader size={20} color="#377dff" />
      </div>

      <div className={`${loading ? 'd-none' : 'd-block'}`}>
        <Card className="mb-3 mb-lg-5">
          <div className="profile-cover">
            <div className="profile-cover-img-wrapper">
              <picture>
                <source
                  srcSet="/assets/images/default-cover-picture.webp"
                  type="image/webp"
                />
                <source
                  srcSet="/assets/images/default-cover-picture.jpg"
                  type="image/jpg"
                />
                <img
                  className="profile-cover-img"
                  src="/assets/images/default-cover-picture.jpg"
                  alt="Default Cover Picture"
                />
              </picture>
            </div>
          </div>

          <label
            className="avatar avatar-xxl avatar-circle avatar-border-lg avatar-uploader profile-cover-avatar"
            htmlFor="avatarUploader"
          >
            <UploadableAvatar
              onFileUploaded={(serverId) => setNewAvatar(serverId)}
              disabled={saving}
            />
          </label>
          <Card.Body />
        </Card>

        <Card>
          <Card.Body>
            <Form onSubmit={saveChanges} spellCheck={false} disabled={saving}>
              <Form.Row>
                <Col xs={12} md={4}>
                  <Form.Group controlId="usernameFormControl">
                    <Form.Label>
                      <FormattedMessage id="profile.form.username.label" />
                    </Form.Label>
                    <Form.Control
                      type="text"
                      minLength={appConfig.validation.usernameMinLength}
                      maxLength={appConfig.validation.usernameMaxLength}
                      defaultValue={username}
                      pattern={appConfig.validation.usernamePattern.source}
                      onChange={(e) => setUsername(e.target.value)}
                      autoComplete="username"
                      className="dir-ltr"
                    />
                  </Form.Group>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col xs={12} sm={5} md={4} lg={3}>
                  <Form.Group controlId="firstNameFormControl">
                    <Form.Label>
                      <FormattedMessage id="profile.form.firstname.label" />
                    </Form.Label>
                    <Form.Control
                      type="text"
                      dir="auto"
                      required
                      autoComplete="given-name"
                      defaultValue={firstName}
                      onChange={(e) => setFirstName(e.target.value)}
                      maxLength="255"
                    />
                  </Form.Group>
                </Col>
                <Col xs={12} sm={7} md={5} lg={4}>
                  <Form.Group controlId="formLastName">
                    <Form.Label>
                      <FormattedMessage id="profile.form.lastname.label" />
                    </Form.Label>
                    <Form.Control
                      type="text"
                      dir="auto"
                      autoComplete="family-name"
                      required
                      defaultValue={lastName}
                      onChange={(e) => setLastName(e.target.value)}
                      maxLength="255"
                    />
                  </Form.Group>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col xs={12} md={4}>
                  <Form.Group>
                    <Form.Label htmlFor="genderFormControl">
                      <span className="mr-1">
                        <FormattedMessage id="profile.form.gender.label" />
                      </span>
                      <OverlayTrigger
                        delay={{ show: 250, hide: 400 }}
                        placement="top"
                        overlay={
                          <Tooltip>
                            <FormattedMessage id="profile.form.gender.tooltip.help_us" />
                          </Tooltip>
                        }
                      >
                        <FontAwesomeIcon icon={faLock} />
                      </OverlayTrigger>
                    </Form.Label>

                    <Select
                      options={GENDERS}
                      inputId="genderFormControl"
                      isRtl={i18n.isRTL}
                      placeholder={intl.formatMessage({
                        id: 'settings.profile.form.gender.placeholder',
                      })}
                      isSearchable={false}
                      defaultValue={findGenderFromList(gender)}
                      onChange={(v) => setGender(v.value)}
                      formatOptionLabel={(options, { context }) =>
                        intl.formatMessage({ id: options.label })
                      }
                    />
                  </Form.Group>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col xs={12} md={4}>
                  <Form.Group>
                    <Form.Label htmlFor="rateFormControl">
                      <span className="mr-1">
                        <FormattedMessage id="profile.form.rate.label" />
                      </span>
                      <OverlayTrigger
                        delay={{ show: 250, hide: 400 }}
                        placement="top"
                        overlay={
                          <Tooltip>
                            <FormattedMessage id="profile.form.rate.text.how_to_choose_rate" />
                          </Tooltip>
                        }
                      >
                        <FontAwesomeIcon icon={faQuestionCircle} />
                      </OverlayTrigger>
                    </Form.Label>
                    <InputGroup>
                      <IMaskInput
                        mask={Number}
                        radix="."
                        max={appConfig.validation.eventRateMaxLength}
                        min={0}
                        thousandsSeparator=","
                        normalizeZeros
                        unmask={true}
                        id="rateFormControl"
                        signed={false}
                        scale={0}
                        value={`${rate}`}
                        onAccept={(value, mask) => {
                          setRate(Number(value));
                        }}
                        placeholder="75,000"
                        required
                        className="dir-ltr form-control"
                      />

                      <InputGroup.Append>
                        <InputGroup.Text>
                          <FormattedMessage id="profile.form.rate.toman.label" />
                        </InputGroup.Text>
                      </InputGroup.Append>
                    </InputGroup>
                  </Form.Group>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col xs={12} md={4}>
                  <Form.Group controlId="taglineFormControl">
                    <Form.Label>
                      <FormattedMessage id="profile.form.tagline.label" />
                    </Form.Label>
                    <Form.Control
                      type="text"
                      dir="auto"
                      defaultValue={tagline}
                      maxLength="255"
                      onChange={(e) => setTagline(e.target.value)}
                    />
                  </Form.Group>
                </Col>
              </Form.Row>
              <Form.Row>
                <Col xs={12} md={5}>
                  <Form.Group controlId="formDescription">
                    <Form.Label>
                      <FormattedMessage id="profile.form.description.label" />
                    </Form.Label>
                    <div className="quill-custom">
                      <ReactQuill
                        value={description}
                        modules={{
                          toolbar: [
                            [
                              'bold',
                              'italic',
                              'underline',
                              { list: 'bullet' },
                              { list: 'ordered' },
                            ],
                          ],
                        }}
                        onChange={setDescription}
                        placeholder={intl.formatMessage({
                          id: 'settings.profile.form.description.placeholder',
                        })}
                        style={{ minHeight: '15rem' }}
                      />
                    </div>
                  </Form.Group>
                </Col>
              </Form.Row>
              <Button
                className="float-right"
                type="submit"
                variant="primary"
                disabled={saving}
              >
                <FormattedMessage id="profile.form.btn.save.label" />
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    </>
  );
};

Profile.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
  authUser: PropTypes.object.isRequired,
};

export default compose(withIO, withAuthUser, withI18n)(Profile);
