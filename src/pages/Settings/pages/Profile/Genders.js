import _ from 'lodash';

export const GENDERS = [
  { value: 'MALE', label: 'gender_male' },
  { value: 'FEMALE', label: 'gender_female' },
  { value: 'OTHER', label: 'gender_other' },
];

/**
 * Finds a gender from the list above.
 *
 * @param {string} value
 */
export const findGenderFromList = (value) => {
  // Gender is null by default for new users
  if (!_.isString(value)) return;

  return GENDERS.filter(
    (genderOption) => genderOption.value.toUpperCase() === value.toUpperCase()
  );
};
