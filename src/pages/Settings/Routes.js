import { APP_CONST } from 'consts';
import { Notifications, Account, Finance, Profile, Calendar } from './pages';

export const ROUTES = [
  {
    path: APP_CONST.ROUTES.SETTINGS,
    component: Profile,
  },
  {
    path: APP_CONST.ROUTES.NOTIFICATION_SETTINGS,
    component: Notifications,
  },
  {
    path: APP_CONST.ROUTES.ACCOUNT_SETTINGS,
    component: Account,
  },
  {
    path: APP_CONST.ROUTES.FINANCE_SETTINGS,
    component: Finance,
  },
  {
    path: APP_CONST.ROUTES.PROFILE_SETTINGS,
    component: Profile,
  },
  {
    path: APP_CONST.ROUTES.CALENDAR_SETTINGS,
    component: Calendar,
  },
];
