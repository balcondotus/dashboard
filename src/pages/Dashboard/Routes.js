import { default as Meetings } from 'pages/Meetings/Meetings';
import { default as Settings } from 'pages/Settings/Settings';
import { default as Payments } from 'pages/Payments/Payments';
import { APP_CONST } from 'consts';

export const ROUTES = [
  {
    path: APP_CONST.ROUTES.HOME,
    component: Meetings,
  },
  {
    path: APP_CONST.ROUTES.SETTINGS,
    component: Settings,
  },
  {
    path: APP_CONST.ROUTES.NOTIFICATION_SETTINGS,
    component: Settings,
  },
  {
    path: APP_CONST.ROUTES.ACCOUNT_SETTINGS,
    component: Settings,
  },
  {
    path: APP_CONST.ROUTES.FINANCE_SETTINGS,
    component: Settings,
  },
  {
    path: APP_CONST.ROUTES.PROFILE_SETTINGS,
    component: Settings,
  },
  {
    path: APP_CONST.ROUTES.CALENDAR_SETTINGS,
    component: Settings,
  },
  {
    path: APP_CONST.ROUTES.PAYMENTS,
    component: Payments,
  },
];
