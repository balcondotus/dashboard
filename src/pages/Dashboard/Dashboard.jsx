import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { Navbar, Footer } from 'components';
import { withIO } from 'hoc';
import { IO } from 'services';
import { ROUTES } from './Routes';
import './Dashboard.scss';

const Dashboard = ({ io }) => {
  useEffect(() => {
    // Connect the socket here if already not.
    // We only need the socket connection in the dashboard.
    io.socket.connect();

    return () => {
      // Disconnect the socket because we don't need it anymore.
      io.socket.disconnect();
    };
  }, []);

  return (
    <div className="dashboard-component bg-light">
      <Navbar />
      <div className="my-5">
        {ROUTES.map((route) => {
          return (
            <Route
              key={route.path}
              path={route.path}
              exact
              component={route.component}
            />
          );
        })}
      </div>
      <Footer />
    </div>
  );
};

Dashboard.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(Dashboard);
