import moment from 'moment';
import _ from 'lodash';

const intervalPattern = /^(?<hours>[0-9]{2}):(?<minutes>[0-9]{2})$/;

/**
 * Sorts intervals in ascending order.
 *
 * @param {Array} list Array of intervals.
 *
 * @returns {Array} Sorted intervals.
 */
const sortIntervals = (list) => {
  return _.sortBy(list, (date) => moment.utc(date.start.value, 'HH:mm'));
};

/**
 * Finds overlapping intervals.
 * It sorts the given list natively.
 *
 * @param {Array} list Array of intervals.
 */
const findOverlapingIntervals = (list) => {
  const sortedList = sortIntervals(list);

  const overlappingIntervals = sortedList.reduce(
    (result, current, idx, arr) => {
      // get the previous range
      if (idx === 0) return result;

      const previous = arr[idx - 1];

      const currentRange = moment.range(
        moment.utc(current.start.value, 'HH:mm'),
        moment.utc(current.end.value, 'HH:mm')
      );
      const previousRange = moment.range(
        moment.utc(previous.start.value, 'HH:mm'),
        moment.utc(previous.end.value, 'HH:mm')
      );
      const overlap = currentRange.overlaps(previousRange);

      if (overlap) {
        if (!result.includes(current)) result.push(current);
        if (!result.includes(previous)) result.push(previous);
      }

      return result;
    },
    []
  );

  return overlappingIntervals;
};

/**
 * Builds a UTC moment().
 *
 * @param {string} date UTC date
 * @param {String} interval UTC interval
 *
 * @returns {moment.Moment}
 */
const makeMoment = (date, interval) => {
  const { hours, minutes } = interval.match(intervalPattern).groups;

  return moment
    .utc(date)
    .set('hour', Number(hours))
    .set('minute', Number(minutes));
};

/**
 * Tests time string.
 *
 * @param {String} time
 *
 * @returns {Boolean} true if regex.test() passes.
 */
const testTime = (time) => {
  return intervalPattern.test(time);
};

/**`
 * Checks if given time is within given date.`
 *
 * @param {string} date UTC date
 * @param {String} time UTC time
 */
const isTimeWithinDate = (date, time) => {
  return moment.utc(date).isSame(makeMoment(date, time), 'date');
};

/**
 * Removes ids and errors.
 * UTC natively applied.
 *
 * @param {Array} intervals
 *
 * @returns {Array} Flatten intervals
 */
const flattenIntervals = (intervals) => {
  return _.map(intervals, (interval) => {
    interval.start = interval.start.value;
    interval.end = interval.end.value;

    delete interval.id;
    delete interval.errors;

    return interval;
  });
};

/**
 * Get duration from an interval.
 *
 * @param {string} startTime Formatted as `HH:mm`
 * @param {string} endTime Formatted as `HH:mm`
 */
const getDurationFromInterval = (startTime, endTime) => {
  const format = 'HH:mm';

  const startMoment = moment.utc(startTime, format);
  const endMoment = moment.utc(endTime, format);

  return moment.duration(endMoment.diff(startMoment));
};

/**
 * Format given duration to the given `format` and `locale`.
 *
 * @param {moment.Duration} duration
 * @param {string} locale `en` is default
 * @param {string} format `HH:mm` is default
 */
const formatDuration = (duration, locale = 'en', format = 'HH:mm') => {
  const durationAsMills = duration.asMilliseconds();

  return moment.utc(durationAsMills).locale(locale).format(format);
};

export default {
  sortIntervals,
  flattenIntervals,
  testTime,
  getDurationFromInterval,
  isTimeWithinDate,
  findOverlapingIntervals,
  makeMoment,
  formatDuration,
};
