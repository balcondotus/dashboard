import { Subject } from 'rxjs';

class AuthUser {
  /**
   * A list of subscribers.
   */
  #handlers = new Subject();

  subscribe(observer) {
    this.#handlers.subscribe(observer);
  }

  unsubscribe() {
    this.#handlers.unsubscribe();
  }

  fire(value) {
    this.#handlers.next(value);
  }
}

export default AuthUser;
