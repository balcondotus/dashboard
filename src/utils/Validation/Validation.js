import validator from 'validator';
import { appConfig } from 'configs';
import _ from 'lodash';

/**
 * Validates password.
 *
 * @param {String} password
 */
const validatePassword = (password) => {
  return (
    _.isString(password) &&
    !_.isEmpty(password) &&
    validator.isLength(password, { min: appConfig.validation.minPassword })
  );
};

export default { validatePassword };
