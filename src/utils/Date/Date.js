import moment from 'jalali-moment';

const isPast = (date) => {
  return moment().isSameOrAfter(date);
};

const isHoursPast = (value, number) => {
  return moment(value).add(number, 'hour').isSameOrAfter(moment());
};

/**
 *
 * @param {moment.MomentInput} date
 * @param {moment.MomentInput} time
 *
 * @returns {moment.Moment}
 */
const swapTime = (date, time) => {
  const timeMoment = moment(time);
  return moment(date)
    .set('hour', timeMoment.get('hour'))
    .set('minute', timeMoment.get('minute'))
    .set('second', timeMoment.get('second'))
    .set('millisecond', timeMoment.get('millisecond'));
};

/**
 * Returns weekday name of given date.
 *
 * @param {moment.MomentInput} date
 *
 * @returns {String} weekday name in English and lowercase.
 */
const getDateWeekDayName = (date) => {
  const weekDayName = moment(date).locale('en').format('ddd');

  return weekDayName.toLowerCase();
};

/**
 * Formats given date based on given locale and templates.
 *
 * @param {moment.MomentInput} date
 * @param {String} locale
 * @param {String} gregorianTemplate
 * @param {String} jalaliTemplate
 */
const formatDateBasedOnLocale = (
  date,
  locale,
  gregorianTemplate,
  jalaliTemplate
) => {
  return moment(date)
    .format(locale === 'fa' ? jalaliTemplate : gregorianTemplate);
};

export default {
  isPast,
  isHoursPast,
  swapTime,
  getDateWeekDayName,
  formatDateBasedOnLocale,
};
