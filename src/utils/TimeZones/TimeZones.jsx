export const TIME_ZONES = [
  {
    label: 'US/Canada',
    options: [
      { value: 'Pacific/Honolulu', label: 'Hawaii Time' },
      { value: 'America/Anchorage', label: 'Alaska Time' },
      { value: 'America/Los_Angeles', label: 'Pacific Time - US & Canada' },
      { value: 'America/Phoenix', label: 'Arizona Time' },
      { value: 'America/Denver', label: 'Mountain Time - US & Canada' },
      { value: 'America/Chicago', label: 'Central Time - US & Canada' },
      { value: 'America/New_York', label: 'Eastern Time - US & Canada' },
      { value: 'America/St_Johns', label: 'Newfoundland Time' },
    ],
  },

  {
    label: 'America',
    options: [
      { value: 'America/Adak', label: 'America/Adak' },
      { value: 'America/Santa_Isabel', label: 'America/Santa Isabel' },
      {
        value: 'America/Guatemala',
        label: 'Saskatchewan, Guatemala, Costa Rica Time',
      },
      { value: 'America/Mazatlan', label: 'America/Mazatlan' },
      { value: 'America/Bogota', label: 'Bogota, Jamaica, Lima Time' },
      { value: 'America/Mexico_City', label: 'Mexico City Time' },
      { value: 'America/Asuncion', label: 'Asuncion Time' },
      { value: 'America/Campo_Grande', label: 'America/Campo Grande' },
      { value: 'America/Caracas', label: 'Caracas Time' },
      { value: 'America/Havana', label: 'America/Havana' },
      { value: 'America/Santo_Domingo', label: 'Atlantic Standard Time' },
      { value: 'America/Argentina/Buenos_Aires', label: 'Buenos Aires Time' },
      { value: 'America/Goose_Bay', label: 'Atlantic Time' },
      { value: 'America/Montevideo', label: 'Montevideo Time' },
      { value: 'America/Santiago', label: 'Santiago Time' },
      { value: 'America/Sao_Paulo', label: 'Brasilia Time' },
      { value: 'America/Godthab', label: 'America/Godthab' },
      { value: 'America/Miquelon', label: 'America/Miquelon' },
      { value: 'America/Noronha', label: 'America/Noronha' },
    ],
  },

  {
    label: 'Africa',
    options: [
      { value: 'Africa/Lagos', label: 'West Africa Time' },
      { value: 'Africa/Cairo', label: 'Africa/Cairo' },
      { value: 'Africa/Johannesburg', label: 'Central Africa Time' },
      { value: 'Africa/Windhoek', label: 'Africa/Windhoek' },
    ],
  },

  {
    label: 'Asia',
    options: [
      { value: 'Asia/Amman', label: 'Jordan Time' },
      { value: 'Asia/Baghdad', label: 'Baghdad, East Africa Time' },
      { value: 'Asia/Beirut', label: 'Lebanon Time' },
      { value: 'Asia/Damascus', label: 'Syria Time' },
      { value: 'Asia/Gaza', label: 'Asia/Gaza' },
      { value: 'Asia/Jerusalem', label: 'Israel Time' },
      { value: 'Asia/Tehran', label: 'Tehran Time' },
      { value: 'Asia/Baku', label: 'Asia/Baku' },
      { value: 'Asia/Dubai', label: 'Dubai Time' },
      { value: 'Asia/Yerevan', label: 'Asia/Yerevan' },
      { value: 'Asia/Kabul', label: 'Kabul Time' },
      { value: 'Asia/Karachi', label: 'Pakistan, Maldives Time' },
      { value: 'Asia/Yekaterinburg', label: 'Yekaterinburg Time' },
      { value: 'Asia/Kolkata', label: 'India, Sri Lanka Time' },
      { value: 'Asia/Kathmandu', label: 'Kathmandu Time' },
      { value: 'Asia/Dhaka', label: 'Asia/Dhaka' },
      { value: 'Asia/Omsk', label: 'Asia/Omsk' },
      { value: 'Asia/Rangoon', label: 'Asia/Rangoon' },
      { value: 'Asia/Jakarta', label: 'Indochina Time' },
      { value: 'Asia/Krasnoyarsk', label: 'Krasnoyarsk Time' },
      { value: 'Asia/Irkutsk', label: 'Asia/Irkutsk' },
      { value: 'Asia/Shanghai', label: 'China, Singapore, Perth' },
      { value: 'Asia/Tokyo', label: 'Japan, Korea Time' },
      { value: 'Asia/Yakutsk', label: 'Asia/Yakutsk' },
      { value: 'Asia/Vladivostok', label: 'Asia/Vladivostok' },
      { value: 'Asia/Kamchatka', label: 'Pacific/Majuro' },
    ],
  },

  {
    label: 'Atlantic',
    options: [
      { value: 'Atlantic/Cape_Verde', label: 'Cape Verde Time' },
      { value: 'Atlantic/Azores', label: 'Azores Time' },
    ],
  },

  {
    label: 'Australia',
    options: [
      { value: 'Australia/Perth', label: 'Australia/Perth' },
      { value: 'Australia/Eucla', label: 'Australia/Eucla' },
      { value: 'Australia/Adelaide', label: 'Adelaide Time' },
      { value: 'Australia/Darwin', label: 'Australia/Darwin' },
      { value: 'Australia/Brisbane', label: 'Brisbane Time' },
      { value: 'Australia/Sydney', label: 'Sydney, Melbourne Time' },
      { value: 'Australia/Lord_Howe', label: 'Australia/Lord Howe' },
    ],
  },

  {
    label: 'UTC',
    options: [{ value: 'UTC', label: 'UTC Time' }],
  },

  {
    label: 'Europe',
    options: [
      { value: 'Europe/London', label: 'UK, Ireland, Lisbon Time' },
      { value: 'Europe/Berlin', label: 'Central European Time' },
      { value: 'Europe/Helsinki', label: 'Eastern European Time' },
      { value: 'Europe/Minsk', label: 'Minsk Time' },
      { value: 'Europe/Moscow', label: 'Moscow Time' },
    ],
  },

  {
    label: 'Pacific',
    options: [
      { value: 'Pacific/Pago_Pago', label: 'Pacific/Pago Pago' },
      { value: 'Pacific/Marquesas', label: 'Pacific/Marquesas' },
      { value: 'Pacific/Gambier', label: 'Pacific/Gambier' },
      { value: 'Pacific/Pitcairn', label: 'Pacific/Pitcairn' },
      { value: 'Pacific/Easter', label: 'Pacific/Easter' },
      { value: 'Pacific/Norfolk', label: 'Pacific/Norfolk' },
      { value: 'Pacific/Noumea', label: 'Pacific/Noumea' },
      { value: 'Pacific/Fiji', label: 'Pacific/Fiji' },
      { value: 'Asia/Kamchatka', label: 'Pacific/Majuro' },
      { value: 'Pacific/Tarawa', label: 'Pacific/Tarawa' },
      { value: 'Pacific/Auckland', label: 'Auckland Time' },
      { value: 'Pacific/Tongatapu', label: 'Pacific/Tongatapu' },
      { value: 'Pacific/Chatham', label: 'Pacific/Chatham' },
      { value: 'Pacific/Apia', label: 'Pacific/Apia' },
      { value: 'Pacific/Kiritimati', label: 'Pacific/Kiritimati' },
    ],
  },
];

/**
 * Finds the given time zone from the list above.
 *
 * @param {string} value
 */
export const findTimeZoneOptionFromList = (value) => {
  let timeZone;
  TIME_ZONES.forEach(({ options }) => {
    options.forEach((option) => {
      if (option.value === value) timeZone = option;
    });
  });

  return timeZone;
};
