import moment from 'moment';
import DateUtils from 'utils/Date/Date';

/**
 * Finds intervals of given date based on priority.
 *
 * @param {Object} availabilities
 * @param {moment.MomentInput} date
 *
 * @returns {Array} It can be an empty array
 */
const findIntervals = (availabilities, date) => {
  const dateWeekDayName = DateUtils.getDateWeekDayName(date);
  const dateOnlyDate = moment.utc(date).locale('en').format('YYYY-MM-DD');
  const weekDayAvailabilities = availabilities[dateWeekDayName];

  // Specifics field has high order.
  if (Object.keys(weekDayAvailabilities.specifics).includes(dateOnlyDate)) {
    return weekDayAvailabilities.specifics[dateOnlyDate];
  }

  return weekDayAvailabilities.times;
};

/**
 * Updates given availabilities with new intervals.
 *
 * @param {Object} availabilities
 * @param {moment.MomentInput} date
 * @param {Array} intervals UTC intervals
 *
 * @returns {Object} Updated availabilities
 */
const updateDateIntervals = (availabilities, date, intervals) => {
  const dateWeekDayName = DateUtils.getDateWeekDayName(date);
  const dateOnlyDate = moment.utc(date).locale('en').format('YYYY-MM-DD');

  availabilities[dateWeekDayName].specifics[dateOnlyDate] = intervals;

  return availabilities;
};

/**
 * Updates given availabilities with new intervals.
 *
 * @param {Object} availabilities
 * @param {String} weekDayName
 * @param {Array} intervals
 *
 * @returns {Object} Updated availabilities
 */
const updateWeekDayIntervals = (availabilities, weekDayName, intervals) => {
  // Override specifics
  availabilities[weekDayName].specifics = {};
  availabilities[weekDayName].times = intervals;

  return availabilities;
};

export default {
  findIntervals,
  updateWeekDayIntervals,
  updateDateIntervals,
};
