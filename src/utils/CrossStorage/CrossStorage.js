import cookies from 'js-cookie';
import _ from 'lodash';
import { APP_CONST } from 'consts';

/**
 * Sets items.
 *
 * @param {String} key
 * @param {String} value
 * @param {Boolean} global Optional, sets the item cross all subdomains.
 * @param {Number|Date} expires If number given, it should present number of days.
 *
 * @returns {String}
 */
function setItem(key, value, global, expires) {
  // eslint-disable-next-line no-param-reassign
  value = !_.isString(value) ? JSON.stringify(value) : value;

  return cookies.set(key, value, {
    expires: expires || undefined,
    sameSite: 'lax',
    secure: true,
    domain: global ? 'balcon.us' : undefined,
  });
}

/**
 * Gets item.
 *
 * @param {String} key
 * @param {Boolean} parseJson If true returns `JSON.parse(item)`
 *
 * @returns {Object|String} Depends on `parseJson`
 */
function getItem(key, parseJson) {
  if (parseJson) return cookies.getJSON(key);
  return cookies.get(key);
}

/**
 * Removes token and authUser items.
 */
function clear() {
  cookies.remove(APP_CONST.STORAGE_AUTH_USER_KEY, { domain: 'balcon.us' });
  cookies.remove(APP_CONST.STORAGE_TOKEN_KEY, { domain: 'balcon.us' });
}

export default { setItem, getItem, clear };
