import crossStorage from 'utils/CrossStorage/CrossStorage';
import _ from 'lodash';
import qs from 'query-string';
import { APP_CONST } from 'consts';

/**
 * Wraps `fetch()` with already defined `Authorization` and `Content-Type` headers.
 *
 * @param {String} resourcePath
 * @param {String} method HTTP Method (GET|POST|DELETE|...)
 * @param {Object} params Query params will be added to the given resource path.
 * @param {Object} body It will be stringified automatically
 * @param {any} headers Rewriting properties is possible.
 */
function fetch(resourcePath, method, params, body, headers) {
  const localToken = crossStorage.getItem(APP_CONST.STORAGE_TOKEN_KEY, true);
  let stringifiedParams = '';
  const options = {
    method: _.toUpper(method),
    headers: {
      'Content-Type': 'application/json',
      Authorization: localToken ? `Bearer ${localToken}` : undefined,
    },
  };
  if (_.isObject(body)) options.body = JSON.stringify(body);

  if (_.isObject(headers)) Object.assign(options.headers, headers);

  if (_.isObject(params)) stringifiedParams = `?${qs.stringify(params)}`;

  return window.fetch(
    process.env.REACT_APP_BALCON_BACKEND_URI + resourcePath + stringifiedParams,
    options
  );
}

export default { fetch };
