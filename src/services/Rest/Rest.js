import { Subject } from 'rxjs';
import { IO_CONST, REST_CONST } from 'consts';
import { RestUtils } from 'utils';

class Rest {
  /**
   * A list of subscribers.
   */
  handlers = new Subject();

  /**
   * Fires an update then subscribers can get the update.
   *
   * @param {String} type
   * @param {any} data
   */
  _fire(type, data) {
    this.handlers.next({ type, data });
  }

  /**
   * Signs in.
   *
   * @param {String} email
   * @param {String} password
   */
  async signin(email, password) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.SIGNIN,
      'POST',
      null,
      { email, password }
    );

    const json = await response.json();

    if (response.ok) {
      this._fire(IO_CONST.AUTHENTICATED, json);
    }

    return json;
  }

  /**
   * Signs up.
   *
   * @param {String} email
   * @param {String} password
   * @param {String} firstName
   * @param {String} lastName
   */
  async signup(email, password, firstName, lastName) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.SIGNUP,
      'POST',
      null,
      { email, password, firstName, lastName }
    );

    const json = await response.json();

    if (response.ok) {
      this._fire(IO_CONST.AUTHENTICATED, json);
    }

    return json;
  }

  /**
   * Does forgot password.
   *
   * @param {String} email
   */
  async forgotPassword(email) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.FORGOT_PASSWORD,
      'POST',
      null,
      { email }
    );

    return response.json();
  }

  /**
   * Cancels meeting.
   *
   * @param {string} meetingId
   */
  async cancelMeeting(meetingId, reason) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.MEETING.CANCEL,
      'POST',
      null,
      { meetingId, reason }
    );

    return response.json();
  }

  /**
   * Disputes meeting.
   *
   * @param {string} meetingId
   */
  async disputeMeeting(meetingId, reason) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.MEETING.DISPUTE,
      'POST',
      null,
      { meetingId, reason }
    );

    return response.json();
  }

  /**
   * Resets password.
   *
   * @param {String} token
   * @param {String} password
   */
  async resetPassword(token, password) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.RESET_PASSWORD,
      'POST',
      null,
      { token, password }
    );

    return response.json();
  }

  /**
   * Verifies User's email.
   *
   * @param {String} token
   */
  async verifyEmail(token) {
    const response = await RestUtils.fetch(
      REST_CONST.RESOURCE.VERIFY_EMAIL,
      'POST',
      null,
      { token }
    );

    return response.json();
  }

  /**
   * Authenticates any service provider.
   *
   * @param {String} resourcePath
   */
  async authenticate(resourcePath, params) {
    const response = await RestUtils.fetch(resourcePath, 'GET', params);

    const json = await response.json();

    if (response.ok) {
      this._fire(IO_CONST.AUTHENTICATED, json);
    }

    return { status: response.status, response: json };
  }
}

export default Rest;
