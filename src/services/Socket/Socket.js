import io from 'socket.io-client';
import { Subject } from 'rxjs';
import { APP_CONST, IO_CONST, SOCKET_CONST } from 'consts';
import { crossStorage } from 'utils';

/**
 * Handles Socket.io
 */
class Socket {
  isReconnecting = false;

  constructor() {
    // Initializes the connection, but needs to connect manually.
    this.io = io(
      process.env.REACT_APP_BALCON_BACKEND_URI + SOCKET_CONST.NAMESPACE,
      {
        autoConnect: false,
        transports: ['websocket'],
      }
    );

    // https://socket.io/docs/client-api/
    this.io.on('connect', () => {});
    this.io.on('connect_error', (error) => {
      if (this.isReconnecting) return;
      this._fire(IO_CONST.CONNECTION_ERROR, error);
      this.isReconnecting = true;
    });
    this.io.on('connect_timeout', () => {
      this._fire('connectTimeout');
    });
    this.io.on('reconnect', () => {
      this._fire(IO_CONST.CONNECTION_ESTABLISHED);
      this.isReconnecting = false;
    });
    this.io.on('reconnect_attempt', () => {
      this._fire('reconnectAttempt');
    });
    this.io.on('reconnecting', () => {
      this._fire('reconnecting');
    });
    this.io.on('reconnect_error', (error) => {
      this._fire('reconnectError', error);
    });
    this.io.on('reconnect_failed', () => {
      this._fire('reconnectFailed');
    });
    this.io.on('disconnect', (data) => {
      this._fire('disconnect', data);
    });
    this.io.on('ping', () => {});
    this.io.on('pong', () => {});
    this.io.on('error', (data) => {
      if (data === 'Unauthorized') {
        this._fire(IO_CONST.UNAUTHORIZED, data);
      }
    });

    this.io.on(SOCKET_CONST.EVENT.USER, (data) => {
      this._fire(IO_CONST.USER_UPDATED, data);
    });
  }

  /**
   * A list of subscribers.
   */
  handlers = new Subject();

  /**
   * Fires an update then subscribers can get the update.
   *
   * @param {String} type
   * @param {any} data
   */
  _fire(type, data) {
    this.handlers.next({ type, data });
  }

  /**
   * Mark all unread notifications as read if any for the current user.
   *
   * @param {Function} cb
   */
  readNotifications(cb) {
    this.io.emit(SOCKET_CONST.EVENT.READ_NOTIFICATIONS, cb);
  }

  /**
   * Gets a list of notifications that the current user has received today.
   *
   * @param {Function} cb
   */
  getNotifications(cb) {
    this.io.emit(SOCKET_CONST.EVENT.NOTIFICATIONS, cb);
  }

  /**
   * Gets an updated balance of the current user.
   *
   * @param {Function} cb
   */
  getBalance(cb) {
    this.io.emit(SOCKET_CONST.EVENT.BALANCE, cb);
  }

  /**
   * Gets a list of meetings.
   *
   * @param {Date|null} startDate
   * @param {Date|null} endDate
   * @param {Function} cb
   */
  getMeetings(startDate, endDate, cb) {
    this.io.emit(SOCKET_CONST.EVENT.MEETINGS, startDate, endDate, cb);
  }

  /**
   * Cancels a meeting.
   *
   * @param {String} meetingId
   * @param {String} reason
   * @param {Function} cb
   */
  cancelMeeting(meetingId, reason, cb) {
    this.io.emit(SOCKET_CONST.EVENT.CANCEL_MEETING, meetingId, reason, cb);
  }

  /**
   * Saves a notification change.
   *
   * @param {String} notification
   * @param {String} method
   * @param {Function} cb
   */
  saveNotificationChange(notification, method, cb) {
    this.io.emit(
      SOCKET_CONST.EVENT.SAVE_NOTIFICATION,
      notification,
      method,
      cb
    );
  }

  disableNotificationsByMethod(method, cb) {
    this.io.emit(
      SOCKET_CONST.EVENT.DISABLE_NOTIFICATIONS,
      method,
      cb
    );
  }

  /**
   * Unlinks oauth provider.
   *
   * @param {String} provider One of supported providers
   * @param {Function} cb
   */
  unlinkOauth(provider, cb) {
    this.io.emit(SOCKET_CONST.EVENT.UNLINK_OAUTH, provider, cb);
  }

  /**
   * Deletes the user's account.
   *
   * @param {Function} cb
   */
  deleteAccount(cb) {
    this.io.emit(SOCKET_CONST.EVENT.DELETE_ACCOUNT, (result) => {
      this._fire(IO_CONST.USER_DELETED, result);
      cb(result);
    });
  }

  /**
   * Updates User options.
   *
   * @param {String} optionKey
   * @param {any} optionValue
   * @param {Function} cb
   */
  updateUserOption(optionKey, optionValue, cb) {
    this.io.emit(
      SOCKET_CONST.EVENT.UPDATE_USER_OPTION,
      optionKey,
      optionValue,
      cb
    );
  }

  /**
   * Sends a verification email to User's email address.
   *
   * @param {Function} cb
   */
  sendEmailVerification(cb) {
    this.io.emit(SOCKET_CONST.EVENT.SEND_EMAIL_VERIFICATION, cb);
  }

  /**
   * Sends a verification code to User's phone number.
   *
   * @param {Function} cb
   */
  sendPhoneVerification(cb) {
    this.io.emit(SOCKET_CONST.EVENT.SEND_PHONE_VERIFICATION, cb);
  }

  /**
   * Verifies User's phone verification code input.
   *
   * @param {String} code Verification code
   * @param {Function} cb
   */
  verifyPhoneVerificationCode(code, cb) {
    this.io.emit(SOCKET_CONST.EVENT.VERIFY_PHONE_VERIFICATION_CODE, code, cb);
  }

  /**
   * Saves finance settings.
   *
   * @param {String} iban IBAN without first two letters (IR for instance)
   * @param {Function} cb
   */
  saveFinanceSettings(iban, cb) {
    this.io.emit(SOCKET_CONST.EVENT.SAVE_FINANCE_SETTINGS, iban, cb);
  }

  /**
   * Saves profile settings.
   *
   * @param {String} username
   * @param {String} firstName
   * @param {String} lastName
   * @param {String} gender
   * @param {Number} rate
   * @param {String} tagline
   * @param {String} description
   * @param {String} newAvatar
   * @param {Function} cb
   */
  saveProfileSettings(
    username,
    firstName,
    lastName,
    gender,
    rate,
    tagline,
    description,
    newAvatar,
    cb
  ) {
    this.io.emit(
      SOCKET_CONST.EVENT.SAVE_PROFILE_SETTINGS,
      username,
      firstName,
      lastName,
      gender,
      rate,
      tagline,
      description,
      newAvatar,
      cb
    );
  }

  /**
   * Saves account changes.
   *
   * @param {Function} cb
   */
  saveAccountChanges(phone, email, timeZone, locale, cb) {
    this.io.emit(
      SOCKET_CONST.EVENT.SAVE_ACCOUNT,
      phone,
      email,
      timeZone,
      locale,
      cb
    );
  }

  /**
   * Gets a list of payments.
   *
   * @param {Function} cb
   */
  getPayments(cb) {
    this.io.emit(SOCKET_CONST.EVENT.PAYMENTS, cb);
  }

  /**
   * Gets User's default event.
   *
   * @param {Function} cb
   */
  getDefaultEvent(cb) {
    this.io.emit(SOCKET_CONST.EVENT.USER_DEFAULT_EVENT, cb);
  }

  /**
   * Saves password.
   *
   * @param {Function} cb
   */
  savePassword(oldPassword, newPassword, cb) {
    this.io.emit(
      SOCKET_CONST.EVENT.SAVE_PASSWORD,
      oldPassword,
      newPassword,
      cb
    );
  }

  /**
   * Saves calendar settings.
   *
   * @param {Object} availabilities
   * @param {Number} duration
   * @param {Number} frequency
   * @param {Object} buffer
   * @param {Number} meetingsPerDay
   * @param {Number} noticeOfSchedule
   * @param {Object} dateRange
   * @param {Function} cb
   */
  saveCalendarSettings(
    availabilities,
    duration,
    frequency,
    buffer,
    meetingsPerDay,
    noticeOfSchedule,
    dateRange,
    cb
  ) {
    this.io.emit(
      SOCKET_CONST.EVENT.SAVE_CALENDAR_SETTINGS,
      availabilities,
      duration,
      frequency,
      buffer,
      meetingsPerDay,
      noticeOfSchedule,
      dateRange,
      cb
    );
  }

  /**
   * Wraps Socket.on()
   *
   * @param {String} event
   * @param {Function} cb
   */
  on(event, cb) {
    return this.io.on(event, cb);
  }

  /**
   * Wraps Socket.off()
   *
   * @param {String} event
   * @param {Function} cb
   */
  off(event, cb) {
    return this.io.off(event, cb);
  }

  /**
   * Wraps Socket.open().
   * It also defines token query here because the it can be undefined at the connection.
   */
  connect() {
    if (!this.io.connected) {
      this.io.io.opts.query = {
        token: crossStorage.getItem(APP_CONST.STORAGE_TOKEN_KEY, true),
      };
      return this.io.open();
    }

    return this.io;
  }

  /**
   * Wraps Socket.close()
   */
  disconnect() {
    if (!this.io.disconnected) {
      return this.io.close();
    }

    return this.io;
  }
}

export default Socket;
