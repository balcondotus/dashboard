import { AuthUser, crossStorage } from 'utils';
import { APP_CONST, IO_CONST } from 'consts';
import { toast } from 'react-toastify';
import Socket from '../Socket/Socket';
import Rest from '../Rest/Rest';

/**
 * This is a wrapper around Socket and Rest.
 */
class IO {
  socket = new Socket();

  rest = new Rest();

  authUser = new AuthUser();

  reconnectingToastId = null;

  constructor() {
    this.socket.handlers.subscribe(this._servicesChanges.bind(this));
    this.rest.handlers.subscribe(this._servicesChanges.bind(this));
  }

  /**
   * Handles services emits.
   *
   * @param {any} value
   */
  _servicesChanges(value) {
    switch (value.type) {
      case IO_CONST.UNAUTHORIZED:
        this.signout();
        break;
      case IO_CONST.AUTHENTICATED:
        this._authenticate(value.data);
        break;
      case IO_CONST.USER_UPDATED:
        this._updateLocalUser(value.data);
        break;
      case IO_CONST.USER_DELETED:
        this.signout();
        break;
      case IO_CONST.CONNECTION_ERROR:
        this.reconnectingToastId = toast.warn(
          'Cannot connect to the server, retrying…',
          {
            autoClose: false,
            draggable: false,
            closeOnClick: false,
            closeButton: false,
          }
        );
        break;
      case IO_CONST.CONNECTION_ESTABLISHED:
        if (toast.isActive(this.reconnectingToastId))
          toast.update(this.reconnectingToastId, {
            type: 'info',
            autoClose: true,
            draggable: true,
            closeOnClick: true,
            closeButton: true,
            render: 'Connection to the server has established',
          });
        else toast.info('Connection to the server has established');
        break;
      default:
    }
  }

  /**
   * Updates the locally saved user.
   *
   * @param {any} result
   */
  _updateLocalUser(result) {
    const { data } = result;

    crossStorage.setItem(APP_CONST.STORAGE_AUTH_USER_KEY, data, true);

    this.authUser.fire(data);
  }

  /**
   * Authenticate the user locally if and only if no error.
   *
   * @param {any} result
   */
  _authenticate(result) {
    if (result.error || !result.data || !result.data.token) return;

    const {
      data: { token, ...user },
    } = result;

    crossStorage.setItem(APP_CONST.STORAGE_TOKEN_KEY, token, true);
    crossStorage.setItem(APP_CONST.STORAGE_AUTH_USER_KEY, user, true);

    this.socket.connect();

    this.authUser.fire(user);
  }

  /**
   * Signs out the user.
   */
  signout() {
    crossStorage.clear();
    this.authUser.fire(null);
    this.socket.disconnect();
  }
}

export default IO;
