import React from 'react';
import { Row, Container, Col } from 'react-bootstrap';
import { appConfig } from 'configs';
import { FormattedMessage } from 'react-intl';
import moment from 'jalali-moment';
import { APP_CONST } from 'consts';
import styles from './Footer.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faInstagram,
  faLinkedinIn,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { faAt, faPhone } from '@fortawesome/free-solid-svg-icons';
import { formatIncompletePhoneNumber } from 'libphonenumber-js';

const Footer = () => {
  return (
    <footer>
      <div className={`${styles['social-networks']}`}>
        <Container fluid>
          <div className="d-flex flex-items-center justify-content-around">
            <div className="d-none d-md-block">
              <FormattedMessage id="footer.networks.get_connected" />
            </div>
            <div className="d-flex justify-justify-justify-content-end">
              <a
                href={APP_CONST.ROUTES.SOCIAL_MEDIA.INSTAGRAM}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faInstagram} />
              </a>
              <a
                className="ml-5 mr-5"
                href={APP_CONST.ROUTES.SOCIAL_MEDIA.TWITTER}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faTwitter} />
              </a>
              <a
                href={APP_CONST.ROUTES.SOCIAL_MEDIA.LINKEDIN}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FontAwesomeIcon icon={faLinkedinIn} />
              </a>
            </div>
          </div>
        </Container>
      </div>

      <div className={styles.contacts}>
        <Container fluid>
          <Row className="justify-content-center justify-content-md-around text-center text-sm-left">
            <Col sm={4} md={4} lg={2} className="d-none d-sm-block">
              <span className="font-weight-bold text-white h6 d-block">
                <FormattedMessage id="footer.about.section.heading" />
              </span>
              <hr className={`${styles.divider} mx-auto mx-sm-0`} />
              <div className={styles.content}>
                <FormattedMessage id="footer.about.section.desc" />
              </div>
            </Col>

            <Col xs={12} sm={4} md={4} lg={2} className="mt-2 mt-sm-0">
              <span className="font-weight-bold text-white h6 d-block">
                <FormattedMessage id="footer.links.section.heading" />
              </span>
              <hr className={`${styles.divider} mx-auto mx-sm-0`} />
              <div className={styles.content}>
                <ul className="list-unstyled">
                  <li className="mb-2">
                    <a href={APP_CONST.ROUTES.FAQ} target="blank">
                      <FormattedMessage id="footer.faq.title" />
                    </a>
                  </li>
                  <li className="mb-2">
                    <a href={APP_CONST.ROUTES.PRIVACY} target="blank">
                      <FormattedMessage id="footer.privacy.title" />
                    </a>
                  </li>
                  <li className="mb-2">
                    <a href={APP_CONST.ROUTES.TERMS_OF_USE} target="blank">
                      <FormattedMessage id="footer.terms.title" />
                    </a>
                  </li>
                  <li className="mb-2">
                    <a
                      href={APP_CONST.ROUTES.BLOG}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      <FormattedMessage id="footer.blog.title" />
                    </a>
                  </li>
                </ul>
              </div>
            </Col>

            <Col xs={12} sm={4} md={4} lg={2} className="mt-2 mt-sm-0">
              <span className="font-weight-bold text-white h6 d-block">
                <FormattedMessage id="footer.contact.section.heading" />
              </span>
              <hr className={`${styles.divider} mx-auto mx-sm-0`} />
              <div className={styles.content}>
                <div className="d-flex align-items-center justify-content-center justify-content-sm-start">
                  <FontAwesomeIcon icon={faAt} />
                  <span className="ml-3">
                    <a
                      href={`mailto:${appConfig.contactDetail.email}`}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      {appConfig.contactDetail.email}
                    </a>
                  </span>
                </div>
                <div className="d-flex align-items-center mt-3 justify-content-center justify-content-sm-start">
                  <FontAwesomeIcon icon={faPhone} />
                  <span className="ml-3 dir-ltr">
                    <a
                      href={`tel:${appConfig.contactDetail.phoneNumber}`}
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      {formatIncompletePhoneNumber(
                        appConfig.contactDetail.phoneNumber
                      )}
                    </a>
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className={styles.copyright}>
        <Container fluid>
          <div>
            <FormattedMessage
              id="footer.copyright"
              values={{
                year: moment('2020-07-01').toDate(),
                websiteLink: (chunks) => (
                  <a
                    href={process.env.REACT_APP_BALCON_WEBSITE_URI}
                    target="blank"
                  >
                    {chunks}
                  </a>
                ),
              }}
            />
          </div>
        </Container>
      </div>
    </footer>
  );
};

Footer.propTypes = {};

export default Footer;
