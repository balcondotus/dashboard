import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faComments } from '@fortawesome/free-regular-svg-icons';
import { toast } from 'react-toastify';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { withAuthUser, withI18n, withIO } from 'hoc';
import { SOCKET_CONST } from 'consts';
import { IO } from 'services';
import { FormattedMessage, useIntl } from 'react-intl';
import { NotificationRow } from './components';
import { Dropdown, ListGroup, Nav, Tab } from 'react-bootstrap';
import { compose } from 'recompose';
import {
  faCheckDouble,
  faEllipsisV,
  faToggleOff,
} from '@fortawesome/free-solid-svg-icons';
import { appConfig } from 'configs';
import { HSUnfold } from 'shared';
import './NotificationDropdown.scss';

const NotificationDropdown = ({ io, i18n, authUser }) => {
  const [notifications, setNotifications] = useState([]);
  const intl = useIntl();

  const _handleIncomingNotifications = (result) => {
    const { error, data } = result;
    if (error) return toast.error(error.message);

    return setNotifications((oldArray) => {
      const newArray = [...oldArray];
      newArray.unshift(data);
      return newArray.flat();
    });
  };

  useEffect(() => {
    io.socket.on(SOCKET_CONST.EVENT.NOTIFICATIONS, (result) => {
      _handleIncomingNotifications(result);
    });

    io.socket.getNotifications((result) => {
      _handleIncomingNotifications(result);
    });

    return () => {
      io.socket.off(SOCKET_CONST.EVENT.NOTIFICATIONS);
    };
  }, []);

  const unreadCount = () => {
    const unreadNotifications = notifications.filter(
      (notification) => !notification.readAt || notification.readAt === null
    );

    return unreadNotifications.length;
  };

  const disableInternalNotifications = () => {
    io.socket.disableNotificationsByMethod('balcon', (result) => {
      if (result.error) return toast.error(result.error.message);

      return toast.success(
        intl.formatMessage({
          id:
            'navbar.notifications.toast.success.balcon_notifications_disabled',
        })
      );
    });
  };

  /**
   *
   * @param {React.ChangeEvent} e
   */
  const markAsRead = (e) => {
    e.preventDefault();

    io.socket.readNotifications((result) => {
      if (result.error) return toast.error(result.error);

      setNotifications((oldArray) => {
        const newArray = oldArray.map((notification) => {
          const newNotification = notification;
          newNotification.readAt = true;
          return newNotification;
        });

        return newArray;
      });
    });
  };

  return (
    <div className="hs-unfold">
      <HSUnfold
        className="btn btn-icon btn-ghost-secondary rounded-circle"
        target="#notificationNavbarDropdown"
        invokerId="notificationsInvoker"
        type="css-animation"
      >
        <FontAwesomeIcon icon={faBell} />
        <span
          className={`btn-status btn-sm-status ${
            unreadCount() !== 0 ? 'btn-status-danger' : ''
          }`}
        ></span>
      </HSUnfold>

      <div
        id="notificationNavbarDropdown"
        className="hs-unfold-content dropdown-unfold dropdown-menu navbar-dropdown-menu hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated"
        style={{ width: '25rem', animationDuration: '300ms' }}
        data-hs-target-height="518"
        data-hs-unfold-content=""
        data-hs-unfold-content-animation-in="slideInUp"
        data-hs-unfold-content-animation-out="fadeOut"
      >
        <div className="card-header bg-white d-flex align-items-center justify-content-between">
          <span className="card-title h5 my-0">
            <FormattedMessage id="navbar.notifications.notifications.title" />
          </span>

          <div className="hs-unfold">
            <HSUnfold
              className="btn btn-sm btn-icon btn-ghost-secondary rounded-circle"
              target="#notificationOptionsNavbarDropdown"
              invokerId="notificationsOptionsInvoker"
              type="css-animation"
            >
              <FontAwesomeIcon icon={faEllipsisV} />
            </HSUnfold>
            <div
              id="notificationOptionsNavbarDropdown"
              className="hs-unfold-content dropdown-unfold dropdown-menu hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated"
              data-hs-target-height="0"
              data-hs-unfold-content=""
              data-hs-unfold-content-animation-in="slideInUp"
              data-hs-unfold-content-animation-out="fadeOut"
              style={{ animationDuration: '300ms' }}
            >
              <Dropdown.Header>
                <FormattedMessage id="navbar.notifications.settings.title" />
              </Dropdown.Header>
              <Dropdown.Item onClick={markAsRead}>
                <FontAwesomeIcon
                  icon={faCheckDouble}
                  fixedWidth
                  className="dropdown-item-icon"
                />
                <FormattedMessage id="navbar.notifications.read_all.label" />
              </Dropdown.Item>

              <Dropdown.Item onClick={disableInternalNotifications}>
                <FontAwesomeIcon
                  icon={faToggleOff}
                  fixedWidth
                  className="dropdown-item-icon"
                />
                <FormattedMessage id="navbar.notifications.disable_notifications.label" />
              </Dropdown.Item>

              <Dropdown.Divider />
              <Dropdown.Header>
                <FormattedMessage id="navbar.notifications.feedback.title" />
              </Dropdown.Header>
              <Dropdown.Item
                href={`mailto:${appConfig.contactDetail.email}`}
                target="_blank"
              >
                <FontAwesomeIcon
                  icon={faComments}
                  className="dropdown-item-icon"
                  fixedWidth
                />
                <FormattedMessage id="navbar.notifications.report.label" />
              </Dropdown.Item>
            </div>
          </div>
        </div>

        <Tab.Container defaultActiveKey="notifications">
          <Nav as="ul" justify navbar={false} className="nav-tabs">
            <Nav.Link eventKey="notifications">
              <FormattedMessage
                id="navbar.notifications.notifications_x.title"
                values={{ count: unreadCount() }}
              />
            </Nav.Link>
          </Nav>
          <Tab.Content>
            <div className="card-body-height">
              <Tab.Pane eventKey="notifications">
                <ListGroup className="list-group-flush navbar-card-list-group">
                  {!_.isEmpty(notifications) ? (
                    notifications.map((notification) => (
                      <NotificationRow
                        notification={notification}
                        key={notification._id}
                      />
                    ))
                  ) : (
                    <div className="notification d-flex">
                      <div className="mx-auto p-3">
                        <span className="font-weight-bold">
                          <FormattedMessage id="navbar.notifications.no_new_notifications.label" />
                        </span>
                      </div>
                    </div>
                  )}
                </ListGroup>
              </Tab.Pane>
            </div>
          </Tab.Content>
        </Tab.Container>
      </div>
    </div>
  );
};

NotificationDropdown.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withIO, withAuthUser, withI18n)(NotificationDropdown);
