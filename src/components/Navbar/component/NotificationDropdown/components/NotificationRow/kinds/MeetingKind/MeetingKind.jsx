import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVideo } from '@fortawesome/free-solid-svg-icons';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import { withAuthUser } from 'hoc';
import { NOTIFICATION_TYPE } from '../../NotificationTypes';
import styles from './MeetingKind.module.scss';

const MeetingKind = ({ type, content, authUser }) => {
  const renderContentBasedOnType = () => {
    const otherParty =
      content.host._id === authUser._id ? content.invitee : content.host;

    switch (type) {
      case NOTIFICATION_TYPE.MEETING_CANCELED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.meeting_canceled"
            values={{
              otherFullName: otherParty.profile.fullName,
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      case NOTIFICATION_TYPE.MEETING_CREATED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.meeting_created"
            values={{
              otherFullName: otherParty.profile.fullName,
              meetingDate: moment.utc(content.time.start).toDate(),
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      case NOTIFICATION_TYPE.MEETING_RESCHEDULED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.meeting_rescheduled"
            values={{
              otherFullName: otherParty.profile.fullName,
              rescheduledMeetingDate: moment
                .utc(content.rescheduled.rescheduledTo.time.start)
                .toDate(),
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      case NOTIFICATION_TYPE.MEETING_DISPUTED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.meeting_disputed"
            values={{
              userFullNameDisputed: content.disputed.user.profile.fullName,
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      case NOTIFICATION_TYPE.MEETING_REMINDER:
        return (
          <FormattedMessage
            id="navbar.notifications.item.meeting_reminder"
            values={{
              otherFullName: otherParty.profile.fullName,
              meetingDate: moment.utc(content.time.start).toDate(),
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      default:
        throw new Error(`${type} is not implemented`);
    }
  };
  return (
    <>
      <FontAwesomeIcon
        style={{ fontSize: '24px' }}
        color="green"
        icon={faVideo}
      />

      <div className={`${styles['notification--content']} ml-3`}>
        <div className="font-weight-bold">
          <FormattedMessage id="navbar.notifications.item.meeting.title" />
        </div>
        <span>{renderContentBasedOnType()}</span>
      </div>
    </>
  );
};

MeetingKind.propTypes = {
  type: PropTypes.oneOf(Object.keys(NOTIFICATION_TYPE)).isRequired,
  content: PropTypes.object.isRequired,
};

export default withAuthUser(MeetingKind);
