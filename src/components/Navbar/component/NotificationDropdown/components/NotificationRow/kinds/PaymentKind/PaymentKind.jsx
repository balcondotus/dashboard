import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoneyCheckAlt } from '@fortawesome/free-solid-svg-icons';
import { NOTIFICATION_TYPE } from '../../NotificationTypes';
import styles from './PaymentKind.module.scss';

const PaymentKind = ({ type, content }) => {
  const renderContentBasedOnType = () => {
    switch (type) {
      case NOTIFICATION_TYPE.PAYMENT_REFUNDED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.payment_refunded"
            values={{
              paymentAmount: content.amount,
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      case NOTIFICATION_TYPE.PAYMENT_DEDUCTED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.payment_deducted"
            values={{
              paymentAmount: content.amount,
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      case NOTIFICATION_TYPE.PAYMENT_INCREASED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.payment_increased"
            values={{
              paymentAmount: content.amount,
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );
      case NOTIFICATION_TYPE.PAYMENT_RECEIVED:
        return (
          <FormattedMessage
            id="navbar.notifications.item.payment_received"
            values={{
              paymentAmount: content.amount,
              b: (chunks) => <strong>{chunks}</strong>,
            }}
          />
        );

      default:
        throw new Error(`${type} is not implemented`);
    }
  };
  return (
    <>
      <FontAwesomeIcon
        style={{ fontSize: '24px' }}
        color="green"
        icon={faMoneyCheckAlt}
      />

      <div className={`${styles['notification--content']} ml-3`}>
        <div className="font-weight-bold">
          <FormattedMessage id="navbar.notifications.item.payment.title" />
        </div>
        <span>{renderContentBasedOnType()}</span>
      </div>
    </>
  );
};

PaymentKind.propTypes = {
  type: PropTypes.oneOf(Object.keys(NOTIFICATION_TYPE)).isRequired,
  content: PropTypes.object.isRequired,
};

export default PaymentKind;
