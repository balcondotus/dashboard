import React from 'react';
import PropTypes from 'prop-types';
import { NOTIFICATION_TYPE } from './NotificationTypes';
import { MeetingKind, PaymentKind } from './kinds';
import styles from './NotificationRow.module.scss';

const NotificationRow = ({ notification }) => {
  const renderKind = () => {
    switch (notification.type) {
      case NOTIFICATION_TYPE.MEETING_DISPUTED:
        // ! temporary. remove this case when the following issue has fixed
        break;
      case NOTIFICATION_TYPE.MEETING_CANCELED:
      case NOTIFICATION_TYPE.MEETING_CREATED:
      case NOTIFICATION_TYPE.MEETING_RESCHEDULED:
      // ! FIXME: Backend cannot populate disputed.user field
      // case NOTIFICATION_TYPE.MEETING_DISPUTED:
      case NOTIFICATION_TYPE.MEETING_REMINDER:
        return (
          <MeetingKind
            type={notification.type}
            content={notification.content}
          />
        );
      case NOTIFICATION_TYPE.PAYMENT_REFUNDED:
      case NOTIFICATION_TYPE.PAYMENT_DEDUCTED:
      case NOTIFICATION_TYPE.PAYMENT_INCREASED:
      case NOTIFICATION_TYPE.PAYMENT_RECEIVED:
        return (
          <PaymentKind
            type={notification.type}
            content={notification.content}
          />
        );

      default:
        throw new Error(`${notification.type} is not implemented`);
    }
  };

  return (
    <div
      className={`${styles.notification} d-flex align-items-center ${
        !notification.readAt
          ? styles['notification-unread']
          : styles['notification-read']
      }`}
    >
      {renderKind()}
    </div>
  );
};

NotificationRow.propTypes = {
  notification: PropTypes.object.isRequired,
};

export default NotificationRow;
