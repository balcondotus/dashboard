import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { IO } from 'services';
import { withIO } from 'hoc';
import { SOCKET_CONST } from 'consts';
import { FormattedMessage } from 'react-intl';
import styles from './Balance.module.scss';

const Balance = ({ io }) => {
  const [fetching, setFetching] = useState(true);
  const [balance, setBalance] = useState(0);

  const _handleIncomingBalance = (result) => {
    const { error, data } = result;
    if (error) return toast.error(error.message);

    return setBalance(data.balance);
  };

  useEffect(() => {
    setFetching(true);

    io.socket.on(SOCKET_CONST.EVENT.BALANCE, (result) => {
      _handleIncomingBalance(result);
    });

    io.socket.getBalance((result) => {
      _handleIncomingBalance(result);
      setFetching(false);
    });

    return () => {
      io.socket.off(SOCKET_CONST.EVENT.BALANCE);
    };
  }, []);

  return (
    <div className="balance-component">
      <FormattedMessage
        id="navbar.nav.balance.title"
        values={{
          balance: balance,
          balanceSpan: (chunks) => (
            <span className={styles.balance}>{chunks}</span>
          ),
        }}
      />
    </div>
  );
};

Balance.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(Balance);
