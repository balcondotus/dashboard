import React from 'react';
import { Card, Dropdown, Media } from 'react-bootstrap';
import { withAuthUser, withI18n, withIO } from 'hoc';
import { APP_CONST } from 'consts';
import { LinkContainer } from 'react-router-bootstrap';
import { FormattedMessage } from 'react-intl';
import { compose } from 'recompose';
import { Avatar, HSUnfold } from 'shared';

const AvatarDropdown = ({ authUser, io, i18n }) => {
  /**
   *
   * @param {React.ChangeEvent} e
   */
  const signout = (e) => {
    e.preventDefault();

    io.signout();
  };

  return (
    <div className="hs-unfold">
      <HSUnfold
        className="navbar-dropdown-account-wrapper"
        as="a"
        invokerId="avatarInvoker"
        target="#avatarNavbarDropdown"
        type="css-animation"
      >
        <Avatar
          imgSrc={
            authUser.profile?.avatar ||
            `https://api.adorable.io/avatars/285/${authUser._id}.png`
          }
          imgAlt={authUser.profile.fullName}
        />
      </HSUnfold>

      <div
        id="avatarNavbarDropdown"
        style={{ width: '16rem', animationDuration: '300ms' }}
        className="hs-unfold-content dropdown-unfold dropdown-menu navbar-dropdown-menu navbar-dropdown-account hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated"
        data-hs-target-height="394.281"
        data-hs-unfold-content=""
        data-hs-unfold-content-animation-in="slideInUp"
        data-hs-unfold-content-animation-out="fadeOut"
      >
        <Dropdown.ItemText>
          <Media className="align-items-center">
            <Avatar
              className="mr-2"
              imgSrc={
                authUser.profile?.avatar ||
                `https://api.adorable.io/avatars/285/${authUser._id}.png`
              }
              imgAlt={authUser.profile.fullName}
            />

            <Media.Body>
              <Card.Title className="h5 d-block">
                {authUser.profile.fullName}
              </Card.Title>
              <Card.Text className="text-muted">
                {authUser.email.email}
              </Card.Text>
            </Media.Body>
          </Media>
        </Dropdown.ItemText>
        <Dropdown.Divider />
        <Dropdown.Item
          eventKey="profile"
          target="blank"
          href={APP_CONST.ROUTES.USER_PROFILE(authUser.identifier)}
        >
          <FormattedMessage id="navbar.nav.profile.title" />
        </Dropdown.Item>
        <LinkContainer to={APP_CONST.ROUTES.SETTINGS} exact>
          <Dropdown.Item eventKey="settings">
            <FormattedMessage id="navbar.nav.settings.title" />
          </Dropdown.Item>
        </LinkContainer>

        <Dropdown.Divider />
        <Dropdown.Item eventKey="signout" onClick={signout}>
          <FormattedMessage id="navbar.nav.signout.title" />
        </Dropdown.Item>
      </div>
    </div>
  );
};

AvatarDropdown.propTypes = {};

export default compose(withAuthUser, withIO, withI18n)(AvatarDropdown);
