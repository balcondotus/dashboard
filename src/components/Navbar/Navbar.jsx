import React from 'react';
import { Navbar as BNavbar, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withAuthUser, withI18n, withIO } from 'hoc';
import { IO } from 'services';
import { FormattedMessage } from 'react-intl';
import { APP_CONST } from 'consts';
import { NotificationDropdown, Balance, AvatarDropdown } from './component';

const Navbar = ({ io, authUser, i18n }) => {
  return (
    <>
      <BNavbar
        bg="white"
        expand="md"
        className={`navbar-container navbar-flush navbar-height navbar-bordered`}
      >
        <Nav as="ul" className="align-items-center flex-row">
          <Nav.Item as="li">
            <AvatarDropdown />
          </Nav.Item>
          <Nav.Item as="li" className="d-none d-md-block">
            <NotificationDropdown />
          </Nav.Item>
        </Nav>
        <BNavbar.Toggle aria-controls="basic-navbar-nav" />
        <BNavbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <LinkContainer to={APP_CONST.ROUTES.HOME} exact>
              <Nav.Link>
                <FormattedMessage id="navbar.nav.meetings.title" />
              </Nav.Link>
            </LinkContainer>
            <LinkContainer to={APP_CONST.ROUTES.PAYMENTS} exact>
              <Nav.Link>
                <Balance />
              </Nav.Link>
            </LinkContainer>
          </Nav>
        </BNavbar.Collapse>
      </BNavbar>
    </>
  );
};

Navbar.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default compose(withAuthUser, withIO, withI18n)(Navbar);
