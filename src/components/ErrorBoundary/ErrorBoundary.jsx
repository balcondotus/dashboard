import React from 'react';
import Bugsnag from '@bugsnag/js';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { FormattedMessage } from 'react-intl';
import styles from './ErrorBoundary.module.scss';

// TODO: Rewrite this component with Hooks when possible.
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    this.state = { hasError: false, errorInfo: null, error: undefined };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error };
  }

  componentDidCatch(error, errorInfo) {
    Bugsnag.notify(error);
    this.setState({ errorInfo });
  }

  render() {
    const { hasError } = this.state;
    if (hasError) {
      return (
        <div
          className={`${styles['page-wrap']} d-flex flex-row align-items-center`}
        >
          <Container>
            <Row className="justify-content-center">
              <Col className="text-center">
                <span className="display-1 d-block">
                  <FormattedMessage
                    id="error_boundary.heading"
                    values={{
                      sadEmoji: (chunk) => (
                        <span role="img" aria-label="sad emoji">
                          {chunk}
                        </span>
                      ),
                    }}
                  />
                </span>
                <div className="mb-4 lead">
                  <FormattedMessage id="error_boundary.subheading" />
                </div>
                <Button
                  type="button"
                  variant="outline-primary"
                  onClick={(e) => window.location.reload()}
                >
                  <FormattedMessage id="error_boundary.btn.refresh_page.title" />
                </Button>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
