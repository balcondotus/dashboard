import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { toast, ToastContainer, Bounce } from 'react-toastify';
import { withIO, ProtectedRoute, GuardRoute } from 'hoc';
import { APP_CONST } from 'consts';
import moment from 'moment';
import {
  Dashboard,
  NotFound,
  ForgotPassword,
  ResetPassword,
  SignIn,
  SignUp,
  OAuthRedirect,
  VerifyEmail,
} from 'pages';
import { IO } from 'services';
import { AuthUserContext, I18nContext } from 'contexts';
import { IntlProvider } from 'react-intl';
import messagesFa from 'translations/fa.json';
import messagedEn from 'translations/en.json';
import { crossStorage } from 'utils';
import ErrorBoundary from 'components/ErrorBoundary/ErrorBoundary';
import 'react-toastify/dist/ReactToastify.min.css';
import ScrollToTop from 'components/ScrollToTop/ScrollToTop';

const messages = {
  fa: messagesFa,
  en: messagedEn,
};

const App = ({ io }) => {
  const [authUser, setAuthUser] = useState(
    crossStorage.getItem(APP_CONST.STORAGE_AUTH_USER_KEY, true)
  );
  const [currentLocale, updateCurrentLocale] = useState(
    authUser?.locale || 'fa'
  );
  const [isRTL, setRTL] = useState(currentLocale === 'fa');
  const [currentTimeZone, updateCurrentTimeZone] = useState(
    authUser?.timeZone || 'Asia/Tehran'
  );

  useEffect(() => {
    updateCurrentTimeZone(authUser?.timeZone || 'Asia/Tehran');
    updateCurrentLocale(authUser?.locale || 'fa');
  }, [authUser]);

  useEffect(() => {
    moment.tz.setDefault(currentTimeZone);
  }, [currentTimeZone]);

  useEffect(() => {
    setRTL(currentLocale === 'fa');
    // Change moment locale based on selected locale
    // Make sure to import supported locales in setupApp.js
    moment.locale(currentLocale);
  }, [currentLocale]);

  useEffect(() => {
    io.authUser.subscribe((user) => {
      setAuthUser(user);
    });

    return () => {
      io.authUser.unsubscribe();
    };
  }, []);

  return (
    <>
      <HelmetProvider>
        <Helmet
          titleTemplate="%s | Balcon.us"
          defaultTitle="Balcon.us"
          htmlAttributes={{ dir: isRTL ? 'rtl' : 'ltr', lang: currentLocale }}
        >
          <link
            rel="preload"
            rel="stylesheet"
            href={
              isRTL
                ? '/assets/css/vendors.rtl.min.css'
                : '/assets/css/vendors.min.css'
            }
            as="style"
          />
          <link
            rel="preload"
            rel="stylesheet"
            href={
              isRTL
                ? '/assets/css/front-vendors.rtl.min.css'
                : '/assets/css/front-vendors.min.css'
            }
            as="style"
          />

          <link
            rel="preload"
            rel="stylesheet"
            href={
              isRTL
                ? '/assets/css/front.rtl.min.css'
                : '/assets/css/front.min.css'
            }
            as="style"
          />
          <link
            rel="preload"
            rel="stylesheet"
            href={
              isRTL
                ? '/assets/css/base.rtl.min.css'
                : '/assets/css/base.min.css'
            }
            as="style"
          />
        </Helmet>

        <BrowserRouter>
          <ScrollToTop />
          <AuthUserContext.Provider value={authUser}>
            <I18nContext.Provider
              value={{
                locale: currentLocale,
                timeZone: currentTimeZone,
                isRTL,
              }}
            >
              <IntlProvider
                locale={currentLocale}
                timeZone={currentTimeZone}
                defaultLocale="fa"
                messages={messages[currentLocale]}
              >
                <ErrorBoundary>
                  <Switch>
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.HOME}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.PAYMENTS}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.SETTINGS}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.NOTIFICATION_SETTINGS}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.ACCOUNT_SETTINGS}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.FINANCE_SETTINGS}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.PROFILE_SETTINGS}
                      component={Dashboard}
                    />
                    <ProtectedRoute
                      exact
                      path={APP_CONST.ROUTES.CALENDAR_SETTINGS}
                      component={Dashboard}
                    />
                    <GuardRoute
                      exact
                      path={APP_CONST.ROUTES.SIGNIN}
                      component={SignIn}
                    />
                    <GuardRoute
                      exact
                      path={APP_CONST.ROUTES.SIGNUP}
                      component={SignUp}
                    />
                    <Redirect
                      exact
                      from="/password"
                      to={APP_CONST.ROUTES.FORGOT_PASSWORD}
                    />
                    <GuardRoute
                      exact
                      path={APP_CONST.ROUTES.FORGOT_PASSWORD}
                      component={ForgotPassword}
                    />
                    <GuardRoute
                      exact
                      path={APP_CONST.ROUTES.RESET_PASSWORD}
                      component={ResetPassword}
                    />
                    <Route
                      exact
                      path={APP_CONST.ROUTES.OAUTH_REDIRECT}
                      component={OAuthRedirect}
                    />
                    <Route
                      exact
                      path={APP_CONST.ROUTES.VERIFY_EMAIL}
                      component={VerifyEmail}
                    />
                    <Route component={NotFound} />
                  </Switch>
                </ErrorBoundary>
              </IntlProvider>
            </I18nContext.Provider>
          </AuthUserContext.Provider>
        </BrowserRouter>
      </HelmetProvider>

      <ToastContainer
        position={
          isRTL ? toast.POSITION.BOTTOM_RIGHT : toast.POSITION.BOTTOM_LEFT
        }
        autoClose={3000}
        className="rotateY animated"
        rtl={isRTL}
        transition={Bounce}
      />
    </>
  );
};

App.propTypes = {
  io: PropTypes.instanceOf(IO).isRequired,
};

export default withIO(App);
